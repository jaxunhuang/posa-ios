# Posa-iOS

104打卡系統 ＆ 新人類表單(請假+追蹤+簽核) ，往後預計陸續會新增更多表單...
目前以"Starlux"命名。

## Prerequisites

專案使用CocoaPods(請先行安裝)，點擊posa.xcworkspace開啟專案，
開啟Terminal在專案底下輸入：
```
pod init
```

## Deployment
目前自動部署使用fastlane結合Fabric，配合企業帳號發佈給公司同仁指令如下
```
fastlane fabric_enterprise
```
相關操作以及設定請參考專案/fastlane/Fatfile

## API
可先行下載postman。點擊下面連結開啟文件，文件右上方可點選Run in Postman匯入
UAT - https://documenter.getpostman.com/view/4409319/RWToPxWN
Release - 待補
