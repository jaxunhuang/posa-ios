//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

class ChannelDaemon {

  private let channelService:ChannelService
  public let PAGE_SIZE:Int = 20

  init(_ channelService:ChannelService) {
    self.channelService = channelService
  }

  func getChannels() -> Observable<Array<ChannelDto>> {
    return channelService.getChannels(programId: nil, channelId: nil)
  }

  func getHotVideos(channelId:Int, pageNumber:Int, size:Int, categoryId:Int64?) -> Observable<Array<VideoHotDto>> {
    return channelService.getHotVideos(channelId: channelId, pageNumber: pageNumber, size: size, categoryId: categoryId)
  }

  func getLatestVideos(channelId:Int, pageNumber:Int, size:Int, categoryId:Int64?) -> Observable<Array<VideoHotDto>> {
    return channelService.getLatestVideosByChanneldId(channelId: channelId, pageNumber: pageNumber, size: size, categoryId: categoryId)
  }

  func getHotPrograms(channelId:Int, pageNumber:Int, size:Int, categoryId:Int64?) -> Observable<Array<ProgramHotDto>>{
    return channelService.getHotPrograms(channelId: channelId, pageNumber: pageNumber, size: size, categoryId: categoryId)
  }
}
