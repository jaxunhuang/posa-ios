//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire

class VideoDaemon {

  private let videoService:VideoService
  private let programService:ProgramService

  init(_ videoService:VideoService, _ programService:ProgramService) {
    self.videoService = videoService
    self.programService = programService
  }

  func getProgramSubscriptionCount(_ programId:Int) -> Observable<Int> {
    return programService.getProgramSubscriptionCount(programId)
  }

  func getVideoTypes() -> Observable<Array<VideoTypeProgramDto>> {
    return videoService.getVideoTypes();
  }

  func getVideosByProgramAndVideoType(programId:Int,
      pageNumber:Int,
      size:Int,
      videoTypeId:Int) -> Observable<Array<VideoProgramDto>> {
    return programService.getVideosByProgramAndVideoType(programId: programId,
        pageNumber: pageNumber,
        size: size,
        videoTypeId: videoTypeId)
  }

  func getVideo(_ videoId:Int) -> Observable<VideoDto> {
    return videoService.getVideo(videoId)
  }

  func getProgram(_ programId:Int) -> Observable<ProgramDto> {
    return programService.getProgram(programId)
  }

}
