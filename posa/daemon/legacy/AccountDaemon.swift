//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

class AccountDaemon {
  private let pref:Pref
  private let accountService:AccountService

  init(_ accountService:AccountService, _ pref:Pref) {
    self.accountService = accountService
    self.pref = pref
  }

  func findMyself() -> Observable<AccountDto> {
    if !isRegistered {
      return Observable.empty()
    }
    return accountService.getMyself()
  }

  var isRegistered:Bool {
    return pref.isRegistered
  }

  func isSubscribe(_ programId:Int) -> Observable<Bool> {
    if !isRegistered {
      return Observable.just(false)
    }
    return accountService.isSubscribe(programId)
  }

  func subscribe(_ programId:Int) -> Observable<Void> {
    return accountService.subscribe(programId)
  }

  func unsubscribe(_ programId:Int) -> Observable<Void> {
    return accountService.unsubscribe(programId)
  }

}
