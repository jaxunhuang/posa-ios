//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class BaseDto: JsonDto {
  public let code:Int
  public let msg:String?
  public let data:[String: Any?]?

  required init(
      _ code:Int,
      _ msg:String,
      _ data:[String: Any?]?) {
    self.code  = code;
    self.msg = msg;
    self.data = data
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {

    return self.init(
        raw["code"] as! Int,
        raw["msg"] as! String,
        raw["data"] as? [String: Any?])
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:BaseDto, rhs:BaseDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
