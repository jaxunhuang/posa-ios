//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class BaseWifisDto: JsonDto {
  public let code:Int
  public let msg:String?
  public let data:[WifiDto]


  required init(
      _ code:Int,
      _ msg:String,
      _ data:[WifiDto]) {
    self.code = code;
    self.msg = msg;
    self.data = data
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {

    let rawData = raw["data"] as! [WifiDto]
    //let rawWifi = rawData.map(WifiDto.fromDictionary)

    return self.init(
        raw["code"] as! Int,
        raw["msg"] as! String,
        rawData)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:BaseWifisDto, rhs:BaseWifisDto) -> Bool {
    fatalError("== has not been implemented")
  }
}

class WifiDto: JsonDto {
  public let bssid:String?

  required init(_ bssid:String) {
    self.bssid = bssid;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["ssid"] as! String)
  }

  var hashValue:Int = 0

  static func ==(lhs:WifiDto, rhs:WifiDto) -> Bool {
    fatalError("== has not been implemented")
  }
}