// Created by Jaxun on 2018/5/22.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class AccountFullDto: JsonDto {
  public let name:String
  public let empNo:String
  public let jobTitle:String
  public let jobEnTitle:String
  public let departmentName:String
  public let aliasIconUrl:String
  public let eName:String
  public let onBoardDay:String
  public let departmentEName:String
  public let sex:String
  public let tel:String
  public let floorSite:String

  required init(
      _ empNo:String,
      _ name:String,
      _ jobTitle:String,
      _ jobEnTitle:String,
      _ eName:String,
      _ departmentName:String,
      _ departmentEName:String,
      _ onBoardDay:String,
      _ sex:String,
      _ tel:String,
      _ floorSite:String,
      _ aliasIconUrl:String
  ) {
    self.name = name
    self.empNo = empNo
    self.jobTitle = jobTitle
    self.jobEnTitle = jobEnTitle
    self.departmentName = departmentName
    self.aliasIconUrl = aliasIconUrl
    self.eName = eName
    self.onBoardDay = onBoardDay
    self.departmentEName = departmentEName
    self.sex = sex
    self.tel = tel
    self.floorSite = floorSite
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    let rawInfo = raw["info"] as! [String: Any?]
    return self.init(
        rawInfo["emp_no"] as! String,
        rawInfo["emp_name"] as! String,
        rawInfo["job_cname"] as! String,
        rawInfo["job_ename"] as! String,
        rawInfo["en_full_name"] as! String,
        rawInfo["dep_cname"] as! String,
        rawInfo["dep_ename"] as! String,
        rawInfo["day_onboard"] as! String,
        rawInfo["emp_sex"] as! String,
        rawInfo["emp_tel"] as! String,
        rawInfo["site"] as! String,
        rawInfo["photo"] as! String)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:AccountFullDto, rhs:AccountFullDto) -> Bool {
    fatalError("== has not been implemented")
  }
}