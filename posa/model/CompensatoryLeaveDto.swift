//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class CompensatoryLeaveDto: JsonDto {
  public let leaveYear:String
  public let leaveName:String
  public let duration:String
  public let accrued:Float
  public let used:Float
  public let available:Float
  public let leaveNameEn:String

  init(_ raw:[String: Any?]) {
    self.leaveName   = raw["leaveName"] as! String
    self.leaveYear   = raw["leaveYear"] as! String
    self.duration    = raw["duration"] as! String
    self.accrued     = raw["accrued"] as! Float
    self.used        = raw["used"] as! Float
    self.available   = raw["available"] as! Float
    self.leaveNameEn = raw["ELeaveName"] as! String
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw)
  }


  private(set) var hashValue:Int = 0

  static func ==(lhs: CompensatoryLeaveDto, rhs: CompensatoryLeaveDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
