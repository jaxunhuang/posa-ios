//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class BaseMealOrderDto: JsonDto {
  public let date:String
  public let selection:String

  required init(
      _ date:String,
      _ selection:String) {
    self.date  = date
    self.selection = selection
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {

    return self.init(
        raw["date"] as! String,
        raw["selection"] as! String)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:BaseMealOrderDto, rhs:BaseMealOrderDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
