//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class MenstrualLeaveDto: JsonDto {
  public let date:String
  public let accrued:Float
  public let used:Float
  public let available:Float
  public let signing:Float
  public let leavePeriod:String

  required init(
      _ date:String,
      _ accrued:Float,
      _ used:Float,
      _ available:Float,
      _ signing:Float,
      _ leavePeriod:String
  ) {

    self.date = date
    self.accrued = accrued
    self.used = used
    self.available = available
    self.signing = signing
    self.leavePeriod = leavePeriod
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(
        raw["duration"] as! String,
        raw["accrued"] as! Float,
        raw["used"] as! Float,
        raw["available"] as! Float,
        raw["signing"] as! Float,
        raw["leavePeriod"] as! String
    )
  }


  private(set) var hashValue:Int = 0

  static func ==(lhs:MenstrualLeaveDto, rhs:MenstrualLeaveDto) -> Bool {
    fatalError("== has not been implemented")
  }

}
