//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class EmployeeAllLeaveDto: JsonDto {
  public let leaveName:String
  public let leaveYear:String
  public let duration:String
  public let accrued:Float
  public let used:Float
  public let available:Float
  public let signing:Float
  public let leaveUnitName:String
  public let leaveUnit:String
  public let leaveNameEn:String

  required init(
      _ leaveName:String,
      _ leaveYear:String,
      _ duration:String,
      _ accrued:Float,
      _ used:Float,
      _ available:Float,
      _ signing:Float,
      _ leaveUnitName:String,
      _ leaveUnit:String,
      _ leaveNameEn:String
  ) {
    self.leaveName = leaveName
    self.leaveYear = leaveYear
    self.duration = duration
    self.accrued = accrued
    self.used = used
    self.available = available
    self.signing = signing
    self.leaveUnitName = leaveUnitName
    self.leaveUnit = leaveUnit
    self.leaveNameEn = leaveNameEn
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(
        raw["leaveName"] as! String,
        raw["leaveYear"] as! String,
        raw["duration"] as! String,
        raw["accrued"] as! Float,
        raw["used"] as! Float,
        raw["available"] as! Float,
        raw["signing"] as! Float,
        raw["CLeaveUnit"] as! String,
        raw["leaveUnitCode"] as! String, // new server -> raw["leaveUnitCode"]
        raw["ELeaveName"] as! String
    )
  }


  private(set) var hashValue:Int = 0

  static func ==(lhs:EmployeeAllLeaveDto, rhs:EmployeeAllLeaveDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
