//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class MonthlyAbnormalDto: JsonDto {
  public let date:String
  public let abnormalStatusList:Array<AbnormalStatusDto>

  required init(
      _ date:String,
      _ abnormalStatusList:Array<AbnormalStatusDto>) {
    self.date = date;
    self.abnormalStatusList = abnormalStatusList
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(
        raw["date"] as! String,
        raw["status"] as! Array<AbnormalStatusDto>)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:MonthlyAbnormalDto, rhs:MonthlyAbnormalDto) -> Bool {
    fatalError("== has not been implemented")
  }
}

class AbnormalStatusDto: JsonDto {

  public let cardType:Int
  public let cardShould:String
  public let cardTime:String
  public let code:Int

  required init(
      _ cardType:Int,
      _ cardShould:String,
      _ cardTime:String,
      _ code:Int) {
    self.cardType = cardType
    self.cardShould = cardShould
    self.cardTime = cardTime
    self.code = code
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(
        raw["cardType"] as! Int,
        raw["cardShould"] as! String,
        raw["cardTime"] as! String,
        raw["code"] as! Int)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:AbnormalStatusDto, rhs:AbnormalStatusDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
