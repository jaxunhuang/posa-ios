//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class BaseTimeDto: JsonDto {
  public let code:Int
  public let msg:String?
  public let data:TimeDto? //[String: Any?]? //

  required init(
      _ code:Int,
      _ msg:String,
      _ data:TimeDto?) { //[String: Any?]?
    self.code = code;
    self.msg = msg;
    self.data = data
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    let rawData = raw["data"] as? [String: Any?]
    let rawTime = TimeDto.fromDictionary(rawData!) as? TimeDto

    return self.init(
        raw["code"] as! Int,
        raw["msg"] as! String,
        rawTime)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:BaseTimeDto, rhs:BaseTimeDto) -> Bool {
    fatalError("== has not been implemented")
  }
}

class TimeDto: JsonDto {
  public let time:String?

  required init(
      _ time:String) {
    self.time = time;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["time"] as! String)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:TimeDto, rhs:TimeDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
