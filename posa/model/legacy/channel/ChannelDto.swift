//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class ChannelDto: JsonDto {
  public let channelId:Int
  public let channelTitle:String
  public let selected:Bool
  public let categories:Array<CategoryDto>

  required init(_ channelId:Int, _ channelTitle:String, selected:Bool, _ categories:Array<CategoryDto>) {
    self.channelId = channelId;
    self.channelTitle = channelTitle;
    self.selected = selected;
    self.categories = categories;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    let rawCategories = raw["categories"] as! Array<[String: Any?]>;
    let categories = rawCategories.map(CategoryDto.fromDictionary)
    return self.init(raw["channelId"] as! Int,
        raw["channelTitle"] as! String,
        selected: raw["selected"] as! Bool,
        categories);
  }

  var hashValue:Int {
    return self.channelId
  }

  static func ==(lhs:ChannelDto, rhs:ChannelDto) -> Bool {
    return lhs.channelId == rhs.channelId
  }
}
