//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class CategoryDto: JsonDto {
  public let categoryId:Int64
  public let categoryTitle:String

  required init(_ categoryId:Int64, _ categoryTitle:String) {
    self.categoryId = categoryId;
    self.categoryTitle = categoryTitle;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["categoryId"] as! Int64, raw["categoryTitle"] as! String)
  }

  var hashValue:Int {
    return Int(truncatingIfNeeded: self.categoryId)
  }

  static func ==(lhs:CategoryDto, rhs:CategoryDto) -> Bool {
    return lhs.categoryId == rhs.categoryId
  }
}
