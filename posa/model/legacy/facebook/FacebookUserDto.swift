//
// Created by jaxun on 1/4/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation

class FacebookUserDto: JsonDto {

  public let id:String
  public let name:String
  public let email:String
  public let birthday:String?
  public let locale:String?
  public let gender:String?
  public let picture:FacebookUserPictureDto?

  required init(_ id:String,
      _ name:String,
      _ email:String,
      _ birthday:String?,
      _ locale:String?,
      _ gender:String?,
      _ picture:FacebookUserPictureDto?) {
    self.id = id;
    self.name = name;
    self.email = email;
    self.birthday = birthday
    self.locale = locale
    self.gender = gender
    self.picture = picture
  }

  var pictureUrl:String? {
    return picture?.data?.url
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    let rawPicture = raw["picture"] as? [String: Any?]
    return self.init(raw["id"] as! String,
        raw["name"] as! String,
        raw["email"] as! String,
        raw["birthday"] as? String,
        raw["locale"] as? String,
        raw["gender"] as? String,
        rawPicture != nil ? FacebookUserPictureDto.fromDictionary(rawPicture!) : nil);
  }

  var hashValue:Int {
    return self.id.hashValue
  }

  static func ==(lhs:FacebookUserDto, rhs:FacebookUserDto) -> Bool {
    return lhs.id == rhs.id
  }
}

class FacebookUserPictureDto: JsonDto {
  let data:FacebookUserDataDto?
  required init(_ data:FacebookUserDataDto?) {
    self.data = data;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    let rawData = raw["data"] as? [String: Any?]
    return self.init(rawData != nil ? FacebookUserDataDto.fromDictionary(rawData!) : nil);
  }

  var hashValue:Int {
    return self.data?.hashValue ?? 0
  }

  static func ==(lhs:FacebookUserPictureDto, rhs:FacebookUserPictureDto) -> Bool {
    return lhs.data == rhs.data
  }
}

class FacebookUserDataDto: JsonDto {
  let url:String?
  required init(_ url:String?) {
    self.url = url;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["url"] as? String);
  }

  var hashValue:Int {
    return self.url?.hashValue ?? 0
  }

  static func ==(lhs:FacebookUserDataDto, rhs:FacebookUserDataDto) -> Bool {
    return lhs.url == rhs.url
  }
}
