//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class ProgramSubscriptionDto: JsonDto {
  public let programId:Int
  public let programTitle:String
  public let programCoverUrl:String

  public let count:Int
  public let newestCount:Int

  required init(_ programId:Int, _ programTitle:String, count:Int, _ programCoverUrl:String, newestCount:Int) {
    self.programId = programId;
    self.programTitle = programTitle;
    self.count = count;
    self.programCoverUrl = programCoverUrl;
    self.newestCount = newestCount
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["programId"] as! Int,
        raw["programTitle"] as! String,
        count: raw["count"] as! Int,
        raw["programCoverUrl"] as! String,
        newestCount: raw["newestCount"] as! Int);
  }

  var hashValue:Int {
    return self.programId
  }

  static func ==(lhs:ProgramSubscriptionDto, rhs:ProgramSubscriptionDto) -> Bool {
    return lhs.programId == rhs.programId
  }
}
