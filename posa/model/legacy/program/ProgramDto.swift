//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class ProgramDto: JsonDto {
  public let channelId:Int
  public let title:String
  public let totalEpisode:Int
  public let participants:Array<ParticipantDto>
  public let programDescription:String
  public let backgroundImg:String?
  public let genre:String?
  public let ratingPic:String

  required init(_ channelId:Int,
      _ title:String,
      totalEpisode:Int,
      _ participants:Array<ParticipantDto>,
      _ programDescription:String,
      _ backgroundImg:String?,
      _ genre:String?,
      _ ratingPic:String) {
    self.channelId = channelId;
    self.title = title;
    self.totalEpisode = totalEpisode;
    self.participants = participants;
    self.programDescription = programDescription
    self.backgroundImg = backgroundImg
    self.genre = genre
    self.ratingPic = ratingPic
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    let rawParticipants = raw["participants"] as! [[String: Any?]]
    let participants = rawParticipants.map(ParticipantDto.fromDictionary)
    return self.init(raw["channelId"] as! Int,
        raw["title"] as! String,
        totalEpisode: raw["totalEpisode"] as! Int,
        participants,
        raw["description"] as! String,
        raw["backgroundImg"] as? String,
        raw["genre"] as? String,
        raw["ratingPic"] as! String);
  }

  var hashValue:Int {
    return self.channelId ^ self.title.hashValue ^ self.programDescription.hashValue
  }

  static func ==(lhs:ProgramDto, rhs:ProgramDto) -> Bool {
    return lhs.channelId == rhs.channelId && lhs.title == rhs.title && lhs.programDescription == rhs.programDescription
  }
}
