//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class ProgramHotDto: JsonDto {
  public let programId:Int
  public let programTitle:String
  public let subscribe:Bool
  public let programCoverUrl:String

  required init(_ programId:Int, _ programTitle:String, subscribe:Bool, _ programCoverUrl:String) {
    self.programId = programId;
    self.programTitle = programTitle;
    self.subscribe = subscribe;
    self.programCoverUrl = programCoverUrl;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["programId"] as! Int,
        raw["programTitle"] as! String,
        subscribe: raw["isSubscribe"] as! Bool,
        raw["programCoverUrl"] as! String);
  }

  var hashValue:Int {
    return self.programId
  }

  static func ==(lhs:ProgramHotDto, rhs:ProgramHotDto) -> Bool {
    return lhs.programId == rhs.programId
  }
}
