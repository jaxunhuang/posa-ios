//
// Created by jaxun on 12/29/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class AccessTokenDto: JsonDto {
  public let accessToken:String

  required init(_ accessToken:String) {
    self.accessToken = accessToken
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["access-token"] as! String)
  }

  var hashValue:Int {
    return accessToken.hashValue
  }

  static func ==(lhs:AccessTokenDto, rhs:AccessTokenDto) -> Bool {
    return lhs.accessToken == rhs.accessToken
  }
}
