//
// Created by jaxun on 12/29/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class AccountDto: JsonDto {
  public let username:String
  public let imagePath:String?
  public let defaultLang:String

   init(_ username:String, _ imagePath:String?, _ defaultLang:String) {
    self.username = username
    self.imagePath = imagePath
    self.defaultLang = defaultLang
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["username"] as! String, raw["imagePath"] as? String, raw["defaultLang"] as! String)
  }

  var hashValue:Int {
    return username.hashValue
  }

  static func ==(lhs:AccountDto, rhs:AccountDto) -> Bool {
    return lhs.username == rhs.username
  }
}
