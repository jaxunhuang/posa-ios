//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class VideoTypeDto: JsonDto {
  public let videoTypeId:Int
  public let title:String
  public let selected:Bool

  required init(_ videoTypeId:Int, _ title:String, selected:Bool) {
    self.videoTypeId = videoTypeId;
    self.title = title;
    self.selected = selected
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["videoTypeId"] as! Int, raw["title"] as! String, selected: raw["selected"] as! Bool)
  }

  var hashValue:Int {
    return self.videoTypeId
  }

  static func ==(lhs:VideoTypeDto, rhs:VideoTypeDto) -> Bool {
    return lhs.videoTypeId == rhs.videoTypeId
  }
}
