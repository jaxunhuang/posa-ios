//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class VideoFavoriteDto: JsonDto {
  public let videoId:Int
  public let videoTitle:String
  public let programId:Int
  public let programTitle:String

  public let channelId:Int
  public let channelTitle:String

  public let duration:String
  public let thumbUrl:String

  required init(_ videoId:Int,
      _ videoTitle:String,
      _ channelTitle:String,
      _ duration:String,
      _ thumbUrl:String,
      _ programId:Int,
      _ programTitle:String,
      _ channelId:Int) {
    self.videoId = videoId;
    self.videoTitle = videoTitle;
    self.channelTitle = channelTitle;
    self.duration = duration
    self.thumbUrl = thumbUrl
    self.programId = programId
    self.programTitle = programTitle
    self.channelId = channelId
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["videoId"] as! Int,
        raw["videoTitle"] as! String,
        raw["channelTitle"] as! String,
        raw["duration"] as! String,
        raw["thumbUrl"] as! String,
        raw["programId"] as! Int,
        raw["programTitle"] as! String,
        raw["channelId"] as! Int
    )
  }

  var hashValue:Int {
    return self.videoId
  }

  static func ==(lhs:VideoFavoriteDto, rhs:VideoFavoriteDto) -> Bool {
    return lhs.videoId == rhs.videoId
  }
}
