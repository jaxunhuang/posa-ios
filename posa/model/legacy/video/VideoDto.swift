//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class VideoDto: JsonDto {
  public let videoTitle:String
  public let episode:String
  public let streamingInfo:StreamingInfoDto
  public let videoDescription:String
  public let genre:String?
  public let participants:[ParticipantDto]?

  required init(
      _ videoTitle:String,
      _ episode:String,
      _ streamingInfo:StreamingInfoDto,
      _ videoDescription:String,
      _ genre:String?,
      _ participants:[ParticipantDto]?) {
    self.videoTitle = videoTitle;
    self.episode = episode;
    self.streamingInfo = streamingInfo
    self.videoDescription = videoDescription
    self.genre = genre
    self.participants = participants
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    let participants = (raw["participants"] as? [[String: Any?]])?.map(ParticipantDto.fromDictionary)

    return self.init(
        raw["videoTitle"] as! String,
        raw["episode"] as! String,
        StreamingInfoDto.fromDictionary(raw["streamingInfo"] as! [String: Any?]),
        raw["videoDescription"] as! String,
        raw["genre"] as? String,
        participants)
  }

  var hashValue:Int {
    return self.videoTitle.hashValue ^ self.videoDescription.hashValue
  }

  static func ==(lhs:VideoDto, rhs:VideoDto) -> Bool {
    return lhs.videoDescription == rhs.videoDescription && lhs.videoTitle == rhs.videoTitle
  }
}
