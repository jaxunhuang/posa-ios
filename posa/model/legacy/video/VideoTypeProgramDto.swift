//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class VideoTypeProgramDto: JsonDto {
  public let videoTypeId:Int
  public let title:String
  public let sequence:Int
  public let brandId:Int

  required init(_ videoTypeId:Int, _ title:String, _ sequence:Int, _ brandId:Int) {
    self.videoTypeId = videoTypeId;
    self.title = title;
    self.sequence = sequence
    self.brandId = brandId
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["videoTypeId"] as! Int,
        raw["title"] as! String,
        raw["sequence"] as! Int,
        raw["brandId"] as! Int)
  }

  var hashValue:Int {
    return self.videoTypeId
  }

  static func ==(lhs:VideoTypeProgramDto, rhs:VideoTypeProgramDto) -> Bool {
    return lhs.videoTypeId == rhs.videoTypeId
  }
}
