//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class ParticipantDto: JsonDto {
  public let participantId:Int
  public let name:String
  public let type:String

  required init(_ participantId:Int, _ name:String, type:String) {
    self.participantId = participantId;
    self.name = name;
    self.type = type;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["participantId"] as! Int, raw["name"] as! String, type: raw["type"] as! String)
  }

  var hashValue:Int {
    return self.participantId
  }

  static func ==(lhs:ParticipantDto, rhs:ParticipantDto) -> Bool {
    return lhs.participantId == rhs.participantId
  }
}
