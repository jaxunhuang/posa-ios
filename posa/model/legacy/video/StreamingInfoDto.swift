//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class StreamingInfoDto: JsonDto {

  public let hls:String
  public let dash:String

  required init(_ hls:String, _ dash:String) {
    self.hls = hls;
    self.dash = dash;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["hls"] as! String, raw["dash"] as! String)
  }

  var hashValue:Int {
    return self.hls.hashValue ^ self.dash.hashValue
  }

  static func ==(lhs:StreamingInfoDto, rhs:StreamingInfoDto) -> Bool {
    return lhs.hls == rhs.hls && lhs.dash == rhs.dash
  }

}
