//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class VideoHotDto: JsonDto {
  public let videoId:Int
  public let videoTitle:String
  public let episode:String
  public let duration:String
  public let thumbUrl:String
  public let programId:Int
  public let programTitle:String

  required init(_ videoId:Int,
      _ videoTitle:String,
      _ episode:String,
      _ duration:String,
      _ thumbUrl:String,
      _ programId:Int,
      _ programTitle:String) {
    self.videoId = videoId;
    self.videoTitle = videoTitle;
    self.episode = episode;
    self.duration = duration
    self.thumbUrl = thumbUrl
    self.programId = programId
    self.programTitle = programTitle
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["videoId"] as! Int,
        raw["videoTitle"] as! String,
        raw["episode"] as! String,
        raw["duration"] as! String,
        raw["thumbUrl"] as! String,
        raw["programId"] as! Int,
        raw["programTitle"] as! String
    )
  }

  var hashValue:Int {
    return self.videoId
  }

  static func ==(lhs:VideoHotDto, rhs:VideoHotDto) -> Bool {
    return lhs.videoId == rhs.videoId
  }
}
