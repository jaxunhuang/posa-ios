// Created by Jaxun on 2018/5/22.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation


class PunchTimeDto: JsonDto {
  public let punchedServerTime:String
  public let resultMsg:String

  private init(_ dict: [String: Any?]) {
    self.punchedServerTime = dict["sys_time"] as? String ?? ""
    self.resultMsg = dict["msg"] as? String ?? ""
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs: PunchTimeDto, rhs: PunchTimeDto) -> Bool {
    fatalError("== has not been implemented")
  }
}