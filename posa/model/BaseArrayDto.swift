//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class BaseArrayDto: JsonDto {
  public let code:Int
  public let msg:String?
  public let data:[Any?]?

  required init(
      _ code:Int,
      _ msg:String,
      _ data:[Any?]?) {
    self.code  = code;
    self.msg = msg;
    self.data = data
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {

    return self.init(
        raw["code"] as! Int,
        raw["msg"] as! String,
        raw["data"] as? [Any?])
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:BaseArrayDto, rhs:BaseArrayDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
