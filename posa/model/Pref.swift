//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import KeychainAccess

class Pref {
  private let keychain = Keychain(service: "com.starlux.posa")

  private enum Keys: String {
    case ottToken
    case isOnWork
    case aliasPhoto
    case aliasAccount
    case aliasName
    case aliasEmail
    case aliasNameEn
    case aliasNum
    case aliasJobTitle
    case aliasTel
    case aliasSex
    case aliasSiteFloor
    case aliasDepartment
    case aliasDepartmentEn
    case aliasOnBoardDay
    case aliasUniqueId
    case pushToken
    case base64AccountId
    case base64BPMTel
    case uniqueBPMCode
    case uniqueBPMCode2
  }
}

extension Pref {
  var isRegistered: Bool {
    return !ottToken.isEmpty
  }

  var isOnWorkPunched: Bool {
    return setOnWork != nil
  }
}

//Mark:- ivar getter/setter
extension Pref {
  var ottToken: String {
    get {
      return keychain[Keys.ottToken.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.ottToken.rawValue] = newValue
    }
  }

  var setOnWork: String? { //todo: rename
    get {
      return keychain[Keys.isOnWork.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.isOnWork.rawValue] = newValue
    }
  }

  var aliasPhoto: String? {
    get {
      return keychain[Keys.aliasPhoto.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasPhoto.rawValue] = newValue
    }
  }

  var aliasAccount: String? {
    get {
      return keychain[Keys.aliasAccount.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasAccount.rawValue] = newValue
    }
  }

  var aliasName: String? {
    get {
      return keychain[Keys.aliasName.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasName.rawValue] = newValue
    }
  }

  var aliasEmail: String? {
    get {
      return keychain[Keys.aliasEmail.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasEmail.rawValue] = newValue
    }
  }

  var aliasNameEn: String? {
    get {
      return keychain[Keys.aliasNameEn.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasNameEn.rawValue] = newValue
    }
  }

  var aliasNum: String? {
    get {
      return keychain[Keys.aliasNum.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasNum.rawValue] = newValue
    }
  }

  var aliasJobTitle: String? {
    get {
      return keychain[Keys.aliasJobTitle.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasJobTitle.rawValue] = newValue
    }
  }

  var aliasTel: String? {
    get {
      return keychain[Keys.aliasTel.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasTel.rawValue] = newValue
    }
  }

  var aliasSex: String? {
    get {
      return keychain[Keys.aliasSex.rawValue] as? String
    }
    set {
      keychain[Keys.aliasSex.rawValue] = newValue
    }
  }

  var aliasSiteFloor: String? {
    get {
      return keychain[Keys.aliasSiteFloor.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasSiteFloor.rawValue] = newValue
    }
  }

  var aliasDepartment: String? {
    get {
      return keychain[Keys.aliasDepartment.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasDepartment.rawValue] = newValue
    }
  }

  var aliasDepartmentEn: String? {
    get {
      return keychain[Keys.aliasDepartmentEn.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasDepartmentEn.rawValue] = newValue
    }
  }

  var aliasOnBoardDay: String? {
    get {
      return keychain[Keys.aliasOnBoardDay.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasOnBoardDay.rawValue] = newValue
    }
  }

  var aliasUniqueId: String? {
    get {
      return keychain[Keys.aliasUniqueId.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.aliasUniqueId.rawValue] = newValue
    }
  }

  var fcmPushToken: String? {
    get {
      return keychain[Keys.pushToken.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.pushToken.rawValue] = newValue
    }
  }

  var base64AccountId: String? { // with base64 encode used for BPM
    get {
      return keychain[Keys.base64AccountId.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.base64AccountId.rawValue] = newValue
    }
  }

  var base64BPMTel: String? { // with base64 encode used for BPM
    get {
      return keychain[Keys.base64BPMTel.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.base64BPMTel.rawValue] = newValue
    }
  }

  var uniqueBPMCode: String? { // with base64 encode used for BPM
    get {
      return keychain[Keys.uniqueBPMCode.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.uniqueBPMCode.rawValue] = newValue
    }
  }

  var uniqueBPMCode2: String? { // with base64 encode used for BPM
    get {
      return keychain[Keys.uniqueBPMCode2.rawValue] as? String ?? ""
    }
    set {
      keychain[Keys.uniqueBPMCode2.rawValue] = newValue
    }
  }

  //todo: to reduce keychain loading instead of memory setting
/*  lazy var a: String = keychain[Keys.uniqueBPMCode2.rawValue] as? String ?? ""{
    didSet{
      keychain[Keys.uniqueBPMCode2.rawValue] = a
    }
  }*/

}
