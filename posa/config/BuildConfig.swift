//
// Created by jaxun on 12/30/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit

///
/// see XCode for Project -> Build Settings -> Other Swift Flags
///

class BuildConfig {
#if BUILD_DEBUG

  //MARK: change will check all project.
  //static let endpoint = "https://virgo2.starlux-airlines.com:8143/api/" //todo: tmp SIT
  //static let endpoint = "https://ehrms-bpm.starlux-airlines.com/api/" //todo: UAT
  static let endpoint = "https://mobileqa.starlux-airlines.com/api/" //todo: UAT
  static let bpmEndpoint = "http://bpmqa.starlux-airlines.com:82/BPM/" //todo: beta
  //static let bpmEndpoint = "http://bpm.starlux-airlines.com/BPM/" //todo: releases

  static let ottEndpoint = "http://localhost:3000/"
  static let ottBrand = "application/x-www-form-urlencoded"

  static let facebookAppId = "221604121624098"
  static let facebookClientToken = "2bd155f15e33bc47a462e1fe8b812ac2"
  static let facebookAppURLSchemeSuffix = ""

  static let enableLogger = true

#elseif BUILD_RELEASE

  //static let endpoint = "https://virgo2.starlux-airlines.com:8143/api/" //todo: tmp SIT
//  static let endpoint = "https://ehrms-bpm.starlux-airlines.com/api/" //todo: UAT
  static let endpoint = "https://mobileqa.starlux-airlines.com/api/" //todo: UAT
  static let bpmEndpoint = "http://bpmqa.starlux-airlines.com:82/BPM/" //todo: beta
  //static let bpmEndpoint = "http://bpm.starlux-airlines.com/BPM/" //todo: release

  static let ottEndpoint = "http://localhost:3000/"
  static let ottBrand = "application/x-www-form-urlencoded"

  static let facebookAppId = "221604121624098"
  static let facebookClientToken = "2bd155f15e33bc47a462e1fe8b812ac2"
  static let facebookAppURLSchemeSuffix = ""

  static let enableLogger = true

#endif
}
