//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

private class Container {

  static let shared = Container()
    
  let channelDaemon:ChannelDaemon
  let videoDaemon:VideoDaemon
  let accountDaemon:AccountDaemon

  let loginService:LoginService
  let punchTimeService:PunchTimeService
  let mainCalenderService:MainCalenderService
  let leaveStatisticsService:LeaveStatisticsService
  let choseLunchService:ChoseLunchService

  let chooseLunchVMP:ChooseLunchVMP
  let loginVMP:LoginVMP

  init() {
    let pref = Pref()

    Container.configureNetworkLogging()
    BaseService.apiErrorHandler = toastApiErrorHandler;

    let channelService = ChannelService(pref)
    let videoService = VideoService(pref)
    let programService = ProgramService(pref)
    let accountService = AccountService(pref)

    self.channelDaemon = ChannelDaemon(channelService)
    self.videoDaemon = VideoDaemon(videoService, programService)
    self.accountDaemon = AccountDaemon(accountService, pref)

    self.punchTimeService = PunchTimeService(pref)
    self.loginVMP = LoginViewModel()
    self.loginService = LoginService(pref)
    self.mainCalenderService = MainCalenderService(pref)
    self.leaveStatisticsService = LeaveStatisticsService(pref)
    self.choseLunchService = ChoseLunchService(pref)

    self.chooseLunchVMP = ChooseLunchViewModel()
  }

  private static func configureNetworkLogging() {
    //change logLevelForDebug if you want see different http request logging
    let logLevelForDebug = AlamofireLoggerLogLevel.basic
    AlamofireLogger.shared.startLogging(level: BuildConfig.enableLogger ? logLevelForDebug : .none);
    
  }
}

class Injector {

  private static let container = Container.shared

  public static func inject<T>() -> T {
    if T.self == ChannelDaemon.self {
      return container.channelDaemon as! T
    } else if T.self == AccountDaemon.self {
      return container.accountDaemon as! T
    } else if T.self == VideoDaemon.self {
      return container.videoDaemon as! T
    } else if T.self == PunchTimeService.self {
      return container.punchTimeService as! T
    } else if T.self == MainCalenderService.self {
      return container.mainCalenderService as! T
    } else if T.self == LoginVMP.self {
      return container.loginVMP as! T
    } else if T.self == LoginService.self {
      return container.loginService as! T
    } else if T.self == LeaveStatisticsService.self {
      return container.leaveStatisticsService as! T
    } else if T.self == ChoseLunchService.self {
      return container.choseLunchService as! T
    } else if T.self == ChooseLunchVMP.self {
      return container.chooseLunchVMP as! T
    }
    fatalError("no instance registered as type: \(T.self)")
  }
}

