//
// Created by Jaxun on 2018/5/28.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Alertift

//import PageMenu

import CHIPageControl

class MainPageEntry: UIViewController, CAPSPageMenuDelegate {

  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))

  fileprivate var pageMenu:CAPSPageMenu?

  fileprivate var currentPage:Int = 0
  fileprivate var numberOfPage:Int = 0
  fileprivate let pageControl = CHIPageControlAleppo(frame: .zero)

  fileprivate let enterPunchTimeBtn:UIButton = UIButton(type: .system)
  fileprivate let applyFormBtn:UIButton = UIButton(type: .system)
  fileprivate let signFormBtn:UIButton = UIButton(type: .system)
  fileprivate let trackF1ormBtn:UIButton = UIButton(type: .system)

  fileprivate var isLogin = false
  fileprivate var punchType:Int = 9

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

    required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  init(isLogin:Bool, type:Int) {
    super.init(nibName: nil, bundle: nil)
    self.isLogin = isLogin
    self.punchType = type
  }

  // MARK: - UIViewController
  override func viewDidLoad() {
    super.viewDidLoad()

    setNaviLogo()

    //todo: log out icon
    navigationItem.backBarButtonItem = UIBarButtonItem(barButtonSystemItem: .reply, target: nil, action: nil)
    //navigationItem.backBarButtonItem = UIBarButtonItem(image: UIImage(named:"icon_logout"), style: .plain, target: nil, action: nil)

    navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"icon_calendar"), style: .plain,
        target: self,
        action: #selector(clickInfo))
    navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ㄏㄟ")
    navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "tmp_back")

    setPageMenu()

    pageControl.numberOfPages = self.numberOfPage
    pageControl.radius = 4
    pageControl.padding = 6
    pageControl.tintColor = .lightGray
    pageControl.inactiveTransparency = -1
    pageControl.borderWidth = 10
    pageControl.currentPageTintColor = Themes.starluxGold
    pageControl.set(progress: currentPage, animated: true)

    view.addSubview(pageControl)

    initLayout()
  }

  private func setNaviLogo() {
    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
  }

  override func viewWillAppear(_ animated:Bool) {
    // change back icon
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_logout"),
        style: .plain,
        target: self,
        action: #selector(clickLogout))
  }

    @objc func clickLogout() {
    Alertift.alert(title: I18N.key("log_out?"), message: nil)
        .action(.destructive(I18N.key("confirm"))) { [weak self] (_, _, _) in

          self?.cleanAccount()
          self?.navigationController?.popViewController(animated: true)
        }
        .action(.cancel(I18N.key("cancel")))
        .show()
  }

  func cleanAccount() {
    let pref = Pref()
    pref.aliasAccount = nil
    pref.aliasName = nil
    pref.aliasJobTitle = nil
    pref.aliasNum = nil
    pref.aliasDepartment = nil
    pref.aliasPhoto = nil
    pref.aliasEmail = nil
    pref.base64AccountId = nil
    pref.base64BPMTel = nil
    pref.uniqueBPMCode = nil
    pref.uniqueBPMCode2 = nil
  }

    @objc func clickInfo() {
    let controller = MainCalenderController()
    controller.view.backgroundColor = Themes.starluxBg
    navigationController?.pushViewController(controller, animated: true)
  }

  func setPageMenu() {
    var aController: UIViewController

    if self.punchType == 9 {
      aController = PunchTimeEntry(shortCut: false, controller: self)
    } else {
      aController = PunchTimeEntry(shortCut: true, punchType: self.punchType, controller: self)
    }
    let bController = SecondPage(controller: self)

    bController.view.backgroundColor = Themes.starluxBg
    let controllers = [aController, bController]

    self.numberOfPage = controllers.count

    let parameters = [CAPSPageMenuOptionMenuHeight: (0),
                      CAPSPageMenuOptionSelectionIndicatorHeight: (0)] as NSDictionary
    let pageMenu:CAPSPageMenu = CAPSPageMenu(viewControllers: controllers,
        frame: .zero,
        options: parameters as [NSObject: AnyObject])

    self.pageMenu = pageMenu
    self.pageMenu!.delegate = self
    self.view.addSubview(pageMenu.view)
  }

  private func initLayout() {
    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }
    pageControl.snp.makeConstraints { make in
      make.bottom.equalTo(bottomLayoutGuide.snp.bottom).offset(-5)
      make.leading.trailing.equalTo(0)
      make.size.equalTo(CGSize(width: 200, height: 30))
    }
    pageMenu?.view.snp.makeConstraints { make in
      make.top.leading.trailing.equalTo(0)
      make.bottom.equalTo(bottomLayoutGuide.snp.bottom).offset(-5)
    }
  }

  func didMove(toPage controller:UIViewController!, index:Int) {
    print("did \(index)")
    pageControl.set(progress: index, animated: true)
  }
}
