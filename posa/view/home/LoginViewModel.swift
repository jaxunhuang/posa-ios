//
//  LoginViewModel.swift
//  posa
//
//  Created by JaxunC on 2018/8/28.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import RxSwift

class LoginViewModel: LoginVMP{
    var emailText: ReplaySubject<String> = ReplaySubject.create(bufferSize: 1)

    var passwordText: ReplaySubject<String> = ReplaySubject.create(bufferSize: 1)

    var didPressLoginBtn: PublishSubject<Void> = PublishSubject()

    var isLogin: ReplaySubject<Bool> = ReplaySubject.create(bufferSize: 1)

    var punchType: ReplaySubject<Int> = ReplaySubject.create(bufferSize: 1) // punch on = 0, punch off = 1

    var willNavigateViewController: Observable<Bool> = .empty()

    var errorMsgEmptyEmail: Observable<String> = .empty()

    var errorMsgEmptyPassword: Observable<String> = .empty()

    var errorMsgResult: Observable<String> = .empty()

    var suffixEmailString: Observable<String> = .empty()

    fileprivate lazy var loginService: LoginService = Injector.inject()

    init(){

        errorMsgEmptyEmail = Observable.of("E-mail can not be empty")

        errorMsgEmptyPassword = Observable.of("Password can not be empty")

        suffixEmailString = Observable.of("@starlux-airlines.com")

        let zip = Observable.combineLatest(emailText, passwordText)

        let optionalDto = didPressLoginBtn
            .withLatestFrom(zip)
            .flatMap { [weak self] (email, password) -> Observable<BaseDto?> in
                guard let `self` = self else { return .empty() }
                return self.loginService.fetchAlias(account: email, password: password)
            }
            .share(replay: 1)

        errorMsgResult = optionalDto
            .map{ $0?.code != 1 ? $0?.msg : "" }
            .flatMap{ Observable.from(optional: $0) }

        willNavigateViewController = optionalDto
            .map{ $0?.code == 1 }
    }
}
