//
// Created by Jaxun on 2018/8/27.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxKeyboard
import PKHUD

class LoginViewController: UIViewController {

    //MARK:- Vars
    @IBOutlet weak var loginButton: UIButton!{
        willSet{
            newValue.backgroundColor = Themes.starluxGold
            newValue.titleLabel?.font = Themes.font20
            newValue.setTitle("Login", for: .normal)
            newValue.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var emailTextField: UITextField!{
        willSet{
            newValue.placeholder = "E-mail"
            newValue.borderStyle = .none
            newValue.returnKeyType = .next
            newValue.textColor = .gray
            newValue.autocorrectionType = .no
        }
    }
    
    @IBOutlet weak var passwordTextField: UITextField!{
        willSet{
            newValue.placeholder = "Password"
            newValue.isSecureTextEntry = true
            newValue.borderStyle = .none
            newValue.returnKeyType = .done
            newValue.textColor = .gray
            newValue.autocorrectionType = .no
        }
    }
    
    fileprivate lazy var tap = UITapGestureRecognizer()
    
    private let vm: LoginVMP = Injector.inject()

    //MARK:- Methods
    init(isLogin: Bool) {
        super.init(nibName: nil, bundle: nil)
        vm.isLogin.onNext(isLogin)
        vm.punchType.onNext(9)
    }
    
    init(isLogin: Bool, type: Int) {
        super.init(nibName: nil, bundle: nil)
        vm.isLogin.onNext(isLogin)
        vm.punchType.onNext(type)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK:- Keyboard behavior
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [weak self] (visibledHeight) in
                guard let `self` = self else { return }
                
                var safeAreaInsetBottom: CGFloat = 0
                if #available(iOS 11.0, *), visibledHeight > 0 {
                    safeAreaInsetBottom = self.view.safeAreaInsets.bottom
                }
                
                let scrollViewBottom = visibledHeight - safeAreaInsetBottom
                
                self.view.setNeedsLayout()
                UIView.animate(withDuration: 0) {
                    self.scrollView.contentInset.bottom = scrollViewBottom
                    self.scrollView.scrollIndicatorInsets.bottom = scrollViewBottom
                    self.view.layoutIfNeeded()
                }
            }).disposed(by: disposeBag)
        
        
        //MARK:- ControlEvent behavior
        self.view.addGestureRecognizer(tap)
        tap.rx.event.subscribe(onNext: { [weak self] _ in
            self?.view.endEditing(true)
        }).disposed(by: disposeBag)
        
        emailTextField.rx.controlEvent([.editingDidBegin])
            .withLatestFrom(vm.suffixEmailString)
            .subscribe(onNext: { [weak self] text in
                self?.emailTextField.text = text
                self?.setEmailTextFieldCursorPosition()
            }
            ).disposed(by: disposeBag)
        
        passwordTextField.rx.controlEvent([.editingDidBegin])
            .map{ _ in "" }
            .bind(to: passwordTextField.rx.text).disposed(by: disposeBag)
        
        emailTextField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe(onNext: { [weak self] _ in
                self?.passwordTextField.becomeFirstResponder()
            }).disposed(by: disposeBag)
        
        passwordTextField.rx.controlEvent([.editingDidEndOnExit])
            .bind(to: vm.didPressLoginBtn).disposed(by: disposeBag)

        
        //MARK:- Text behavior
        emailTextField.rx.text
            .flatMap{ Observable.from(optional: $0) }
            .bind(to: vm.emailText).disposed(by: disposeBag)
        
        passwordTextField.rx.text
            .flatMap{ Observable.from(optional: $0) }
            .bind(to: vm.passwordText).disposed(by: disposeBag)
        
        
        //MARK:- Button behavior
        //0 == empty email, 1 == empty password, 2 == pass
        let loginType = loginButton.rx.tap
            .map{ [weak self] _ -> Int in
                if self?.emailTextField.text?.isEmpty == true {
                    return 0
                }
                else if self?.passwordTextField.text?.isEmpty == true {
                    return 1
                }
                else{
                    return 2
                }
            }
            .share(replay: 1)
        
        loginType.filter{ $0 == 0 }
            .withLatestFrom(vm.emailText)
            .do(onNext: { (text) in
                HUD.flash(.labeledError(title: text, subtitle: nil), delay: 3.0, completion: nil)
            })
            .subscribe().disposed(by: disposeBag)
        
        loginType.filter{ $0 == 1 }
            .withLatestFrom(vm.passwordText)
            .do(onNext: { (text) in
                HUD.flash(.labeledError(title: text, subtitle: nil), delay: 3.0, completion: nil)
            })
            .subscribe().disposed(by: disposeBag)
        
        loginType.filter{ $0 == 2 }
            .map{ _ in }
            .do(onNext: { _ in
                HUD.show(.progress)
            })
            .bind(to: vm.didPressLoginBtn).disposed(by: disposeBag)
        
        //MARK:- Subscribe VM
        vm.errorMsgResult
            .do(onNext: { (text) in
                guard !text.isEmpty else { return }
                HUD.flash(.labeledError(title: text, subtitle: nil), delay: 3.0, completion: nil)
            })
            .subscribe().disposed(by: disposeBag)
        
        vm.willNavigateViewController
            .filter { $0 }
            .withLatestFrom(vm.punchType)
            .do(onNext: { [weak self] punchType in
                HUD.hide()
                self?.navigateMainViewController(true, punchType: punchType)
            })
            .subscribe().disposed(by: disposeBag)
        
        vm.isLogin.filter{ $0 }
            .withLatestFrom(vm.punchType)
            .do(onNext: { [weak self] type in
                self?.hideNavigationBar()
                self?.navigateMainViewController(false, punchType: type)
            })
            .subscribe().disposed(by: disposeBag)
        
        //MARK:- View life cycle
        self.rx.methodInvoked(#selector(viewWillAppear))
            .map{ $0.first as? Bool ?? false }
            .map{ animated in Void() }
            .do(onNext: { [weak navigationController] _ in
                navigationController?.isNavigationBarHidden = true
                navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            }).subscribe().disposed(by: disposeBag)
        
        self.rx.methodInvoked(#selector(viewWillDisappear))
            .map{ $0.first as? Bool ?? false }
            .map{ animated in Void() }
            .do(onNext: { [weak self] _ in
                self?.hideNavigationBar()
            }).subscribe().disposed(by: disposeBag)
    }
}

private extension LoginViewController{
    func setEmailTextFieldCursorPosition(_ position: Int = 0) {
        let beginning = emailTextField.beginningOfDocument
        guard let textFieldPosition = emailTextField.position(from: beginning, offset: position) else { return }
        emailTextField.selectedTextRange = emailTextField.textRange(from: textFieldPosition, to: textFieldPosition)
    }
    
    func navigateMainViewController(_ animated: Bool = true, punchType: Int){
        let controller = MainPageEntry(isLogin: true, type: punchType)
        controller.view.backgroundColor = UIColor.white
        navigationController?.pushViewController(controller, animated: animated)
    }
    
    func hideNavigationBar() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = Themes.starluxGold
        navigationController?.navigationBar.isTranslucent = false
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_logout")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_logout")
    }
}

