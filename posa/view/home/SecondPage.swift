//
// Created by Jaxun on 2018/6/17.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import PKHUD
import Kingfisher

class SecondPage: UIViewController {

  fileprivate let pref = Pref()
  fileprivate var mainPage = MainPageEntry()

  fileprivate let aliasIcon = UIImageView()
  fileprivate let aliasName = UILabel(frame: .zero)
  fileprivate let aliasNumber = UILabel(frame: .zero)
  fileprivate let aliasDep = UILabel(frame: .zero)
  fileprivate let aliasUnit = UILabel(frame: .zero)
  fileprivate let separateLine = UIView()

  fileprivate let leaveStatisticsBtn = UIButton(type: .custom)
  fileprivate let leaveStatisticsLabel = UILabel(frame: .zero)
  fileprivate let leaveStatisticsImageView = UIImageView(frame: .zero)

    required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  init(controller:MainPageEntry) {
    super.init(nibName: nil, bundle: nil)
    self.mainPage = controller
  }



  override func viewDidLoad() {
    super.viewDidLoad()

    if let iconUrl = pref.aliasPhoto {
      let optionalUrl = URL(string: iconUrl) // todo: tmp for ad server error
//    if let email = pref.aliasEmail {
      //let optionalUrl = URL(string: "http://c0065/photo/" + email)
      aliasIcon.kf.setImage(with: optionalUrl, placeholder: UIImage(named: "aliasDefault"))
    }
    aliasIcon.layer.cornerRadius = 32.5
    aliasIcon.layer.masksToBounds = true
    aliasIcon.layer.borderWidth = 1
    aliasIcon.layer.borderColor = Themes.starluxGold.cgColor
    self.view.addSubview(aliasIcon)

    aliasName.text = pref.aliasNameEn
    aliasName.textColor = Themes.starluxBlack
    aliasName.font = Dimension.isPhoneSE ? Themes.font17 : Themes.font20
    aliasName.adjustsFontSizeToFitWidth = true
    self.view.addSubview(aliasName)

    aliasNumber.text = pref.aliasNum
    aliasNumber.textColor = Themes.starluxBlack
    aliasNumber.font = Dimension.isPhoneSE ? Themes.font14 : Themes.font14
    aliasNumber.textAlignment = .left

    self.view.addSubview(aliasNumber)

    aliasUnit.text = pref.aliasJobTitle
    aliasUnit.textColor = Themes.starluxBlack
    aliasUnit.font = Dimension.isPhoneSE ? Themes.font11 : Themes.font11
    aliasUnit.adjustsFontSizeToFitWidth = true
    self.view.addSubview(aliasUnit)

    aliasDep.text = pref.aliasDepartmentEn
    aliasDep.textColor = Themes.starluxBlack
    aliasDep.font = Dimension.isPhoneSE ? Themes.font11 : Themes.font11
    aliasDep.adjustsFontSizeToFitWidth = true
    self.view.addSubview(aliasDep)

    separateLine.backgroundColor = .lightGray
    self.view.addSubview(separateLine)
    displayFormApplyBtn()
    initLayout()
  }

  private func displayFormApplyBtn() {
    // cornerRadius + shadow + color -> no highlighted effect
    // maskToBounds + bgImg -> no shadow & corner

    leaveStatisticsBtn.addTarget(self, action: #selector(clickFormLeave), for: UIControlEvents.touchUpInside)
    leaveStatisticsBtn.setTitleColor(Themes.starluxGold, for: .normal)
    leaveStatisticsBtn.setTitleColor(.white, for: .selected)
    leaveStatisticsBtn.titleLabel?.textAlignment = .center
    leaveStatisticsBtn.backgroundColor = .white
    leaveStatisticsBtn.setTitleColor(.gray, for: .selected)
    leaveStatisticsBtn.layer.shadowRadius = 3
    leaveStatisticsBtn.layer.shadowOpacity = 0.3
    leaveStatisticsBtn.layer.cornerRadius = 10
    leaveStatisticsBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
    //leaveStatisticsBtn.setBackgroundImage(Themes.whiteImg, for: .normal)
    //leaveStatisticsBtn.setBackgroundImage(Themes.starluxGrayImg, for: .highlighted)
    //leaveStatisticsBtn.layer.masksToBounds = true
    self.view.addSubview(leaveStatisticsBtn)

    leaveStatisticsLabel.text = I18N.key("attendance_record")
    leaveStatisticsLabel.textAlignment = .center
    leaveStatisticsLabel.textColor = Themes.starluxGold
    leaveStatisticsLabel.font = Dimension.isPhoneSE ? Themes.font12 : Themes.font15
    leaveStatisticsLabel.numberOfLines = 2
    leaveStatisticsImageView.image = UIImage(named: "icon_leaves") //todo: on img
    leaveStatisticsBtn.addSubview(leaveStatisticsLabel)
    leaveStatisticsBtn.addSubview(leaveStatisticsImageView)
  }

    @objc func clickFormLeave() {
    let controller = LeaveStatisticsController()
    mainPage.navigationController?.pushViewController(controller, animated: true)
  }

  private func initLayout() {
    aliasIcon.snp.makeConstraints { make in
      make.leading.equalTo(separateLine).offset(20)
      make.top.equalTo(10)
      if Dimension.isPhoneSE { // Phone SE size
        make.leading.equalTo(separateLine).offset(15)
      }
      make.size.equalTo(CGSize(width: 65, height: 65))
    }
    aliasName.snp.makeConstraints { make in
      make.top.equalTo(aliasIcon).offset(2)
      make.leading.equalTo(aliasIcon.snp.trailing).offset(20)
    }
    aliasNumber.snp.makeConstraints { make in
      make.bottom.equalTo(aliasName)
      make.leading.equalTo(aliasName.snp.trailing).offset(10)
      if Dimension.isPhoneSE { // Phone SE size
        //make.trailing.lessThanOrEqualTo(-10)
        make.top.equalTo(aliasName.snp.bottom).offset(10)
      }
    }

    aliasDep.snp.makeConstraints { make in
      make.top.equalTo(aliasNumber.snp.bottom).offset(5)
      make.leading.equalTo(aliasName)
      make.trailing.equalTo(-10)
    }

    aliasUnit.snp.makeConstraints { make in
      make.top.equalTo(aliasDep.snp.bottom).offset(3)
      make.leading.equalTo(aliasName)
      make.trailing.equalTo(-10)
    }

    separateLine.snp.makeConstraints { make in
      make.top.equalTo(aliasIcon.snp.bottom).offset(10)
      make.leading.equalTo(10)
      make.trailing.equalTo(-10)
      make.height.equalTo(1)
      //make.bottom.equalTo(lastPunchLabel.snp.top).offset(-20)
    }

    leaveStatisticsBtn.snp.makeConstraints { make in
      make.top.equalTo(separateLine.snp.bottom).offset(15)
      make.leading.equalTo(separateLine)
      make.width.equalTo(Screens.width / 2)
      make.height.equalTo(80)
    }
    leaveStatisticsImageView.snp.makeConstraints { make in
      make.centerX.equalTo(leaveStatisticsBtn)
      make.size.equalTo(CGSize(width: 50, height: 50))
      make.bottom.equalTo(leaveStatisticsLabel.snp.top).offset(3)
    }
    leaveStatisticsLabel.snp.makeConstraints { make in
      make.centerX.equalTo(leaveStatisticsBtn)
      make.centerY.equalTo(leaveStatisticsBtn).offset(20)
      make.leading.trailing.equalTo(leaveStatisticsBtn)
    }
  }

}
