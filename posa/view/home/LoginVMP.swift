//
//  LoginVMP.swift
//  posa
//
//  Created by JaxunC on 2018/8/28.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import RxSwift

protocol LoginVMP{
    var emailText: ReplaySubject<String> { get }
    
    var passwordText: ReplaySubject<String> { get }
    
    var didPressLoginBtn: PublishSubject<Void> { get }
    
    var willNavigateViewController: Observable<Bool> { get }
    
    var isLogin: ReplaySubject<Bool> { get }
    
    var punchType: ReplaySubject<Int> { get }
    
    var errorMsgEmptyEmail: Observable<String> { get }
    
    var errorMsgEmptyPassword: Observable<String> { get }
    
    var errorMsgResult: Observable<String> { get }
    
    var suffixEmailString: Observable<String> { get }
}
