//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit
import Datez
import RxAlamofire
import Alamofire
import SystemConfiguration.CaptiveNetwork
import PKHUD
import Alertift
import EasyNotificationBadge
import TTGSnackbar


class PunchTimeEntry: UIViewController {

    fileprivate let pref = Pref()
    fileprivate let punchTimeService: PunchTimeService = Injector.inject()

    fileprivate let punchTimeOnBtn = UIButton(type: .system)
    fileprivate let punchTimeOnLabel = UILabel(frame: .zero)
    fileprivate let punchTimeOnImageView = UIImageView(frame: .zero)

    fileprivate let punchTimeOffBtn = UIButton(type: .system)
    fileprivate let punchTimeOffLabel = UILabel(frame: .zero)
    fileprivate let punchTimeOffImageView = UIImageView(frame: .zero)

    fileprivate let firstPunchLabel = UILabel(frame: .zero)
    fileprivate let firstPunchTimeLabel = UILabel(frame: .zero)
    fileprivate let lastPunchLabel = UILabel(frame: .zero)
    fileprivate let lastPunchTimeLabel = UILabel(frame: .zero)

    fileprivate let date = Date()
    fileprivate var accountId = String()
    fileprivate var nextTimeSecond = Date()
    fileprivate var deviceBSSID = String()
    fileprivate var bssidList = Array<String>()
    fileprivate var isMatch = false
    fileprivate var isFromShortCut = false
    fileprivate var punchTimeType = 9 // 上班打卡 = 0, 下班打卡 = 1
    fileprivate var isLastTimePunch = false
    fileprivate let dateFormatter = DateFormatter()

    fileprivate let currentDayLabel: UILabel = UILabel(frame: .zero)

    fileprivate let currentDayBigLabel: UILabel = UILabel(frame: .zero)
    fileprivate let currentTimeLabel: UILabel = UILabel(frame: .zero)
    fileprivate let currentTimeContainer = UIView()

    fileprivate let separateLine = UIView()

    fileprivate let formSignBadge = UIView()
    fileprivate let formApplyBtn = UIButton(type: .system)
    fileprivate let formApplyLabel = UILabel(frame: .zero)
    fileprivate let formApplyImageView = UIImageView(frame: .zero)

    fileprivate var badgeAppearance = BadgeAppearance()

    fileprivate let formSignBtn = UIButton(type: .system)
    fileprivate let formSignLabel = UILabel(frame: .zero)
    fileprivate let formSignImageView = UIImageView(frame: .zero)

    fileprivate let formTrackBadge = UIView()
    fileprivate let formTrackBtn = UIButton(type: .system)
    fileprivate let formTrackLabel = UILabel(frame: .zero)
    fileprivate let formTrackImageView = UIImageView(frame: .zero)

    fileprivate let buttonContainer = UIView()

    fileprivate var mainPage = MainPageEntry()

    required override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(shortCut: Bool, punchType: Int, controller: MainPageEntry) {
        super.init(nibName: nil, bundle: nil)

        isFromShortCut = shortCut
        punchTimeType = punchType

        self.mainPage = controller
    }

    init(shortCut: Bool, controller: MainPageEntry) {
        super.init(nibName: nil, bundle: nil)

        isFromShortCut = shortCut
        self.mainPage = controller
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self,
                selector: #selector(getBadgeCounts),
                name: NSNotification.Name(rawValue: "UpdateBadgeCount"),
                object: nil)

        //detect device rotate
        NotificationCenter.default.addObserver(self,
                selector: #selector(deviceRotated),
                name: NSNotification.Name.UIDeviceOrientationDidChange,
                object: nil)

        // register pushToken to starlux server

        sendPushToken()
        updateDisplayTime(date: Date())
        syncTime()
        updateTodayRecord()
        setUI()

        if (isFromShortCut) {
            if punchTimeType == 0 {
                clickOnWorkPunch()
            } else {
                clickOffWorkPunch()
            }
        }
    }

    fileprivate func sendPushToken() {
        if let pushToken = pref.fcmPushToken {
            punchTimeService.sendPushToken(token: pushToken)
        }
    }

    @objc func getBadgeCounts() {
        guard isConnect() else {
            return
        }
        punchTimeService
                .getFormBadgeCounts()
                .subscribe(onNext: { [weak self] result -> Void in
                    guard let strongSelf = self,
                          let result = result as? BaseDto,
                          let rawDic = result.data as [String: AnyObject]?
                            else {
                        return
                    }
                    //                let rawDic = result.data as! [String: AnyObject]

                    let approveCounts = rawDic["badgeForApprove"] as! Int
                    let trackCounts = rawDic["badgeForTrack"] as! Int

                    print(approveCounts)
                    print(trackCounts)
                    strongSelf.formSignBadge.badge(text: String(approveCounts), appearance: strongSelf.badgeAppearance)
                    strongSelf.formTrackBadge.badge(text: String(trackCounts), appearance: strongSelf.badgeAppearance)

                    if approveCounts == 0 {
                        strongSelf.formSignBadge.isHidden = true
                    }

                    if trackCounts == 0 {
                        strongSelf.formTrackBadge.isHidden = true
                    }

                }).disposed(by: self.disposeBag)
    }

    fileprivate func getBPMInfo() {
        if isConnect() {
            punchTimeService.getBPMInfo()
                    .subscribe(onNext: { [weak self] result -> Void in
                        guard let strongSelf = self else {
                            return
                        }

                        /* API result:
                         *
                         * {"xml":{"group":{"UniqueID":"3","AccountID":"1700294","Mobile":"\"(02)27911000-7657","IMEICode":"1234","DeviceToken":"12222","Status":"1"}}}
                         */

                        if let dict = result as? [String: AnyObject] {
                            //print(dict)

                            let valDict = dict["xml"]!["group"] as! [String: Any?]
                            let tel = valDict["Mobile"] as! String
                            let unique1 = valDict["IMEICode"] as! String
                            let unique2 = valDict["DeviceToken"] as! String
                            let bpmAccount = valDict["AccountID"] as! String

                            strongSelf.pref.base64AccountId = bpmAccount.urlEncoded()
                            strongSelf.pref.base64BPMTel = tel.urlEncoded()
                            strongSelf.pref.uniqueBPMCode = unique1.urlEncoded()
                            strongSelf.pref.uniqueBPMCode2 = unique2.urlEncoded()
                        }
                    }).disposed(by: self.disposeBag)
        }
    }

    fileprivate func syncTime() {
        let url = URL(string: "\(BuildConfig.endpoint)time")
        var urlRequest = URLRequest(url: url!)
        _ = Date()

        urlRequest.httpMethod = HTTPMethod.get.rawValue
        urlRequest.timeoutInterval = 5

        Alamofire.request(urlRequest)
                .responseJSON { [weak self] response in
                    guard let strongSelf = self else {
                        return
                    }
                    //to get JSON return value
                    if let dict = response.result.value as? [String: AnyObject] {
                        _ = dict as NSDictionary
                        let valDict = dict["data"] as! [String: Any?]
                        if let serverTime = valDict["time"] as? String {
                            //print("serverTime = \(serverTime)")

                            //displayTime = Utils.tranceTimeFormat(serverTime)
                            //strongSelf.updateDisplayTime(date: displayTime)
                            strongSelf.updateDisplayServerTime(date: serverTime)
                        }
                    }
                }
    }

    func isConnect() -> Bool {
        var reachability: Reachability!
        do {
            reachability = try Reachability.init(hostname: "\(BuildConfig.endpoint)wifi")
        } catch {
            return false
        }
        if reachability.isReachable {
            //showBlockAlert()
            return true
        } else {
            return false
        }
    }

    // check before punch time
    fileprivate func checkNetworkState() {
        if isConnect() {
            fetchWifiAndCompare()
        } else {
            let bar: TTGSnackbar = TTGSnackbar.init(message: "Unable To connect the internet", duration: .short)
            bar.backgroundColor = Themes.warningRed
            bar.show()
        }
    }

    private func fetchWifiAndCompare() {
        let url = URL(string: "\(BuildConfig.endpoint)wifi")
        let urlRequest = URLRequest(url: url!)
        Alamofire.request(urlRequest)
                .responseJSON { [weak self] response in
                    guard let strongSelf = self else {
                        return
                    }
                    if let status = response.response?.statusCode {
                        switch (status) {
                        case 200:
                            print("example success")
                        default:
                            print("error with response status: \(status)")
                            strongSelf.showUnreachAlert()
                        }
                    }
                    if let dict = response.result.value as? [String: AnyObject] {
                        _ = dict as NSDictionary
                        let valDict = dict["data"] as! [String: Any?]

                        if let wifiList = valDict["ssid"] as? [String] {
                            //print("wifiList = \(wifiList)")
                            strongSelf.bssidList = wifiList
                            strongSelf.wifiListCompare()
                        }
                    }
                }
    }

    private func updateTodayRecord() {
        if let aId = pref.aliasAccount {
            accountId = aId
        }
        let todayParams = Utils.getFullDateFormat(Date())
        punchTimeService.fetchTodayRecords(accountId: accountId, today: todayParams)
                .subscribe(onNext: { [weak self] result -> Void in
                    guard let strongSelf = self else {
                        return
                    }
                    let recordsDto = result as RecordsDto
                    let firstTime: String! = recordsDto.workOnRecordList.first
                    let lastTime: String! = recordsDto.workOffRecordList.last

                    strongSelf.firstPunchTimeLabel.text = firstTime
                    strongSelf.lastPunchTimeLabel.text = lastTime

                    if !recordsDto.workOffRecordList.isEmpty {

                    }

                    if recordsDto.workOnRecordList.isEmpty {
                        strongSelf.firstPunchTimeLabel.text = PunchTimeDefine.noData
                    }

                    if recordsDto.workOffRecordList.isEmpty {
                        strongSelf.lastPunchTimeLabel.text = PunchTimeDefine.noData
                    }

                    strongSelf.setBtnStatus()
                }).disposed(by: self.disposeBag)
    }

    private func timeTransform(time: String) -> Date {
        let format = DateFormatter()
        format.dateFormat = PunchTimeDefine.yearNtimeFormat
        if let timeDate = format.date(from: time) {
            return timeDate
        }
        return Date()
    }

    private func setUI() {
        displayContainer()

        separateLine.backgroundColor = .lightGray
        self.view.addSubview(separateLine)

        displayPunchTimeOnUI()
        displayPunchTimeOffUI()

        buttonContainer.backgroundColor = Themes.starluxBg
        self.view.addSubview(buttonContainer)

        // for form badge count
        badgeAppearance.backgroundColor = Themes.badgeRed
        badgeAppearance.textColor = .white

        displayFormApplyUI()
        displayFormSignUI()
        displayFormTrackButton()

        initLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        checkNetworkState()
        getDeviceBssid()
        getBPMInfo()
        getBadgeCounts()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        cleanSettings()
    }

    private func displayContainer() {
        self.view.addSubview(currentDayBigLabel)
        self.view.addSubview(currentDayLabel)
        self.view.addSubview(currentTimeContainer)
        currentTimeContainer.addSubview(currentTimeLabel)
        self.view.addSubview(firstPunchLabel)
        self.view.addSubview(firstPunchTimeLabel)
        self.view.addSubview(lastPunchLabel)
        self.view.addSubview(lastPunchTimeLabel)

        currentDayBigLabel.font = Dimension.isPhoneSE ? Themes.font50FutureM : Themes.font80FutureM
        currentDayBigLabel.textAlignment = .center
        currentDayBigLabel.textColor = Themes.starluxGold

        if Dimension.isPad {
            currentDayBigLabel.font = Themes.font100FutureM
        }

        currentDayLabel.font = Dimension.isPhoneSE ? Themes.font20FutureM : Themes.font30FutureM
        currentDayLabel.textAlignment = .center
        currentDayLabel.textColor = Themes.starluxGold

        if Dimension.isPad {
            currentDayLabel.font = Themes.font50FutureM
        }

        currentTimeLabel.font = Dimension.isPhoneSE ? Themes.font20NewSec : Themes.font25NewSec
        currentTimeLabel.textAlignment = .center
        currentTimeLabel.textColor = Themes.starluxGold

        if Dimension.isPad {
            currentTimeLabel.font = Themes.font30
        }

        currentTimeContainer.backgroundColor = Themes.starluxLight

        firstPunchLabel.text = PunchTimeDefine.firstTime
        firstPunchLabel.textColor = Themes.starluxBlack
        firstPunchLabel.textAlignment = .left
        firstPunchLabel.font = Dimension.isPhoneSE ? Themes.font10FuturaM : Themes.font12FutureM

        if Dimension.isPad {
            firstPunchLabel.font = Themes.font15FutureM
        }

        firstPunchTimeLabel.text = PunchTimeDefine.noData
        firstPunchTimeLabel.textColor = Themes.starluxBlack
        firstPunchTimeLabel.font = Dimension.isPhoneSE ? Themes.font15 : Themes.font17
        firstPunchTimeLabel.textAlignment = .center

        if Dimension.isPad {
            firstPunchTimeLabel.font = Themes.font20
        }

        lastPunchLabel.text = PunchTimeDefine.lastTime
        lastPunchLabel.textColor = Themes.starluxBlack
        lastPunchLabel.font = Dimension.isPhoneSE ? Themes.font10FuturaM : Themes.font12FutureM
        lastPunchLabel.textAlignment = .left

        if Dimension.isPad {
            lastPunchLabel.font = Themes.font15FutureM
        }

        lastPunchTimeLabel.text = PunchTimeDefine.noData
        lastPunchTimeLabel.textColor = Themes.starluxBlack
        lastPunchTimeLabel.font = Dimension.isPhoneSE ? Themes.font15 : Themes.font17
        lastPunchTimeLabel.textAlignment = .center

        if Dimension.isPad {
            lastPunchTimeLabel.font = Themes.font20
        }
    }

    private func updateDisplayServerTime(date: String) { // Wed Jul 25 2018 12:16:53
        let splitArr = date.components(separatedBy: " ")

        self.currentDayBigLabel.text = splitArr[0].uppercased()
        self.currentDayLabel.text = splitArr[1].uppercased() + "  " + splitArr[2] + "  " + splitArr[3]
    }

    private func updateDisplayTime(date: Date) {
        let dayBigFormat = DateFormatter()
        dayBigFormat.dateFormat = PunchTimeDefine.weekDayFormat
        dayBigFormat.locale = Locale(identifier: "en_US_POSIX")
        let bigToday: String = dayBigFormat.string(from: date)

        let dayFormat = DateFormatter()
        dayFormat.dateFormat = PunchTimeDefine.yearDayFormat
        dayFormat.locale = Locale(identifier: "en_US_POSIX")
        let today: String = dayFormat.string(from: date)

        let timeFormat = DateFormatter()
        timeFormat.dateFormat = PunchTimeDefine.timeFormat
        timeFormat.locale = Locale(identifier: "en_US_POSIX")
        let time: String = timeFormat.string(from: date)

        self.currentDayBigLabel.text = bigToday.uppercased()
        self.currentDayLabel.text = today.uppercased()
        self.currentTimeLabel.text = time
        self.nextTimeSecond = date

        updateLocalCurrentTime()

        //print(currentDayLabel.text)
        //print(currentDayBigLabel.text)
        //print(currentTimeLabel.text)
    }

    private func displayFormApplyUI() {
        formApplyBtn.addTarget(self, action: #selector(clickFormApply), for: .touchUpInside)
        formApplyBtn.setTitleColor(Themes.starluxGold, for: .normal)
        formApplyBtn.titleLabel?.textAlignment = .center
        formApplyBtn.backgroundColor = .white
        formApplyBtn.layer.shadowRadius = 3
        formApplyBtn.layer.shadowOpacity = 0.3
        formApplyBtn.layer.cornerRadius = 10
        formApplyBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.buttonContainer.addSubview(formApplyBtn)

        formApplyLabel.text = PunchTimeDefine.formApply
        formApplyLabel.textAlignment = .center
        //formApplyLabel.font = Dimension.isPhoneSE ? Themes.font12 : Themes.font15
        formApplyLabel.font = Dimension.isPhoneSE ? Themes.font10 : Themes.font12
        formApplyLabel.textColor = Themes.starluxGold
        formApplyImageView.image = UIImage(named: "icon_form_apply")
        formApplyImageView.contentMode = .scaleAspectFit
        formApplyBtn.addSubview(formApplyLabel)
        formApplyBtn.addSubview(formApplyImageView)

    }

    private func displayFormSignUI() {
        formSignBtn.addTarget(self, action: #selector(clickFormSign), for: .touchUpInside)
        formSignBtn.setTitleColor(Themes.starluxGold, for: UIControlState.normal)
        formSignBtn.titleLabel?.textAlignment = .center
        formSignBtn.backgroundColor = .white
        formSignBtn.layer.shadowRadius = 3
        formSignBtn.layer.shadowOpacity = 0.3
        formSignBtn.layer.cornerRadius = 10
        formSignBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.buttonContainer.addSubview(formSignBtn)

        formSignLabel.text = PunchTimeDefine.formSign
        formSignLabel.textAlignment = .center
        //formSignLabel.font = Dimension.isPhoneSE ? Themes.font12 : Themes.font15
        formSignLabel.font = Dimension.isPhoneSE ? Themes.font10 : Themes.font12
        formSignLabel.textColor = Themes.starluxGold
        formSignImageView.image = UIImage(named: "icon_form_sign")
        formSignImageView.contentMode = .scaleAspectFit
        formSignBtn.addSubview(formSignLabel)
        formSignBtn.addSubview(formSignImageView)

        formSignBtn.addSubview(formSignBadge)
    }

    private func displayFormTrackButton() {
        formTrackBtn.addTarget(self, action: #selector(clickFormTrack), for: UIControlEvents.touchUpInside)
        formTrackBtn.setTitleColor(Themes.starluxGold, for: UIControlState.normal)
        formTrackBtn.titleLabel?.textAlignment = .center
        formTrackBtn.backgroundColor = .white
        formTrackBtn.layer.shadowRadius = 3
        formTrackBtn.layer.shadowOpacity = 0.3
        formTrackBtn.layer.cornerRadius = 10
        formTrackBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.buttonContainer.addSubview(formTrackBtn)

        formTrackLabel.text = PunchTimeDefine.formTrack
        formTrackLabel.textAlignment = .center
        formTrackLabel.adjustsFontSizeToFitWidth = true
        //formTrackLabel.font = Dimension.isPhoneSE ? Themes.font12 : Themes.font15
        formTrackLabel.font = Dimension.isPhoneSE ? Themes.font10 : Themes.font12
        formTrackLabel.textColor = Themes.starluxGold
        formTrackImageView.image = UIImage(named: "icon_form_track")
        formTrackImageView.contentMode = .scaleAspectFit
        formTrackBtn.addSubview(formTrackLabel)
        formTrackBtn.addSubview(formTrackImageView)

        formTrackBtn.addSubview(formTrackBadge)
    }

    private func displayPunchTimeOnUI() {
        punchTimeOnBtn.addTarget(self, action: #selector(clickOnWorkPunch), for: UIControlEvents.touchUpInside)
        punchTimeOnBtn.setBackgroundImage(Themes.starluxGoldImg, for: .normal)
        self.view.addSubview(punchTimeOnBtn)

        punchTimeOnLabel.text = PunchTimeDefine.punchTimeOn
        punchTimeOnLabel.font = Dimension.isPhoneSE ? Themes.font17 : Themes.font20
        punchTimeOnLabel.textColor = .white

        punchTimeOnImageView.image = UIImage(named: "icon_clock_on")
        punchTimeOnBtn.addSubview(punchTimeOnLabel)
        punchTimeOnBtn.addSubview(punchTimeOnImageView)
    }

    private func displayPunchTimeOffUI() {
        punchTimeOffBtn.addTarget(self, action: #selector(clickOffWorkPunch), for: UIControlEvents.touchUpInside)
        punchTimeOffBtn.setBackgroundImage(Themes.starluxGrayImg, for: .normal)
        self.view.addSubview(punchTimeOffBtn)

        punchTimeOffLabel.text = PunchTimeDefine.punchTimeOff
        punchTimeOffLabel.font = Dimension.isPhoneSE ? Themes.font17 : Themes.font20
        punchTimeOffLabel.textColor = .white

        punchTimeOffImageView.image = UIImage(named: "icon_clock_off")
        punchTimeOffBtn.addSubview(punchTimeOffLabel)
        punchTimeOffBtn.addSubview(punchTimeOffImageView)
    }

    private func getCurrentLocalDayTime() -> String {
        dateFormatter.dateFormat = PunchTimeDefine.yearNtimeFormat
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let currentTime: String = dateFormatter.string(from: date)
        return currentTime
    }

    private func getCurrentLocalTime() -> String {
        let localDayFormatter = DateFormatter()
        localDayFormatter.dateFormat = PunchTimeDefine.timeFormat
        localDayFormatter.locale = Locale(identifier: "en_US_POSIX")
        let currentTime: String = localDayFormatter.string(from: Date())
        return currentTime
    }

    @objc func clickFormApply() {
        //showOrderMeal()
        let controller = FormApplyEntry()
        mainPage.navigationController?.pushViewController(controller, animated: true)
    }

    @objc func clickFormSign() {
        let controller = FormSignEntry()
        mainPage.navigationController?.pushViewController(controller, animated: true)
    }

    @objc func clickFormTrack() {
        let controller = FormTrackEntry()
        mainPage.navigationController?.pushViewController(controller, animated: true)
    }

    @objc func clickOnWorkPunch() {
        //if (!punchTimeOnBtn.isEnabled)
#if targetEnvironment(simulator)
#else
        if !isMatch {
            showBlockAlert()
            return
        }
#endif
        if let aId = pref.aliasAccount {
            accountId = aId
        }

        HUD.show(.progress)
        punchTimeService.punchTimeWork(accountId: accountId, punchType: 0)
                .subscribe(onNext: { [weak self] result -> Void in
                    let punchTimeDto: PunchTimeDto = result
                    self?.punchTimeExtraResult(punchTimeDto, isOnWork: true)

                }).disposed(by: self.disposeBag)
    }

    func punchTimeExtraResult(_ dto: PunchTimeDto, isOnWork: Bool) {
        if dto.resultMsg.isEmpty == false {
            HUD.hide()
            Alertift.alert(title: dto.resultMsg, message: nil)
                    .buttonTextColor(Themes.starluxGold)
                    .action(.cancel(I18N.key("OK")))
                    .show()

        } else if isOnWork {
            HUD.flash(.labeledSuccess(title: I18N.key("success"), subtitle: dto.punchedServerTime), delay: 1.0)
            guard firstPunchTimeLabel.text == PunchTimeDefine.noData else {
                return
            }
            firstPunchTimeLabel.text = dto.punchedServerTime
            orderMeal(onWorkTime: dto.punchedServerTime)
            pref.setOnWork = "YES"
            punchTimeOnBtn.setBackgroundImage(Themes.starluxGrayImg, for: .normal)
            punchTimeOffBtn.setBackgroundImage(Themes.starluxGoldImg, for: .normal)

        } else if !isOnWork {
            HUD.flash(.labeledSuccess(title: I18N.key("success"), subtitle: dto.punchedServerTime), delay: 1.0)
            lastPunchTimeLabel.text = dto.punchedServerTime

        } else {
            HUD.hide()
        }
    }

    func orderMeal(onWorkTime: String) {
        //todo: check time is before 9:01
        let fakeDayTemplate = "2099/01/01 " //todo: refactor to define
        let onWorkTimeString = fakeDayTemplate + onWorkTime
        let standardTimeString = fakeDayTemplate + "09:01:00"
        let orderTime = Utils.tranceTimeFormat(onWorkTimeString) as Date
        let standardTime = Utils.tranceTimeFormat(standardTimeString) as Date

        let canOrder = orderTime < standardTime

        if canOrder {
            //訂餐基準前
            showOrderMeal()
        } else {
            //不可定
            sendMealDefault()
        }
    }

    func sendMealDefault() {

        let account = pref.aliasAccount
        let cname = pref.aliasName
        let ename = pref.aliasNameEn
        let selection = "Self-prepared"
        let depName = pref.aliasDepartment
        let floorSide = pref.aliasSiteFloor
        //
        //        guard let newAccount = account,
        //            let newCname =
        //            else {
        //            return
        //        }

        let url = URL(string: ChoseLunchDefine.ChoseLunchEndpoint)
        let params = [
            "ep_no": account!,
            "cname": cname!,
            "ename": ename!,
            "selection": selection,
            "dep_name": depName!,
            "site": floorSide!,
        ]

        Alamofire.request(url!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { [weak self] response in
                    print("json = \(response)")
                }
    }

    func showOrderMeal() {
        //let vc = ChoseLunchViewController()
        let vc = ChooseLunchViewController()
        vc.modalPresentationStyle = .overCurrentContext
        mainPage.present(vc, animated: false)
        //print("showOrderMeal")
    }

    @objc func clickOffWorkPunch() {
#if targetEnvironment(simulator)
#else
        if !isMatch {
            showBlockAlert()
            return
        }
#endif
        // server punchTime
        HUD.show(.progress)
        if let aId = pref.aliasAccount {
            accountId = aId
        }
        punchTimeService.punchTimeWork(accountId: accountId, punchType: 1)
                .subscribe(onNext: { [weak self] result -> Void in
                    print("fetchServerTime >> \(result)")
                    let punchTimeDto: PunchTimeDto = result
                    self?.punchTimeExtraResult(punchTimeDto, isOnWork: false)

                }).disposed(by: self.disposeBag)
    }

    func setBtnStatus() {
        if firstPunchTimeLabel.text != PunchTimeDefine.noData {
            pref.setOnWork = "YES"
        } else {
            pref.setOnWork = nil
        }
        //    punchTimeOnBtn.isEnabled = !pref.isOnWorkPunched
        //    punchTimeOffBtn.isEnabled = pref.isOnWorkPunched

        let onBtnColor = pref.isOnWorkPunched ? Themes.starluxGrayImg : Themes.starluxGoldImg
        let offBtnColor = pref.isOnWorkPunched ? Themes.starluxGoldImg : Themes.starluxGrayImg

        punchTimeOnBtn.setBackgroundImage(onBtnColor, for: .normal)
        punchTimeOffBtn.setBackgroundImage(offBtnColor, for: .normal)
    }

    func checkBtnStatus() {
        if firstPunchTimeLabel.text == PunchTimeDefine.noData {
            punchTimeOnBtn.isEnabled = true
        } else {
            punchTimeOnBtn.isEnabled = false
        }
        punchTimeOffBtn.isEnabled = !punchTimeOnBtn.isEnabled
    }

    func cleanSettings() {
        self.pref.setOnWork = nil
    }

    private func getDeviceBssid() {
        if let interface = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interface) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interface, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                if let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString),
                   let interfaceData = unsafeInterfaceData as? [String: AnyObject] {
                    print("BSSID: \(interfaceData["BSSID"]), " +
                            "SSID: \(interfaceData["SSID"]), " +
                            "SSIDDATA: \(interfaceData["SSIDDATA"])")
                    let tmpBssid = (interfaceData["BSSID"] as? String)!
                    self.deviceBSSID = checkBssid(bssid: tmpBssid)
                    print("self.deviceBSSID \(self.deviceBSSID)")
                }
            }
        }
    }

    // MARK: iOS some time will lose one bssid code. so scan than fix if result missing. https://bbs.csdn.net/topics/390966176
    private func checkBssid(bssid: String) -> String {
        let resultBssid = FindMissBssid.standardFormatMAC(bssid)
        return resultBssid!.lowercased()
    }

    fileprivate func updateLocalCurrentTime() {
        Observable<Int>.interval(RxTimeInterval(1), scheduler: MainScheduler()).subscribe(
                onNext: { [weak self] success in
                    //print(success)
                    guard let strongSelf = self else {
                        return
                    }
                    let timeFormat = DateFormatter()
                    timeFormat.dateFormat = PunchTimeDefine.timeFormat
                    timeFormat.locale = Locale(identifier: "en_US_POSIX")
                    let time: String = timeFormat.string(from: Date())
                    strongSelf.currentTimeLabel.text = time

                    let nextTime: Date! = strongSelf.nextTimeSecond
                    let next = nextTime.gregorian + 1.second
                    strongSelf.nextTimeSecond = next.date
                }
                , onError: { error in }
        ).disposed(by: self.disposeBag)
    }

    //MARK: for Pad orientation change
    // using "Screens.height" cus system screens update will delay.
    @objc func deviceRotated() {
        if !Dimension.isPad {
            return
        }

        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            punchTimeOnBtn.snp.updateConstraints { make in
                make.leading.equalToSuperview()
                make.height.equalTo(100)
                make.width.equalTo(Screens.height / 2)
                make.bottom.equalTo(buttonContainer.snp.top)
            }
            punchTimeOffBtn.snp.updateConstraints { make in
                make.trailing.equalToSuperview()
                make.height.equalTo(100)
                make.width.equalTo(Screens.height / 2)
                make.bottom.equalTo(buttonContainer.snp.top)
            }

            formApplyBtn.snp.updateConstraints { make in
                make.width.equalTo(Screens.height / 3 - 15)
            }
            formTrackBtn.snp.updateConstraints { make in
                make.width.equalTo(Screens.height / 3 - 15)
            }
            formSignBtn.snp.updateConstraints { make in
                make.width.equalTo(Screens.height / 3 - 15)
            }

            currentDayBigLabel.snp.updateConstraints { make in
                make.top.equalTo(45)
            }
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            punchTimeOnBtn.snp.updateConstraints { make in
                make.leading.equalToSuperview()
                make.height.equalTo(100)
                make.width.equalTo(Screens.width / 2)
                make.bottom.equalTo(buttonContainer.snp.top)
            }
            punchTimeOffBtn.snp.updateConstraints { make in
                make.trailing.equalToSuperview()
                make.height.equalTo(100)
                make.width.equalTo(Screens.width / 2)
                make.bottom.equalTo(buttonContainer.snp.top)
            }

            formApplyBtn.snp.updateConstraints { make in
                make.width.equalTo(Screens.width / 3 - 15)
            }
            formTrackBtn.snp.updateConstraints { make in
                make.width.equalTo(Screens.width / 3 - 15)
            }
            formSignBtn.snp.updateConstraints { make in
                make.width.equalTo(Screens.width / 3 - 15)
            }

            currentDayBigLabel.snp.updateConstraints { make in
                make.top.equalTo(150)
            }
        }
    }

}

private typealias CompareWifi = PunchTimeEntry

extension CompareWifi {
    fileprivate func wifiListCompare() {
        for element in self.bssidList {
            print("deviceBSSID >> \(self.deviceBSSID)  vs  wifi: \(element)")
            if element == self.deviceBSSID {
                print("element >> \(element)")
                self.isMatch = true
                break
            }
        }

#if targetEnvironment(simulator) // is Simulator
#else
        if !self.isMatch {
            showBlockAlert()
        }
#endif

    }

    fileprivate func showBlockAlert() {
        Alertift.alert(title: I18N.key("Unable to connect to STARLUX WiFi"), message: nil)
                .buttonTextColor(Themes.starluxGold)
                .action(.default(I18N.key("Reconnect"))) { [weak self] (_, _, _) in
                    self?.checkNetworkState()
                }
                .action(.cancel(I18N.key("cancel")))
                .show()
    }

    fileprivate func showUnreachAlert() {
        let alert = UIAlertController(title: "Alert",
                message: "No Network",
                preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

private typealias LayoutSetting = PunchTimeEntry

extension LayoutSetting {
    fileprivate func initLayout() {
        var extraDimension = Dimension.isPhoneSE ? 20 : 45

        if Dimension.isPad {
            extraDimension = 150
        }

        currentDayBigLabel.snp.makeConstraints { make in
            make.top.equalTo(extraDimension)
            make.centerX.equalToSuperview()
        }
        currentDayLabel.snp.makeConstraints { make in
            make.top.equalTo(currentDayBigLabel.snp.bottom).offset(5)
            make.centerX.equalToSuperview()
        }
        currentTimeContainer.snp.makeConstraints { make in
            make.top.equalTo(currentDayLabel.snp.bottom).offset(10)
            make.leading.equalTo(currentDayLabel)
            make.trailing.equalTo(currentDayLabel)
            if Dimension.isPhoneSE {
                make.height.equalTo(45)
            } else {
                make.height.equalTo(55)
            }
        }
        currentTimeLabel.snp.makeConstraints { make in
            make.centerX.equalTo(currentTimeContainer)
            make.centerY.equalTo(currentTimeContainer).offset(3)
        }

        var extraDimon = Dimension.isPhoneSE ? 10 : 20 // base currentTimeContainer - leading & trailing
        if Dimension.isPad {
            extraDimon = 40
        }
        firstPunchLabel.snp.makeConstraints { make in
            make.top.equalTo(currentTimeContainer.snp.bottom).offset(20)

            make.centerX.equalTo(lastPunchLabel)
        }
        firstPunchTimeLabel.snp.makeConstraints { make in
            make.centerY.equalTo(firstPunchLabel)
            make.trailing.equalTo(currentTimeContainer).offset(-extraDimon)
        }
        separateLine.snp.makeConstraints { make in
            make.top.equalTo(firstPunchLabel.snp.bottom).offset(10)
            make.leading.trailing.equalTo(currentTimeContainer)
            make.height.equalTo(1)
        }
        lastPunchLabel.snp.makeConstraints { make in
            if Dimension.isPhoneSE || Dimension.isPhone {
                make.top.equalTo(separateLine.snp.bottom).offset(10)
            } else {
                make.top.equalTo(separateLine.snp.bottom).offset(20)
            }

            make.leading.equalTo(currentTimeContainer).offset(extraDimon)
        }
        lastPunchTimeLabel.snp.makeConstraints { make in
            make.centerY.equalTo(lastPunchLabel)
            make.trailing.equalTo(currentTimeContainer).offset(-extraDimon)
        }
        //---------------------------------------------------------------------------//
        punchTimeOnBtn.snp.makeConstraints { make in
            make.leading.equalTo(view)
            make.height.equalTo(100)
            make.width.equalTo(Screens.width / 2)
            make.bottom.equalTo(buttonContainer.snp.top)
        }
        punchTimeOnImageView.snp.makeConstraints { make in
            make.centerX.equalTo(punchTimeOnBtn)
            make.size.equalTo(CGSize(width: 30, height: 30))
            make.bottom.equalTo(punchTimeOnLabel.snp.top).offset(-5)
        }
        punchTimeOnLabel.snp.makeConstraints { make in
            make.centerX.equalTo(punchTimeOnBtn)
            make.centerY.equalTo(punchTimeOnBtn).offset(20)
        }

        punchTimeOffBtn.snp.makeConstraints { make in
            make.trailing.equalTo(view)
            make.height.equalTo(100)
            make.width.equalTo(Screens.width / 2)
            make.bottom.equalTo(buttonContainer.snp.top)
        }
        punchTimeOffImageView.snp.makeConstraints { make in
            make.centerX.equalTo(punchTimeOffBtn)
            make.size.equalTo(CGSize(width: 30, height: 30))
            make.bottom.equalTo(punchTimeOffLabel.snp.top).offset(-5)
        }
        punchTimeOffLabel.snp.makeConstraints { make in
            make.centerX.equalTo(punchTimeOffBtn)
            make.centerY.equalTo(punchTimeOffBtn).offset(20)
        }

        buttonContainer.snp.makeConstraints { make in
            make.top.equalTo(formSignBtn).offset(-20)
            make.leading.equalTo(punchTimeOnBtn)
            make.trailing.equalTo(0)
            make.bottom.equalTo(view.safeArea.bottom)
        }

        formApplyBtn.snp.makeConstraints { make in
            make.leading.equalTo(15)
            make.centerY.equalTo(formSignBtn)
            make.trailing.equalTo(formSignBtn.snp.leading).offset(-10)
            make.width.equalTo(Screens.width / 3 - 15)
            make.top.equalTo(formSignBtn)
            make.bottom.equalTo(formSignBtn)
        }
        formSignBadge.snp.makeConstraints { make in
            make.top.equalTo(formSignBtn).offset(15)
            make.trailing.equalTo(formSignBtn).offset(-15)
            make.size.equalTo(CGSize(width: 10, height: 10))
        }
        formApplyImageView.snp.makeConstraints { make in
            make.centerX.equalTo(formApplyBtn)
            make.size.equalTo(CGSize(width: 50, height: 50))
            make.bottom.equalTo(formApplyLabel.snp.top).offset(3)
        }
        formApplyLabel.snp.makeConstraints { make in
            make.centerX.equalTo(formApplyBtn)
            make.leading.trailing.equalTo(formApplyBtn).inset(10)
            make.centerY.equalTo(formApplyBtn).offset(20)
        }

        let extraBtnHeight = Dimension.isPhoneX ? -50 : -30
        formSignBtn.snp.makeConstraints { make in
            make.centerX.equalTo(buttonContainer)
            //make.centerY.equalTo(buttonContainer).offset(-10)
            make.width.equalTo(Screens.width / 3 - 15)
            make.height.equalTo(80)
            make.bottom.equalTo(buttonContainer).offset(extraBtnHeight)
        }
        formSignImageView.snp.makeConstraints { make in
            make.centerX.equalTo(formSignBtn)
            make.size.equalTo(CGSize(width: 50, height: 50))
            make.bottom.equalTo(formSignLabel.snp.top).offset(3)
        }
        formSignLabel.snp.makeConstraints { make in
            //make.centerX.equalTo(formSignBtn)
            make.leading.trailing.equalTo(formSignBtn).inset(10)
            make.centerY.equalTo(formSignBtn).offset(20)
        }
        formTrackBadge.snp.makeConstraints { make in
            make.top.equalTo(formTrackBtn).offset(15)
            make.trailing.equalTo(formTrackBtn).offset(-15)
            make.size.equalTo(CGSize(width: 10, height: 10))
        }
        formTrackBtn.snp.makeConstraints { make in
            make.centerY.equalTo(formSignBtn)
            make.width.equalTo(Screens.width / 3 - 15)
            make.leading.equalTo(formSignBtn.snp.trailing).offset(10)
            make.top.equalTo(formSignBtn)
            make.bottom.equalTo(formSignBtn)
        }
        formTrackImageView.snp.makeConstraints { make in
            make.centerX.equalTo(formTrackBtn)
            make.size.equalTo(CGSize(width: 50, height: 50))
            make.bottom.equalTo(formTrackLabel.snp.top).offset(3)
        }
        formTrackLabel.snp.makeConstraints { make in
            //make.centerX.equalTo(formTrackBtn)
            make.leading.trailing.equalTo(formTrackBtn).inset(10)
            make.centerY.equalTo(formTrackBtn).offset(20)
        }
    }
}

private typealias LegacyPlace = PunchTimeEntry

extension LegacyPlace {
    func changeLocale() {
        Alertift.actionSheet(message: "Which food do you like? (change locale)")
                .actions(["🍣 en", "🍎 zh-Hant"])
                .action(.cancel("None of them"))
                .finally { action, index in
                    if action.style == .cancel {
                        return
                    }

                    if index == 0 {
                        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                    }

                    if index == 1 {
                        UserDefaults.standard.set(["zh-Hant"], forKey: "AppleLanguages")
                    }

                    Alertift.alert(message: "\(index). \(action.title!) take_effect_after_restart = 请重新启动后生效")
                            .action(.destructive("OK")) { (_, _, _) in
                                exit(0)
                            }
                            .show()
                }
                .show()
    }
}
