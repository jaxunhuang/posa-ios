//
// Created by STARLUX on 1/6/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

class PunchTimeDetailPage: UIViewController {

  fileprivate let todayPunchTimesTableView = PunchTimeDetailList()

  fileprivate let backBtn = UIButton(type: .system)
  fileprivate let infoBtn = UIButton(type: .system)

  fileprivate var todayString = ""
  fileprivate var weekdayString = ""

  fileprivate let currentDayLabel:UILabel = UILabel(frame: .zero)
  fileprivate let currentDayBigLabel:UILabel = UILabel(frame: .zero)

  fileprivate let workTimeLabel = UILabel(frame: .zero)
  fileprivate let onWorkTimeLabel = UILabel(frame: .zero)
  fileprivate let timeSeparateLine = UIView()
  fileprivate let offTimeLabel = UILabel(frame: .zero)
  fileprivate let separateLine = UIView()
  fileprivate let downSeparateLine = UIView()

  fileprivate let punchTimeDetailLabel = UILabel(frame: .zero)

  fileprivate let aliasIcon = UIImageView()
  fileprivate let aliasName = UILabel(frame: .zero)
  fileprivate let aliasNumber = UILabel(frame: .zero)
  fileprivate let aliasUnit = UILabel(frame: .zero)

  override func viewDidAppear(_ animated:Bool) {
    super.viewDidAppear(animated)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    setUI()
  }

  private func setUI() {

    todayPunchTimesTableView.updateInfo(["08:20:21", "08:20:41", "09:20:21"])
    self.view.addSubview(todayPunchTimesTableView.view)


    backBtn.setTitleColor(Themes.starluxGold, for: UIControlState.normal)
    backBtn.setTitle("Logout", for: UIControlState.normal) // todo:i18n
    backBtn.addTarget(self, action: #selector(clickLogout), for: UIControlEvents.touchUpInside)
    self.view.addSubview(backBtn)

    infoBtn.setTitle("info", for: UIControlState.normal) // todo:i18n
    infoBtn.setTitleColor(Themes.starluxGold, for: UIControlState.normal)
    infoBtn.addTarget(self, action: #selector(showCal), for: UIControlEvents.touchUpInside)
    self.view.addSubview(infoBtn)


    currentDayLabel.text = todayString
    currentDayLabel.font = Themes.font40
    currentDayLabel.textAlignment = .center
    currentDayLabel.textColor = Themes.starluxGold
    self.view.addSubview(currentDayLabel)

    currentDayBigLabel.text = weekdayString
    currentDayBigLabel.font = Themes.font100
    currentDayBigLabel.textAlignment = .center
    currentDayBigLabel.textAlignment = .center
    currentDayBigLabel.textColor = Themes.starluxGold
    self.view.addSubview(currentDayBigLabel)

    workTimeLabel.text = I18N.key("")
    workTimeLabel.textAlignment = .left
    workTimeLabel.font = Themes.font20
    self.view.addSubview(workTimeLabel)

    onWorkTimeLabel.text = PunchTimeDefine.noData
    onWorkTimeLabel.font = Themes.font20
    onWorkTimeLabel.textAlignment = .right
    self.view.addSubview(onWorkTimeLabel)

    timeSeparateLine.backgroundColor = .black
    self.view.addSubview(timeSeparateLine)

    offTimeLabel.text = PunchTimeDefine.noData
    offTimeLabel.font = Themes.font20
    offTimeLabel.textAlignment = .right
    self.view.addSubview(offTimeLabel)

    separateLine.backgroundColor = .darkGray
    self.view.addSubview(separateLine)

    punchTimeDetailLabel.text = I18N.key("details")
    punchTimeDetailLabel.textAlignment = .left
    punchTimeDetailLabel.font = Themes.font20
    self.view.addSubview(punchTimeDetailLabel)

    downSeparateLine.backgroundColor = .darkGray
    self.view.addSubview(downSeparateLine)


    let fakeUrl = URL(string: "https://necropedia.org/jdd/public/documents/celebrities/2825.jpg")
    aliasIcon.kf.setImage(with: fakeUrl, placeholder: UIImage(named:"aliasDefault"))
    //aliasIcon.backgroundColor = .yellow
    aliasIcon.layer.cornerRadius = 50
    aliasIcon.layer.masksToBounds = true
    self.view.addSubview(aliasIcon)

    aliasName.text = "某一位"
    aliasName.font = Themes.font20
    self.view.addSubview(aliasName)

    aliasNumber.text = "1800123"
    aliasNumber.font = Themes.font20
    self.view.addSubview(aliasNumber)

    aliasUnit.text = "公共關係室"
    aliasUnit.font = Themes.font20
    self.view.addSubview(aliasUnit)

    initLayout()
  }

    @objc func clickLogout() {
    navigationController?.popViewController(animated: true)

//    let transition = CATransition()
//    transition.duration = 0.3
//    transition.type = kCATransitionPush
//    transition.subtype = kCATransitionFromLeft
//    self.view.window!.layer.add(transition, forKey: kCATransition)
//
//    dismiss(animated: false, completion: nil)
  }

    @objc func showCal() {
    //todo show calender to change day.
  }

    required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  init(_ today:String, _ weekday:String) {
    super.init(nibName: nil, bundle: nil)

    self.todayString = today
    self.weekdayString = weekday
  }
}


private typealias LayoutSetting = PunchTimeDetailPage

extension LayoutSetting {
  fileprivate func initLayout() {

    backBtn.snp.makeConstraints { make in
      make.top.equalTo(20)
      make.leading.equalTo(10)
      make.height.greaterThanOrEqualTo(40)
    }
    infoBtn.snp.makeConstraints { make in
      make.top.equalTo(20)
      make.trailing.equalTo(-10)
      make.height.greaterThanOrEqualTo(40)
    }
    currentDayLabel.snp.makeConstraints { make in
      make.top.equalTo(100)
      make.leading.trailing.centerX.equalToSuperview()
    }
    currentDayBigLabel.snp.makeConstraints { make in
      make.top.equalTo(currentDayLabel.snp.bottom).offset(40)
      make.leading.trailing.centerX.equalToSuperview()
    }
    workTimeLabel.snp.makeConstraints { make in
      make.leading.equalTo(70)
      make.trailing.equalTo(onWorkTimeLabel.snp.leading)
      make.top.equalTo(currentDayBigLabel.snp.bottom).offset(20)
    }
    onWorkTimeLabel.snp.makeConstraints { make in
      make.trailing.equalTo(timeSeparateLine.snp.leading).offset(-10)
      //make.leading.equalTo(onWorkLabel.snp.trailing)
      make.top.equalTo(currentDayBigLabel.snp.bottom).offset(20)
    }
    timeSeparateLine.snp.makeConstraints { make in
      make.trailing.equalTo(offTimeLabel.snp.leading).offset(-10)
      make.centerY.equalTo(offTimeLabel)
      make.width.equalTo(10)
      make.height.equalTo(1)
    }
    offTimeLabel.snp.makeConstraints { make in
      make.trailing.equalTo(-50)
      //make.leading.equalTo(timeSeparateLine.snp.trailing)
      make.top.equalTo(currentDayBigLabel.snp.bottom).offset(20)
    }
    separateLine.snp.makeConstraints { make in
      make.leading.equalTo(40)
      make.trailing.equalTo(-40)
      make.height.equalTo(1)
      make.top.equalTo(workTimeLabel.snp.bottom).offset(20)
      //make.bottom.equalTo(lastPunchLabel.snp.top).offset(-20)
    }
    punchTimeDetailLabel.snp.makeConstraints { make in
      make.leading.equalTo(70)
      make.trailing.equalTo(onWorkTimeLabel.snp.leading)
      make.top.equalTo(separateLine.snp.bottom).offset(20)
    }
    todayPunchTimesTableView.view.snp.makeConstraints { make in
      make.top.equalTo(separateLine.snp.bottom).offset(20)
      make.trailing.equalTo(separateLine)
      make.width.equalTo(180)
      make.bottom.equalTo(downSeparateLine.snp.top).offset(-10)
    }
    downSeparateLine.snp.makeConstraints { make in
      make.leading.equalTo(40)
      make.trailing.equalTo(-40)
      make.height.equalTo(1)
      make.bottom.equalTo(aliasIcon.snp.top).offset(-20)
    }
    aliasIcon.snp.makeConstraints { make in
      make.leading.equalTo(separateLine)
      make.bottom.equalTo(-30)
      make.size.equalTo(CGSize(width: 100, height: 100))
    }
    aliasName.snp.makeConstraints { make in
      make.top.equalTo(aliasIcon).offset(20)
      make.leading.equalTo(aliasIcon.snp.trailing).offset(20)
    }
    aliasNumber.snp.makeConstraints { make in
      make.top.equalTo(aliasName)
      make.leading.equalTo(aliasName.snp.trailing).offset(20)
    }
    aliasUnit.snp.makeConstraints { make in
      make.top.equalTo(aliasName.snp.bottom).offset(15)
      make.leading.equalTo(aliasName)
    }
  }
}
