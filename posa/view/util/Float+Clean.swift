//
// Created by Jaxun on 2018/7/26.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit

// remove a decimal from a float if the decimal is equal to 0
// https://stackoverflow.com/questions/31390466/swift-how-to-remove-a-decimal-from-a-float-if-the-decimal-is-equal-to-0/41372126#41372126
extension Float {
  var clean: String {
    return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
  }
}