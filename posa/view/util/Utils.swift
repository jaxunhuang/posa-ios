//
// Created by STARLUX on 1/4/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SwiftTheme

struct Utils {

  static func tranceTimeFormat(_ time:String) -> Date {
    let dayFormat = DateFormatter()
    dayFormat.dateFormat = PunchTimeDefine.yearNtimeFormat
    dayFormat.locale = Locale.current
    let today:Date = dayFormat.date(from: time)!
    return today
  }

  static func getTimeFormat(_ date:Date) -> String {
    let dayFormat = DateFormatter()
    dayFormat.dateFormat = "ss:ss:ss"
    dayFormat.locale = Locale.current
    let today:String = dayFormat.string(from: date)
    return today
  }

  static func getFullDateFormat(_ date:Date) -> String {
    let dayFormat = DateFormatter()
    dayFormat.dateFormat = "yyyy/MM/dd"
    dayFormat.locale = Locale.current
    let today:String = dayFormat.string(from: date)
    return today
  }

  static func getCalendarSelectDateFormat(_ date:Date) -> String {
    let dayFormat = DateFormatter()
    dayFormat.dateFormat = "MMM  dd  yyyy"
    //dayFormat.locale = Locale.current
    dayFormat.locale = Locale(identifier: "en_US_POSIX")
    let today:String = dayFormat.string(from: date)
    return today
  }

  static func getYear(_ date:Date) -> String {
    let dayFormat = DateFormatter()
    dayFormat.dateFormat = "yyyy"
    let today:String = dayFormat.string(from: date)
    return today
  }

  static func getMonth(_ date:Date) -> String {
    let dayFormat = DateFormatter()
    dayFormat.dateFormat = "MM"
    //dayFormat.locale = Locale.current
    dayFormat.locale = Locale(identifier: "en_US_POSIX")
    let today:String = dayFormat.string(from: date)
    return today
  }

}