//
// Created by liq on 2017/1/3.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import UIKit
import CoreGraphics
import CoreGraphics.CGGradient
extension UIColor {

  convenience init(red r:Int, green g:Int, blue b:Int) {
    assert(r >= 0 && r <= 255, "Invalid red component")
    assert(g >= 0 && g <= 255, "Invalid green component")
    assert(b >= 0 && b <= 255, "Invalid blue component")

    self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
  }

  convenience init(hex hexColor:Int) {
    self.init(hex: hexColor, alpha: 1.0)
  }

  convenience init(_ hexColor:Int) {
    self.init(hex: hexColor, alpha: 1.0)
  }

  convenience init(hex hexColor:Int, alpha alphaValue:Double) {
    self.init(
        red: CGFloat((hexColor >> 16) & 0xff) / 255,
        green: CGFloat((hexColor >> 8) & 0xff) / 255,
        blue: CGFloat(hexColor & 0xff) / 255,
        alpha: CGFloat(alphaValue))
  }

  static func generateGradient(from fromColor:UIColor,to toColor:UIColor,frame:CGRect) -> CAGradientLayer {
    let gradient:CAGradientLayer = CAGradientLayer()

    gradient.colors = [fromColor.cgColor, toColor.cgColor]
    gradient.locations = [0.05, 0.95]
    gradient.startPoint = CGPoint(x:0, y:0)
    gradient.endPoint = CGPoint(x:1, y:1)
    return gradient
  }

}
