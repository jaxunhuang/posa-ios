//
// Created by liq on 2017/1/9.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import UIKit

extension String {

  func heightForComment(font:UIFont, width:CGFloat) -> CGFloat {

    let rect = NSString(string: self).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)),
        options: .usesLineFragmentOrigin,
        attributes: [.font: font],
        context: nil)

    return ceil(rect.height)
    
  }

  func replace(target: String, withString: String) -> String
  {
    return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
  }

  //將原始的URL編碼為合法的URL
  func urlEncoded() -> String {

    /*
       CharacterSet.urlHostAllowed: 被轉義的字符有  "#%/<>?@\^`\{\|\}

       Ref -> https://stackoverflow.com/questions/24551816/swift-encode-url
        解決了缺少的 /+=
       %2F <- /
       %2B <- +
       %3D <- =
    */

    let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)
    let encodeUrlString = self.addingPercentEncoding(withAllowedCharacters:allowedCharacterSet)

    return encodeUrlString ?? ""
  }

}
