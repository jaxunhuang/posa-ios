//
// Created by STARLUX on 1/4/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation

struct Dimension {
  static let MATERIAL_SPACING_4 = 4
  static let MATERIAL_SPACING_8 = 8
  static let MATERIAL_SPACING_16 = 16
  static let MATERIAL_SPACING_24 = 24
  static let MATERIAL_SPACING_48 = 48

  static let SPACING_10 = 10
  static let SPACING_20 = 20
  static let SPACING_40 = 40
  static let SPACING_100 = 40

  static let BASE_DURATION = 0.3

  static var isPad:Bool {
    return Screens.height > 1000 // Pad mini, Pad 9.7 = 1024, Pad 10.5 = 1112, Pad 12.9 = 1366
  }
  static var isPhone:Bool {
    return Screens.height == 667
  }
  static var isPhoneSE:Bool {
    return Screens.height == 568
  }
  static var isPhonePlus:Bool {
    return Screens.height == 736
  }
  static var isPhoneX:Bool {
    return Screens.height == 812
  }
}