//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit

struct Screens {
  static let width = UIScreen.main.bounds.width
  static let height = UIScreen.main.bounds.height
}
