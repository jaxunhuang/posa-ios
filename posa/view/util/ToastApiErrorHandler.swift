//
// Created by jaxun on 12/30/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import TTGSnackbar
import RxSwift
import UIKit
import SwiftTheme
import PKHUD

private func toast(_ msg:String) {

  // api error handler may be called before key window initialization, so
  // we have to do one frame delay and executed in main thread to prevent crash
  Observable.just(()).delay(0.03, scheduler: MainScheduler.instance).subscribe(onNext: {
    let bar:TTGSnackbar = TTGSnackbar.init(message: msg, duration: .short)
    //bar.theme_backgroundColor = Themes.errorBackgroundColor
    bar.backgroundColor = Themes.warningRed
    bar.show()
  })
}

func toastApiErrorHandler(apiError:ApiError) {

  HUD.hide()

  switch apiError {
  case let .programBugError(afError):
#if DEBUG
    //fatalError("calling api signature) error: \(afError)")
#else
    toast("*** " + NSLocalizedString("retrofit_service_error", comment: ""))
#endif
  case .serviceError:
    toast(NSLocalizedString("retrofit_service_error", comment: ""));
  case .authenticationError:
    toast(NSLocalizedString("retrofit_authentication_error", comment: ""))
  case .httpStatusError:
    toast(NSLocalizedString("retrofit_http_status_error", comment: ""))
  case .internalServerError:
    toast(NSLocalizedString("retrofit_internal_server_error", comment: ""))
  case .networkError:
    toast(NSLocalizedString("retrofit_network_error", comment: ""))
  case .resourceNotFoundError:
    toast(NSLocalizedString("retrofit_resource_not_found_error", comment: ""))
  }
}
