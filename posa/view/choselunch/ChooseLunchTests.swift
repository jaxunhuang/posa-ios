//
//  ChooseLunchTests.swift
//  posa
//
//  Created by Jaxun on 2018/8/22.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import Quick
import RxSwift
import RxTest
@testable import posa

class ChooseLunchTestCase: QuickSpec {

    var vm: ChooseLunchVMP! = ChooseLunchViewModel()
    var disposeBag = DisposeBag()

    override func spec() {


        describe("btnEvent") {

            //verified api parameter, hud progress, hud flash, view dismiss.
            it("clickVegetableBtn") {
                self.btnTest(self.vm.didPressVegetableBtn)
            }
            it("ClickMeatBtn") {
                self.btnTest(self.vm.didPressMeatBtn)
            }
            it("ClickSelfPrepareBtn") {
                self.btnTest(self.vm.didPressSelfPrepareBtn)
            }

            it("click Meat btn params") {

            }
        }

        describe("labels", closure: {
            //            beforeEach {
            //                self.vm = ChooseLunchViewModel()
            //            }
            context("stringValue", closure: {
                it("chooseTitle") {
                    self.vm.chooseTitle.subscribe(onNext: { title in
                        XCTAssertEqual(title, "Lunch Selection")
                    }).disposed(by: self.disposeBag)
                }
                it("vegetableNameTitle") {
                    self.vm.vegetableNameTitle.subscribe(onNext: { title in
                        XCTAssertEqual(title, "Vegetarian")
                    }).disposed(by: self.disposeBag)
                }
                it("vegetableBtnImageName") {
                    self.vm.vegetableBtnImageName.subscribe(onNext: { title in
                        XCTAssertEqual(title, "icon_vegi")
                    }).disposed(by: self.disposeBag)
                }
                it("meatNameTitle") {
                    self.vm.meatNameTitle.subscribe(onNext: { title in
                        XCTAssertEqual(title, "Regular")
                    }).disposed(by: self.disposeBag)
                }
                it("meatBtnImageName") {
                    self.vm.meatBtnImageName.subscribe(onNext: { title in
                        XCTAssertEqual(title, "icon_meat")
                    }).disposed(by: self.disposeBag)
                }
                it("selfPrepareNameTitle") {
                    self.vm.selfPrepareNameTitle.subscribe(onNext: { title in
                        XCTAssertEqual(title, "Self-prepared")
                    }).disposed(by: self.disposeBag)
                }
                it("selfPrepareBtnImageName") {
                    self.vm.selfPrepareBtnImageName.subscribe(onNext: { title in
                        XCTAssertEqual(title, "icon_self")
                    }).disposed(by: self.disposeBag)
                }
            })
        })
    }

    private func btnTest(_ btnObserver: PublishSubject<Void>) {
        let scheduler = TestScheduler(initialClock: 0)

        //user behavior
        let userEvent: [Recorded<Event<Void>>] = [.next(0, Void()), .next(100, Void()), .next(200, Void())]

        let userDidPressBtnObservable = scheduler.createHotObservable(userEvent)

        userDidPressBtnObservable
            .bind(to: btnObserver).disposed(by: self.disposeBag)

        //expected behavior
        let expectedEvent: [Recorded<Event<Bool>>] = [.next(0, true), .next(100, true), .next(200, true)]

        let eventResultObserver = scheduler.createObserver(Bool.self)

        Observable.zip(self.vm.hudShow, self.vm.hudFlash, self.vm.didClose)
            .map { _, _, _ in
                true
            }
            .subscribe(eventResultObserver)
            .disposed(by: self.disposeBag)

        scheduler.start()

        Observable.of(true)
            .delay(0.5, scheduler: MainScheduler.instance)
            .do(onNext: { _ in
                XCTAssertEqual(eventResultObserver.events, expectedEvent)
            })
            .subscribe().disposed(by: self.disposeBag)
    }
}


//    class RegisterTests: XCTestCase {
//
//        //這個是和用戶註冊ViewController綁定的ViewModel
//        var registerViewModel = RegisterViewModel()
//        var disposeBag = DisposeBag()
//
//        func testInputPhoneNumber() {
//            *//*
//             * TestScheduler相當於一個測試控制中心
//             *它會模擬響應式環境中，用戶在某一時刻進行某個交互（輸入）的情況
//             *參數initialClock是虛擬時間，建議從0開始
//             * *//*
//            let scheduler = TestScheduler(initialClock: 0)
//            *//*
//             *建立一個可以得到結果的Observer，這個角色相當於真實世界中的「我」
//             * Bool.self又是什麼呢？相當於「我」只觀察「是」或「否」兩種結果
//             * *//*
//            let canSendObserver = scheduler.createObserver(Bool.self)
//
//            *//*
//             *從scheduler創建熱信號，通過next這個方法創建。
//             * next(100, ("1862222"))是什麼意思？
//             *表示在100這個時間點，信號源發送了"1862222"這個信息。
//             *換成真實環境，就是用戶在100這個時間點輸入了"1862222"這個字符串。
//             *當然，真實環境下除非用戶用粘帖的方式，不然是不可能一次輸入完的。
//             *為模擬真實情況，分別在200、300這個時間點輸入更多信息。
//             *其中300這個點，還往前貼刪了一個字符。
//             * *//*
//            let inputPhoneNumberObservable = scheduler.createHotObservable([next(100, ("1862222")),
//                                                                            next(200, ("18622222222")),
//                                                                            next(300, ("1862222222"))])
//
//            *//*
//             *將registerViewModel的canSendCode訂閱至前面創建的canSendObserver上
//             *注：canSendCode是一個Observable<Bool>
//             * *//*
//            self.registerViewModel.canSendCode
//                .subscribe(canSendObserver)
//                .addDisposableTo(self.disposeBag)
//
//            *//*
//             *最後一步，要將前面創建的熱信號源，綁定到registerViewModel的phoneNumber上，
//             *這樣信號發出來後，我們的ViewModel才會有相應的反應：即canSendCode這個Observable對應的變化
//             * *//*
//            inputPhoneNumberObservable
//                .bind(to: self.registerViewModel.phoneNumber)
//                .addDisposableTo(self.disposeBag)
//
//            //一切準備就緒，開始吧！
//            scheduler.start()
//
//            *//*
//             *這是期望的測試結果
//             *在0這個時間點，由於沒有輸入，canSendCode是false
//             * 100這個時間點，輸入了“1862222”，手機號不完整，canSendCode也是false
//             * 200這個時間點，手機號完整了，自然是true
//             * 300這個時間點，啊，手機號輸錯了，刪除一個字符，又變成false了
//             * *//*
//            let expectedCanSendEvents = [
//                next(0, false),
//                next(100, false),
//                next(200, true),
//                next(300, false),
//            ]
//
//            // Assert Equal一下Observer真實的events（結果）和期望的結果，一樣就測試通過
//            XCTAssertEqual(canSendObserver.events, expectedCanSendEvents)
//        }
//    }

