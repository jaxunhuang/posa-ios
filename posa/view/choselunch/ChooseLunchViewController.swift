//
//  ChooseLunchViewController.swift
//  posa
//
//  Created by Jaxun on 2018/8/21.
//  Copyright © 2018 STARLUX. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import RxSwift
import RxCocoa

@IBDesignable
class ChooseLunchViewController: UIViewController {

    fileprivate let vm: ChooseLunchVMP = Injector.inject()

    fileprivate lazy var tap = UITapGestureRecognizer()

    @IBOutlet weak var contentView: UIView! {
        willSet {
            newValue.layer.cornerRadius = 20
            newValue.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var chooseTitle: UILabel! {
        willSet {
            newValue.font = Dimension.isPad ? Themes.font40 : Themes.font20Bold
            newValue.minimumScaleFactor = 0.5
            newValue.adjustsFontSizeToFitWidth = true
        }
    }

    @IBOutlet weak var vegetableBtn: UIButton!
    @IBOutlet weak var meatBtn: UIButton!
    @IBOutlet weak var selfPrepareBtn: UIButton!

    @IBOutlet weak var vegetableName: UILabel! {
        willSet {
            newValue.font = Dimension.isPad ? Themes.font30 : Themes.font12
            newValue.textAlignment = .center
            newValue.textColor = Themes.starluxGold
            newValue.minimumScaleFactor = 0.5
            newValue.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet weak var meatName: UILabel! {
        willSet {
            newValue.font = Dimension.isPad ? Themes.font30 : Themes.font12
            newValue.textAlignment = .center
            newValue.textColor = Themes.starluxGold
            newValue.minimumScaleFactor = 0.5
            newValue.adjustsFontSizeToFitWidth = true
        }
    }

    @IBOutlet weak var selfPrepareName: UILabel! {
        willSet {
            newValue.font = Dimension.isPad ? Themes.font30 : Themes.font12
            newValue.textAlignment = .center
            newValue.textColor = Themes.starluxGold
            newValue.minimumScaleFactor = 0.5
            newValue.adjustsFontSizeToFitWidth = true
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(tap)

        self.view.backgroundColor = Themes.blurBg

        vm.chooseTitle
            .bindTo(chooseTitle.rx.text).disposed(by: disposeBag)

        vm.meatBtnImageName
            .map {
                UIImage(named: $0)
            }
            .subscribe(onNext: { [weak self] image in
                self?.meatBtn.setImage(image, for: .normal)
            }).disposed(by: disposeBag)

        vm.vegetableBtnImageName
            .map {
                UIImage(named: $0)
            }
            .subscribe(onNext: { [weak self] image in
                self?.vegetableBtn.setImage(image, for: .normal)
            }).disposed(by: disposeBag)

        vm.selfPrepareBtnImageName
            .map {
                UIImage(named: $0)
            }
            .subscribe(onNext: { [weak self] image in
                self?.selfPrepareBtn.setImage(image, for: .normal)
            }).disposed(by: disposeBag)

        vm.meatNameTitle
            .bindTo(meatName.rx.text).disposed(by: disposeBag)
        vm.vegetableNameTitle
            .bindTo(vegetableName.rx.text).disposed(by: disposeBag)
        vm.selfPrepareNameTitle
            .bindTo(selfPrepareName.rx.text).disposed(by: disposeBag)

        meatBtn.rx.tap
            .throttle(0.18, latest: false, scheduler: MainScheduler.instance)
            .bindTo(vm.didPressMeatBtn).disposed(by: disposeBag)

        vegetableBtn.rx.tap
            .throttle(0.18, scheduler: MainScheduler.instance)
            .bindTo(vm.didPressVegetableBtn).disposed(by: disposeBag)

        selfPrepareBtn.rx.tap
            .throttle(0.18, latest: false, scheduler: MainScheduler.instance)
            .bindTo(vm.didPressSelfPrepareBtn).disposed(by: disposeBag)

        vm.hudFlash.subscribe(onNext: { type in
            HUD.flash(type, delay: 1.0)
        }).disposed(by: disposeBag)

        vm.hudShow.subscribe(onNext: { type in
            HUD.show(type)
        }).disposed(by: disposeBag)

        vm.didClose.subscribe(onNext: { [weak self] _ in
            self?.close()
        }).disposed(by: disposeBag)

        tap.rx.event.subscribe(onNext: { [weak self] _ in
            self?.close()
        }).disposed(by: disposeBag)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func close() {
        dismiss(animated: false)
    }

}
