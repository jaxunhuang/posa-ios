//
// Created by Jaxun on 2018/8/22.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import PKHUD
import Alamofire


protocol ChooseLunchVMP {
    
    //var dataSource: ReplaySubject<ChooseLunchDataSource> //observer, observable
    
    var chooseTitle: Observable<String?> { get } // chose lunch, step1: assert = "chose dinner"

    var vegetableNameTitle: Observable<String?> { get }
    var vegetableBtnImageName: Observable<String> { get }
    var didPressVegetableBtn: PublishSubject<Void> { get }
    
    var meatNameTitle: Observable<String?> { get }
    var meatBtnImageName: Observable<String> { get }
    var didPressMeatBtn: PublishSubject<Void> { get }
    
    var selfPrepareNameTitle: Observable<String?> { get }
    var selfPrepareBtnImageName: Observable<String> { get }
    var didPressSelfPrepareBtn: PublishSubject<Void> { get }

    var hudShow: PublishSubject<HUDContentType> { get }
    var hudFlash: PublishSubject<HUDContentType> { get }

    var didClose: PublishSubject<Void> { get }
}

class ChooseLunchViewModel: ChooseLunchVMP {
    
    let pref = Pref()
    
    let disposeBag = DisposeBag()
    
    var chooseTitle: Observable<String?> = .empty()
    
    var vegetableNameTitle: Observable<String?> = .empty()
    
    var vegetableBtnImageName: Observable<String> = .empty()
    
    let didPressVegetableBtn: PublishSubject<Void> = PublishSubject()
    
    var meatNameTitle: Observable<String?> = .empty()
    
    var meatBtnImageName: Observable<String> = .empty()
    
    let didPressMeatBtn: PublishSubject<Void> = PublishSubject()
    
    var selfPrepareNameTitle: Observable<String?> = .empty()
    
    var selfPrepareBtnImageName: Observable<String> = .empty()
    
    let didPressSelfPrepareBtn: PublishSubject<Void> = PublishSubject()

    var hudShow: PublishSubject<HUDContentType> = PublishSubject()

    var hudFlash: PublishSubject<HUDContentType> = PublishSubject()

    var didClose: PublishSubject<Void> = PublishSubject()

    init(){

        chooseTitle = Observable.of("Lunch Selection")
        vegetableNameTitle = Observable.of("Vegetarian")
        vegetableBtnImageName = Observable.of("icon_vegi")
        
        didPressVegetableBtn.map{ _ in "Vegetarian" }
            .subscribe(onNext: { [weak self] (text) in
                self?.orderMeal(choseName: text)
            }).addDisposableTo(disposeBag)
        
        meatNameTitle = Observable.of("Regular")
        meatBtnImageName = Observable.of("icon_meat")
        didPressMeatBtn.map{ _ in "Regular" }
            .subscribe(onNext: { [weak self] (text) in
                self?.orderMeal(choseName: text)
            }).addDisposableTo(disposeBag)
        
        selfPrepareNameTitle = Observable.of("Self-prepared")
        selfPrepareBtnImageName = Observable.of("icon_self")
        didPressSelfPrepareBtn.map{ _ in "Self-prepared" }
            .subscribe(onNext: { [weak self] (text) in
                self?.orderMeal(choseName: text)
            }).addDisposableTo(disposeBag)
        
    }
    
    func orderMeal(choseName:String) {
        hudShow.onNext(.progress)
        
        let account = pref.aliasAccount
        let cname = pref.aliasName
        let ename = pref.aliasNameEn
        let selection = choseName
        let depName = pref.aliasDepartment
        let floorSide = pref.aliasSiteFloor
        
        print(account!)
        print(cname!)
        print(ename!)
        print(selection)
        print(depName!)
        
        let url = URL(string: ChoseLunchDefine.ChoseLunchEndpoint)
        let params = [
            "ep_no": account!,
            "cname": cname!,
            "ename": ename!,
            "selection": selection,
            "dep_name": depName!,
            "site": floorSide!
            ]
        
        Alamofire
            .request(url!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
            .responseJSON{ [weak self] response in
                guard let `self` = self else {
                    return
                }
                print("json = \(response)")
                
                guard let status = response.response?.statusCode else { return }
                switch (status) {
                case 200:
                    print("example success")
                    self.hudFlash.onNext(.labeledSuccess(title: I18N.key("success"), subtitle: "\(choseName) ordered"))
                default:
                    print("error with response status: \(status)")
                    self.hudFlash.onNext(.labeledError(title: "Failure", subtitle: nil))
                }
                self.didClose.onNext(())
            }
    }

}

