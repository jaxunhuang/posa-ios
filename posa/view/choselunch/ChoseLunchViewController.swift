//
// Created by Jaxun on 2018/7/18.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import SnapKit
import PKHUD
import Alamofire

struct ChoseLunchDefine {
  // response is not json format, so not call from BuildConfig
  static let ChoseLunchEndpoint = "\(BuildConfig.endpoint)MealOrder"
}

class ChoseLunchViewController: UIViewController {

  fileprivate let pref = Pref()
  fileprivate let choseLunchService:ChoseLunchService = Injector.inject()


    required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  init(controller:MainPageEntry) {
    super.init(nibName: nil, bundle: nil)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // for test.
    let tap = UITapGestureRecognizer(target: self, action: #selector(close))
    view.addGestureRecognizer(tap)

    view.backgroundColor = Themes.blurBg

    let contentView = UIView(frame: .zero)
    contentView.backgroundColor = .white
    contentView.layer.cornerRadius = 50
    view.addSubview(contentView)


    let titleLabel = UILabel(frame: .zero)
    titleLabel.text = "Lunch Selection"
    titleLabel.font = Themes.font20Bold
    titleLabel.textAlignment = .center
    contentView.addSubview(titleLabel)

    let img1 = UIButton(type: .custom)
    img1.setImage(UIImage(named: "icon_vegi"), for: .normal)
    img1.addTarget(self, action: #selector(choseVegetarian), for: .touchUpInside)
    contentView.addSubview(img1)

    let img2 = UIButton(type: .custom)
    img2.setImage(UIImage(named: "icon_meat"), for: .normal)
    img2.addTarget(self, action: #selector(choseMeat), for: .touchUpInside)
    contentView.addSubview(img2)

    let img3 = UIButton(type: .custom)
    img3.setImage(UIImage(named: "icon_self"), for: .normal)
    img3.addTarget(self, action: #selector(choseSelfEat), for: .touchUpInside)
    contentView.addSubview(img3)

    let imgName1 = UILabel(frame: .zero)
    imgName1.text = "Vegetarian"
    imgName1.textAlignment = .center
    imgName1.textColor = Themes.starluxGold
    imgName1.font = Themes.font12
    contentView.addSubview(imgName1)

    let imgName2 = UILabel(frame: .zero)
    imgName2.text = "Regular"
    imgName2.font = Themes.font12
    imgName2.textAlignment = .center
    imgName2.textColor = Themes.starluxGold
    contentView.addSubview(imgName2)

    let imgName3 = UILabel(frame: .zero)
    imgName3.text = "Self-prepared"
    imgName3.font = Themes.font12
    imgName3.textAlignment = .center
    imgName3.textColor = Themes.starluxGold
    contentView.addSubview(imgName3)

    let extendCircleSize = Dimension.isPhoneSE ? 80 : 90

    titleLabel.snp.makeConstraints { make in
      make.centerY.equalTo(view).offset(-15)
      make.centerX.equalTo(view)
      make.size.equalTo(CGSize(width: 300, height: 20))
    }

    img2.snp.makeConstraints { make in
      make.top.equalTo(titleLabel.snp.bottom).offset(20)
      make.centerX.equalToSuperview()
      make.size.equalTo(CGSize(width: extendCircleSize, height: extendCircleSize))
    }
    img1.snp.makeConstraints { make in
      make.top.equalTo(img2)
      var extraValue = -10
      if Dimension.isPhoneSE {
        extraValue = -5
      }
      make.trailing.equalTo(img2.snp.leading).offset(extraValue)
      make.size.equalTo(CGSize(width: extendCircleSize, height: extendCircleSize))
    }
    img3.snp.makeConstraints { make in
      make.top.equalTo(img2)
      var extraValue = 10
      if Dimension.isPhoneSE {
        extraValue = 5
      }
      make.leading.equalTo(img2.snp.trailing).offset(extraValue)
      make.size.equalTo(CGSize(width: extendCircleSize, height: extendCircleSize))
    }

    imgName1.snp.makeConstraints { make in
      make.top.equalTo(img1.snp.bottom).offset(5)
      make.centerX.equalTo(img1)
      make.size.equalTo(CGSize(width: 80, height: 20))
    }
    imgName2.snp.makeConstraints { make in
      make.top.equalTo(img2.snp.bottom).offset(5)
      make.centerX.equalTo(img2)
      make.size.equalTo(CGSize(width: 80, height: 20))
    }
    imgName3.snp.makeConstraints { make in
      make.top.equalTo(img3.snp.bottom).offset(5)
      make.centerX.equalTo(img3)
      make.size.equalTo(CGSize(width: 80, height: 20))
    }

    contentView.snp.makeConstraints { make in
      make.top.equalTo(titleLabel).offset(-22)
      make.leading.equalTo(20)
      make.trailing.equalTo(-20)
      make.bottom.equalTo(imgName2).offset(20)
    }

  }

    @objc func choseVegetarian() {
    orderMeal(choseName: "Vegetarian")
  }

    @objc func choseMeat() {
    orderMeal(choseName: "Regular")
  }

    @objc func choseSelfEat() {
    orderMeal(choseName: "Self-prepared")
  }

  func orderMeal(choseName:String) {
    HUD.show(.progress)

    let account = pref.aliasAccount
    let cname = pref.aliasName
    let ename = pref.aliasNameEn
    let selection = choseName
    let depName = pref.aliasDepartment
    let floorSide = pref.aliasSiteFloor

    print(account!)
    print(cname!)
    print(ename!)
    print(selection)
    print(depName!)

    let url = URL(string: ChoseLunchDefine.ChoseLunchEndpoint)
    let params = [
      "ep_no": account!,
      "cname": cname!,
      "ename": ename!,
      "selection": selection,
      "dep_name": depName!,
      "site": floorSide!,
    ]

    Alamofire.request(url!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
        .responseJSON { [weak self] response in
          guard let strongSelf = self else {
            return
          }
          print("json = \(response)")

          if let status = response.response?.statusCode {
            switch (status) {
            case 200:
              print("example success")
              HUD.flash(.labeledSuccess(title: I18N.key("success"), subtitle: "\(choseName) ordered"), delay: 1.0)
              strongSelf.close()
            default:
              print("error with response status: \(status)")
              HUD.flash(.labeledError(title: "Failure", subtitle: nil), delay: 1.0)
            }
          }
        }

/*choseLunchService.orderLunchMeal(accountId: account!, cname: cname!, ename: ename!, selection: selection, depName: depName!)
    .subscribe(onNext: { [weak self] result -> Void in
      guard let strongSelf = self else {
        return
      }
      HUD.flash(.labeledSuccess(title: "訂餐完成", subtitle: "你點的是 \(choseName)"), delay: 1.0)
      strongSelf.close()
    }).addDisposableTo(self.disposeBag)*/
  }

    @objc func close() {
    dismiss(animated: false)
  }

}

