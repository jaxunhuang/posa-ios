//
// Created by Jaxun on 2018/5/15.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import SnapKit
import FSCalendar
import PKHUD
import Datez

// 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職

struct MainCalenderDefine {
  static let unPunchTime = I18N.key("no_clock_in/out")
  static let late = I18N.key("late")
  static let leaveEarly = I18N.key("early_leave")
  static let workOvertime = I18N.key("overtime_attendance")
  static let derelictionOfDuty = I18N.key("absence_without_leave")
}

class MainCalenderController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {
  fileprivate let pref = Pref()
  fileprivate let mainCalenderService:MainCalenderService = Injector.inject()

  fileprivate var accountId:String = ""
  fileprivate var currentPage:Int = 0
  fileprivate let preMonthBtn = UIButton(type: .system)
  fileprivate let nextMonthBtn = UIButton(type: .system)
  fileprivate let topCalenderBgView = UIView()
  fileprivate let calender = FSCalendar(frame: .zero)

  fileprivate let scrollView = UIScrollView() // put items in it.
  fileprivate let scrollContentView = UIView()

  fileprivate let selectDayLabel = UILabel(frame: .zero)
  fileprivate let workTimeLabel = UILabel(frame: .zero)
  fileprivate let workTimeDesc = UILabel(frame: .zero)
  fileprivate let breakTimeLabel = UILabel(frame: .zero)
  fileprivate let breakTimeDesc = UILabel(frame: .zero)
  fileprivate let punchTimeListLabel = UILabel(frame: .zero)
  fileprivate let orderMealLabel = UILabel(frame: .zero)
  fileprivate let orderMealDesc = UILabel(frame: .zero)

  fileprivate let grayLine = UIView()
  fileprivate let separatorLine = UIView()
  fileprivate let separatorLine2 = UIView()
  fileprivate let separatorLine3 = UIView()

  fileprivate let abnormalLabel = UILabel(frame: .zero)
  fileprivate let leaveButton = UIButton(type: .system)
  fileprivate let centerClearSeparator = UIView(frame: .zero)
  fileprivate let forgotButton = UIButton(type: .system)

  fileprivate let abnormalSecondLabel = UILabel(frame: .zero)
  fileprivate let leaveSecondButton = UIButton(type: .system)
  fileprivate let forgotSecondButton = UIButton(type: .system)

  fileprivate let todayRecordsTableView = UITableView(frame: .zero, style: .plain)

  fileprivate var monthlyPunchTimeDtoList:[MonthlyPunchTimeDto] = []
  fileprivate var todayRecords:[String] = []
  fileprivate var abnormalRecordList:[BaseAbnormalDto] = []
  fileprivate var monthlyMealOrderRecordList:[BaseMealOrderDto] = []
  fileprivate var abnormalDayList:[String] = []

  fileprivate var firstAbnormal = AbnormalStatusDto(9, "", "", 0)
  fileprivate var lastAbnormal = AbnormalStatusDto(9, "", "", 0)

  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

    setNaviLogo()
  }

  private func setNaviLogo() {
    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  // MARK: - UIViewController
  override func viewDidLoad() {
    super.viewDidLoad()

    let fDate = Utils.getCalendarSelectDateFormat(Date())
    selectDayLabel.text = fDate.uppercased()

    let currentMonth = Utils.getMonth(Date())
    let currentYear = Utils.getYear(Date())
    fetchApi(choseYear: currentYear, choseMonth: currentMonth)

    calenderUI()
    topContentUI()
    middleContentUI()

    abnormalUI()
    abnormalSecondUI()

    initLayout()
  }

  fileprivate func fetchApi(choseYear:String, choseMonth:String) {
    fetchMonthlyData(choseYear: choseYear, choseMonth: choseMonth)
    fetchMonthlyMealOrder(choseYear: choseYear, choseMonth: choseMonth)
    fetchMonthlyAbnormal(choseYear: choseYear, choseMonth: choseMonth)
  }

  private func middleContentUI() {
    view.addSubview(scrollView)
    scrollView.addSubview(scrollContentView)

    selectDayLabel.font = Themes.font17Bold
    selectDayLabel.textColor = Themes.starluxBlack
    scrollContentView.addSubview(selectDayLabel)
//----------------------------------------------------//
    workTimeLabel.text = I18N.key("attendance")
//    workTimeLabel.font = Themes.font20Bold
    workTimeLabel.textColor = Themes.starluxGold
    scrollContentView.addSubview(workTimeLabel)

    workTimeDesc.text = PunchTimeDefine.noData
    workTimeDesc.textColor = Themes.starluxBlack
    scrollContentView.addSubview(workTimeDesc)
//----------------------------------------------------//
    breakTimeLabel.text = I18N.key("at_lunch")
//    breakTimeLabel.font = Themes.font20Bold
    breakTimeLabel.textColor = Themes.starluxGold
    scrollContentView.addSubview(breakTimeLabel)

    breakTimeDesc.text = "12:30 ~ 13:30" //todo:
    breakTimeDesc.textColor = Themes.starluxBlack
    scrollContentView.addSubview(breakTimeDesc)
//----------------------------------------------------//
    orderMealLabel.text = I18N.key("lunch_selection")
//    orderMealLabel.font = Themes.font20Bold
    orderMealLabel.textColor = Themes.starluxGold
    scrollContentView.addSubview(orderMealLabel)

    orderMealDesc.text = ""
    orderMealDesc.textColor = Themes.starluxBlack
    scrollContentView.addSubview(orderMealDesc)
//----------------------------------------------------//

    grayLine.backgroundColor = .gray
    separatorLine.backgroundColor = .lightGray
    separatorLine2.backgroundColor = .lightGray
    separatorLine3.backgroundColor = .lightGray
    calender.addSubview(grayLine)
    scrollContentView.addSubview(separatorLine)
    scrollContentView.addSubview(separatorLine2)
    scrollContentView.addSubview(separatorLine3)

    punchTimeListLabel.text = I18N.key("details")
//    punchTimeListLabel.font = Themes.font20Bold
    punchTimeListLabel.textColor = Themes.starluxGold
    scrollContentView.addSubview(punchTimeListLabel)

    setPunchTimeDetailUI()
  }

  private func abnormalUI() {
    abnormalLabel.text = "異常" // 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職
//    abnormalLabel.font = Themes.font20Bold
    abnormalLabel.textColor = Themes.warningRed
    scrollContentView.addSubview(abnormalLabel)

    leaveButton.setTitle(I18N.key("request_leave"), for: .normal)
    leaveButton.setTitleColor(Themes.starluxGold, for: .normal)
    leaveButton.backgroundColor = .white
    leaveButton.layer.shadowRadius = 3
    leaveButton.layer.shadowOpacity = 0.3
    leaveButton.layer.cornerRadius = 15
    leaveButton.layer.shadowOffset = CGSize(width: 0, height: 2)
    leaveButton.addTarget(self, action: #selector(clickToApplyLeave), for: .touchUpInside)
    scrollContentView.addSubview(leaveButton)

    centerClearSeparator.backgroundColor = .clear
    scrollContentView.addSubview(centerClearSeparator)

    forgotButton.setTitle(I18N.key("failed_to_clock_in/out"), for: .normal)
    forgotButton.setTitleColor(Themes.starluxGold, for: .normal)
    forgotButton.backgroundColor = .white
    forgotButton.layer.shadowRadius = 3
    forgotButton.layer.shadowOpacity = 0.3
    forgotButton.layer.cornerRadius = 15
    forgotButton.layer.shadowOffset = CGSize(width: 0, height: 2)
    forgotButton.addTarget(self, action: #selector(clickToApplyForgot), for: .touchUpInside)
    scrollContentView.addSubview(forgotButton)
  }

  private func abnormalSecondUI() {
    abnormalSecondLabel.text = "異常" // 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職
//    abnormalSecondLabel.font = Themes.font20Bold
    abnormalSecondLabel.textColor = Themes.warningRed
    scrollContentView.addSubview(abnormalSecondLabel)

    leaveSecondButton.setTitle(I18N.key("request_leave"), for: .normal)
    leaveSecondButton.setTitleColor(Themes.starluxGold, for: .normal)
    leaveSecondButton.backgroundColor = .white
    leaveSecondButton.layer.shadowRadius = 3
    leaveSecondButton.layer.shadowOpacity = 0.3
    leaveSecondButton.layer.cornerRadius = 15
    leaveSecondButton.layer.shadowOffset = CGSize(width: 0, height: 2)
    leaveSecondButton.addTarget(self, action: #selector(clickToApplyLeave), for: .touchUpInside)
    scrollContentView.addSubview(leaveSecondButton)

    forgotSecondButton.setTitle(I18N.key("failed_to_clock_in/out"), for: .normal)
    forgotSecondButton.setTitleColor(Themes.starluxGold, for: .normal)
    forgotSecondButton.backgroundColor = .white
    forgotSecondButton.layer.shadowRadius = 3
    forgotSecondButton.layer.shadowOpacity = 0.3
    forgotSecondButton.layer.cornerRadius = 15
    forgotSecondButton.layer.shadowOffset = CGSize(width: 0, height: 2)
    forgotSecondButton.addTarget(self, action: #selector(clickToApplyForgot), for: .touchUpInside)
    scrollContentView.addSubview(forgotSecondButton)
  }

    @objc func clickToApplyForgot() {
    let controller = ApplyForgotToPunchEntry()
    navigationController?.pushViewController(controller, animated: true)
  }

    @objc func clickToApplyLeave() {
    let controller = ApplyLeaveEntry()
    navigationController?.pushViewController(controller, animated: true)
  }

  private func setPunchTimeDetailUI() {
    todayRecordsTableView.delegate = self
    todayRecordsTableView.dataSource = self
    todayRecordsTableView.separatorStyle = .none
    todayRecordsTableView.backgroundColor = Themes.starluxBg
    todayRecordsTableView.allowsSelection = false
    todayRecordsTableView.flashScrollIndicators()

    scrollContentView.addSubview(todayRecordsTableView)
    todayRecordsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
  }

  private func modifyAbnormalDay() {
    abnormalRecordList.enumerated().forEach { _, dto in
      let baseAbnormalDto = dto as BaseAbnormalDto
      //print("------\(baseAbnormalDto.date)")
      abnormalDayList.append(baseAbnormalDto.date)
    }
  }

  fileprivate func showTodayAbnormalStatus(date:Date) {
    // hide abnormal first
    abnormalLabel.isHidden = true
    forgotButton.isHidden = true
    leaveButton.isHidden = true

    abnormalSecondLabel.isHidden = true
    forgotSecondButton.isHidden = true
    leaveSecondButton.isHidden = true

    // update current day abnormal
    for (_, dto) in abnormalRecordList.enumerated() {
      let baseAbnormalDto = dto as BaseAbnormalDto
      let today = Utils.getFullDateFormat(date)
      if today == baseAbnormalDto.date {

        abnormalLabel.isHidden = false
        forgotButton.isHidden = false
        leaveButton.isHidden = false

        let first = baseAbnormalDto.status?.first as! [String: Any?]
        let last = baseAbnormalDto.status?.last as! [String: Any?]

        firstAbnormal = AbnormalStatusDto.fromDictionary(first)
        lastAbnormal = AbnormalStatusDto.fromDictionary(last)

        if firstAbnormal.cardType != lastAbnormal.cardType {
          /// how to compare
          abnormalSecondLabel.isHidden = false
          forgotSecondButton.isHidden = false
          leaveSecondButton.isHidden = false
        } else {

        }

        // 刷卡異常代碼 - 0: 應刷未刷 1:遲到 2:早退 3:超時出勤 4:曠職
        switch (firstAbnormal.code) {
        case 0:
          abnormalLabel.text = MainCalenderDefine.unPunchTime
        case 1:
          abnormalLabel.text = MainCalenderDefine.late
        case 2:
          abnormalLabel.text = MainCalenderDefine.leaveEarly
        case 3:
          abnormalLabel.text = MainCalenderDefine.workOvertime
        case 4:
          abnormalLabel.text = MainCalenderDefine.derelictionOfDuty
        default:
          abnormalLabel.text = ""
        }

        switch (lastAbnormal.code) {
        case 0:
          abnormalSecondLabel.text = MainCalenderDefine.unPunchTime
        case 1:
          abnormalSecondLabel.text = MainCalenderDefine.late
        case 2:
          abnormalSecondLabel.text = MainCalenderDefine.leaveEarly
        case 3:
          abnormalSecondLabel.text = MainCalenderDefine.workOvertime
        case 4:
          abnormalSecondLabel.text = MainCalenderDefine.derelictionOfDuty
        default:
          abnormalSecondLabel.text = ""
        }
      }
    }
  }

  fileprivate func fetchMonthlyData(choseYear:String, choseMonth:String) {
    HUD.show(.progress)
    if let aId = pref.aliasAccount {
      accountId = aId
    }

    mainCalenderService.fetchMonthlyPunchTimeList(accountId: accountId, year: choseYear, month: choseMonth)
        .subscribe(onNext: { [weak self] result -> Void in
          //print(result)
          HUD.flash(.progress)
          guard let strongSelf = self else {
            return
          }
          strongSelf.monthlyPunchTimeDtoList = result as [MonthlyPunchTimeDto]

          // update every day display
          strongSelf.monthlyPunchTimeDtoList.map { monthlyPunchTimeDto in
            //todo: refactor to service
            let dto = monthlyPunchTimeDto as MonthlyPunchTimeDto

            let selectDay = Utils.getFullDateFormat(Date())

            if selectDay == dto.fullDate {
              strongSelf.todayRecords = dto.recordList
            } else {
              strongSelf.todayRecords.removeAll()
            }

            if strongSelf.todayRecords.first != strongSelf.todayRecords.last {
              let onWorkTime:String! = strongSelf.todayRecords.first
              let offWorkTime:String! = strongSelf.todayRecords.last
              strongSelf.workTimeDesc.text = strongSelf.withOutSec(onWorkTime) + " ~ " + strongSelf.withOutSec(
                  offWorkTime)
            } else {
              strongSelf.workTimeDesc.text = strongSelf.todayRecords.first
            }

            if strongSelf.todayRecords.isEmpty {
              strongSelf.workTimeDesc.text = "--:-- ~ --:--"
            }
            strongSelf.updateTodayRecordsLayout()

            strongSelf.todayRecordsTableView.reloadData()
          }
          // update every day display
        }
        ).addDisposableTo(self.disposeBag)
  }

  fileprivate func fetchMonthlyMealOrder(choseYear:String, choseMonth:String) {
    if let aId = pref.aliasAccount {
      accountId = aId
    }
    mainCalenderService.fetchMonthlyMealOrderList(accountId: accountId, year: choseYear, month: choseMonth)
        .subscribe(onNext: { [weak self] result -> Void in
          guard let strongSelf = self else {
            return
          }
          print(result)
          strongSelf.monthlyMealOrderRecordList = result as [BaseMealOrderDto]
          strongSelf.monthlyMealOrderRecordList.map { mealOrderDto in
            let dto = mealOrderDto as BaseMealOrderDto
            let selectDayOrder = Utils.getFullDateFormat(Date())
            if selectDayOrder == dto.date {
              strongSelf.orderMealDesc.text = dto.selection
            }
          }
        })
  }

  // todo: refactor
  fileprivate func fetchMonthlyAbnormal(choseYear:String, choseMonth:String) {
    if let aId = pref.aliasAccount {
      accountId = aId
    }
    mainCalenderService.fetchMonthlyAbnormalList(accountId: accountId, year: choseYear, month: choseMonth)
        .subscribe(onNext: { [weak self] result -> Void in
          guard let strongSelf = self else {
            return
          }
          //print(result)
          strongSelf.abnormalRecordList = result as [BaseAbnormalDto]
          strongSelf.modifyAbnormalDay()
          strongSelf.calender.reloadData()
          strongSelf.showTodayAbnormalStatus(date: Date())
        }).disposed(by: disposeBag)
  }

  private func topContentUI() {
    preMonthBtn.backgroundColor = Themes.starluxGold
    //preMonthBtn.setTitle("<", for: UIControlState.normal)
    preMonthBtn.setBackgroundImage(UIImage(named: "cal_left_white"), for: .normal)
    preMonthBtn.setTitleColor(.white, for: UIControlState.normal)
    preMonthBtn.layer.shadowOffset = CGSize(width: 0, height: 5)
    preMonthBtn.addTarget(self, action: #selector(preClick), for: UIControlEvents.touchUpInside)

    nextMonthBtn.backgroundColor = Themes.starluxGold
    //nextMonthBtn.setTitle(">", for: UIControlState.normal)
    nextMonthBtn.setBackgroundImage(UIImage(named: "cal_right_white"), for: .normal)
    nextMonthBtn.setTitleColor(.white, for: UIControlState.normal)
    nextMonthBtn.layer.shadowOffset = CGSize(width: 0, height: 5)
    nextMonthBtn.addTarget(self, action: #selector(nextClick), for: UIControlEvents.touchUpInside)

    calender.addSubview(preMonthBtn)
    calender.addSubview(nextMonthBtn)

    topCalenderBgView.backgroundColor = Themes.starluxGold
    self.view.insertSubview(topCalenderBgView, at: 0)
  }

  private func calenderUI() {
    calender.dataSource = self
    calender.delegate = self
    calender.appearance.headerMinimumDissolvedAlpha = 0
    calender.appearance.todayColor = Themes.starluxGray
    calender.appearance.selectionColor = Themes.starluxGold
    calender.appearance.weekdayTextColor = .darkGray
    calender.appearance.titleWeekendColor = .gray
    calender.appearance.headerDateFormat = "MMM  yyyy"
    calender.appearance.headerTitleFont = Themes.font20Bold
    calender.appearance.caseOptions = .headerUsesUpperCase
    calender.appearance.headerTitleColor = .white
    calender.appearance.eventColor = .red
    calender.appearance.eventSelectionColor = Themes.warningRed
    calender.locale = Locale(identifier: "en_US_POSIX")

    calender.backgroundColor = .clear
    self.view.addSubview(calender)
  }

  override func viewWillAppear(_ animated:Bool) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_back")
    navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
  }
}

/// MARK: Tap calender
extension MainCalenderController {
    @objc func preClick() {
    let preMonth = calender.currentPage - 1
    //print(preMonth)
    calender.setCurrentPage(preMonth, animated: true)

    let choseMonth = Utils.getMonth(preMonth)
    let currentYear = Utils.getYear(Date())
    fetchApi(choseYear: currentYear, choseMonth: choseMonth)
  }

    @objc func nextClick() {
    let nextMonth = calender.date(byAddingMonths: 1, to: calender.currentPage)
    calender.setCurrentPage(nextMonth, animated: true)
    //print(nextMonth)

    let choseMonth = Utils.getMonth(nextMonth)
    let currentYear = Utils.getYear(Date())
    fetchApi(choseYear: currentYear, choseMonth: choseMonth)
  }
}

//MARK: FSCalendarDataSource, FSCalendarDelegate
extension MainCalenderController {

  func calendarCurrentMonthDidChange(_ calendar:FSCalendar) {
    let choseMonth = Utils.getMonth(calendar.currentPage)
    let currentYear = Utils.getYear(Date())

    fetchApi(choseYear: currentYear, choseMonth: choseMonth)
  }

  func calendar(_ calendar:FSCalendar, numberOfEventsFor date:Date) -> Int {
    //print("day-----   \(date)")
    let dayFormat = Utils.getFullDateFormat(date)
    //print("dayFormat-----   \(dayFormat)")
    if abnormalDayList.contains(dayFormat) {
      return 1
    }
    //print("abnormalDayList\(abnormalDayList)")
    for (_, dto) in abnormalRecordList.enumerated() {
      let baseAbnormalDto = dto as BaseAbnormalDto
      let eventDay = Utils.getFullDateFormat(date)
      //print(eventDay)
      if eventDay == baseAbnormalDto.date {
        //print(eventDay)
        return 1
      }
    }
    return 0
  }

  func calendar(_ calendar:FSCalendar, boundingRectWillChange bounds:CGRect, animated:Bool) {
    calender.snp.updateConstraints { (make) in
      make.height.equalTo(bounds.height)
    }
    self.view.layoutIfNeeded()
  }

  func calendar(_ calendar:FSCalendar, didSelect date:Date, at monthPosition:FSCalendarMonthPosition) {
    //print(date)

    let fDate = Utils.getCalendarSelectDateFormat(date)
    selectDayLabel.text = fDate.uppercased()

    let selectDay = date.gregorian + 8.hour
    //print(selectDay.date)
    if monthPosition == .previous || monthPosition == .next {
      calendar.setCurrentPage(selectDay.date, animated: true)
      // update month api
    }

    showTodayAbnormalStatus(date: selectDay.date)

    let selectDayStr = Utils.getFullDateFormat(selectDay.date)
    todayRecords.removeAll()

    //---show today meal order---//
    orderMealDesc.text = ""
    monthlyMealOrderRecordList.map { mealOrderDto in
      let dto = mealOrderDto as BaseMealOrderDto
      if selectDayStr == dto.date {
        orderMealDesc.text = dto.selection
      }
    }

    //---show today work time---//
    updateTodayRecordsData(selectDay: selectDayStr)
    updateTodayRecordsLayout()
  }

  fileprivate func updateTodayRecordsData(selectDay:String) {
    todayRecordsTableView.flashScrollIndicators() // show indicators when need it.

    monthlyPunchTimeDtoList.map { monthlyPunchTimeDto in
      let dto = monthlyPunchTimeDto as MonthlyPunchTimeDto
      if selectDay == dto.fullDate {
        //print(">>>map. date>>> \(dto.recordList)")
        todayRecords = dto.recordList
        return
      }
    }

    if todayRecords.first != todayRecords.last {
      let onWorkTime:String! = todayRecords.first
      let offWorkTime:String! = todayRecords.last

      workTimeDesc.text = withOutSec(onWorkTime) + " ~ " + withOutSec(offWorkTime)
    } else {
      workTimeDesc.text = todayRecords.first
    }

    if todayRecords.isEmpty {
      workTimeDesc.text = "--:-- ~ --:--" //PunchTimeDefine.noData + " ~ " + PunchTimeDefine.noData
    }

    todayRecordsTableView.reloadData()
  }

  fileprivate func withOutSec(_ timeWithSec:String) -> String {
    return String(timeWithSec.characters.prefix(5))
  }

  // hide or show "Details"
  fileprivate func updateTodayRecordsLayout() {
    punchTimeListLabel.isHidden = todayRecords.count > 0 ? false : true
    todayRecordsTableView.snp.updateConstraints { make in
      make.height.equalTo(120)
      if todayRecords.count == 0 {
        make.height.equalTo(0)
      }
    }
  }
}

private typealias TableDelegate = MainCalenderController

extension TableDelegate: UITableViewDelegate {
  public func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
    return 30
  }
}

private typealias TableDataSource = MainCalenderController

extension TableDataSource: UITableViewDataSource {
  public func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
    return todayRecords.count
  }

  public func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
    cell.backgroundColor = Themes.starluxBg
    if let timeLabel = cell.textLabel {
      timeLabel.text = "\(todayRecords[indexPath.row])"
      timeLabel.textColor = Themes.starluxBlack
      timeLabel.textAlignment = .center
    }
    return cell
  }
}


private typealias LayoutSetting = MainCalenderController

extension LayoutSetting {

  fileprivate func initLayout() {
    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }
    topCalenderBgView.snp.makeConstraints { make in
      make.top.leading.trailing.equalTo(0)
      make.height.greaterThanOrEqualTo(40)
    }
    preMonthBtn.snp.makeConstraints { make in
      make.top.equalTo(3)
      make.leading.equalTo(20)
      make.height.greaterThanOrEqualTo(24)
      make.size.equalTo(CGSize(width: 36, height: 36))
    }
    nextMonthBtn.snp.makeConstraints { make in
      make.top.equalTo(3)
      make.trailing.equalTo(-15)
      make.height.greaterThanOrEqualTo(24)
      make.size.equalTo(CGSize(width: 36, height: 36))
    }
    calender.snp.makeConstraints { make in
      make.top.leading.trailing.equalTo(0)
      make.height.equalTo(300)
    }

    //--------- insert in scroll view ----------//
    selectDayLabel.snp.makeConstraints { make in
      make.top.equalTo(scrollContentView).offset(20)
      make.centerX.equalTo(view)
    }
    grayLine.snp.makeConstraints { make in
      make.bottom.equalTo(calender)
      make.leading.trailing.equalToSuperview()
      make.height.equalTo(2)
    }
    separatorLine.snp.makeConstraints { make in
      make.top.equalTo(selectDayLabel.snp.bottom).offset(20)
      make.leading.trailing.equalToSuperview().inset(10)
      make.height.equalTo(1)
    }
    workTimeLabel.snp.makeConstraints { make in
      make.top.equalTo(separatorLine.snp.bottom).offset(20)
      make.leading.equalTo(scrollContentView).inset(50)
    }
    workTimeDesc.snp.makeConstraints { make in
      make.centerY.equalTo(workTimeLabel)
      make.centerX.equalTo(todayRecordsTableView)
    }
    breakTimeLabel.snp.makeConstraints { make in
      make.top.equalTo(workTimeDesc.snp.bottom).offset(20)
      make.leading.equalTo(scrollContentView).inset(50)
    }
    breakTimeDesc.snp.makeConstraints { make in
      make.centerY.equalTo(breakTimeLabel)
      make.centerX.equalTo(todayRecordsTableView)
    }
    separatorLine2.snp.makeConstraints { make in
      make.top.equalTo(breakTimeDesc.snp.bottom).offset(20)
      make.leading.trailing.equalToSuperview().inset(10)
      make.height.equalTo(1)
    }
    orderMealLabel.snp.makeConstraints { make in
      make.top.equalTo(separatorLine2.snp.bottom).offset(20)
      make.centerX.equalTo(breakTimeLabel)
    }
    orderMealDesc.snp.makeConstraints { make in
      make.centerY.equalTo(orderMealLabel)
      make.centerX.equalTo(todayRecordsTableView)
    }
    separatorLine3.snp.makeConstraints { make in
      make.top.equalTo(orderMealLabel.snp.bottom).offset(20)
      make.leading.trailing.equalToSuperview().inset(10)
      make.height.equalTo(1)
    }
    punchTimeListLabel.snp.makeConstraints { make in
      make.top.equalTo(separatorLine3.snp.bottom).offset(20)
      make.centerX.equalTo(breakTimeLabel)
      /*if todayRecords.count == 0 {
        make.height.equalTo(0)
      }*/
    }
    todayRecordsTableView.snp.makeConstraints { make in
      make.top.equalTo(punchTimeListLabel).offset(-3)
      make.leading.equalTo(punchTimeListLabel.snp.trailing).offset(20)
      make.trailing.equalTo(-20)
      make.height.equalTo(120)
      /*if todayRecords.count == 0 {
        make.height.equalTo(0)
      }*/
    }
    abnormalLabel.snp.makeConstraints { make in
      make.top.equalTo(todayRecordsTableView.snp.bottom).offset(20)
      make.centerX.equalTo(view)
      //make.leading.equalTo(workTimeLabel)
    }
    leaveButton.snp.makeConstraints { make in
      make.top.equalTo(abnormalLabel.snp.bottom).offset(20)
      make.centerX.equalTo(abnormalLabel)
      make.size.equalTo(CGSize(width: 240, height: 40))
    }

    // base standard separator
    centerClearSeparator.snp.makeConstraints { make in
      make.top.equalTo(abnormalLabel.snp.bottom)
      make.centerX.equalTo(view)
      make.height.equalTo(1)
    }

    forgotButton.snp.makeConstraints { make in

      make.top.equalTo(leaveButton.snp.bottom).offset(20)
      make.centerX.equalTo(abnormalLabel)
      make.size.equalTo(CGSize(width: 240, height: 40))
    }

    abnormalSecondLabel.snp.makeConstraints { make in
      make.top.equalTo(forgotButton.snp.bottom).offset(30)
      make.centerX.equalTo(abnormalLabel)
    }

    leaveSecondButton.snp.makeConstraints { make in
      make.top.equalTo(abnormalSecondLabel.snp.bottom).offset(20)
      make.centerX.equalTo(abnormalLabel)
      make.size.equalTo(CGSize(width: 240, height: 40))
    }

    // between leave & forgot center with a clear separator.

    forgotSecondButton.snp.makeConstraints { make in


//      if Dimension.isPhoneSE {
      make.top.equalTo(leaveSecondButton.snp.bottom).offset(20)

      make.centerX.equalTo(abnormalLabel)
      make.size.equalTo(CGSize(width: 240, height: 40))

//      } else {
//        make.top.equalTo(abnormalSecondLabel.snp.bottom).offset(20)
//        make.leading.equalTo(centerClearSeparator.snp.trailing).offset(10)
//        make.size.equalTo(CGSize(width: 160, height: 40))
//      }
    }
    //--------- insert in scroll view ----------//

    scrollView.snp.makeConstraints { make in
      make.top.equalTo(calender.snp.bottom)
      make.leading.trailing.bottom.equalTo(view)
    }
    scrollContentView.snp.makeConstraints { make in
      make.edges.width.equalTo(scrollView)
      make.top.equalTo(scrollView)
      make.leading.trailing.equalTo(view)
      make.bottom.equalTo(forgotSecondButton).offset(20) // the last one
    }
  }
}




