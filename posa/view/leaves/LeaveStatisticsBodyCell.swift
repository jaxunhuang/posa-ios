//
// Created by Jaxun on 2018/7/3.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

final class LeaveStatisticsBodyCell: UITableViewCell {

  let leaveDisplay1 = UILabel(frame: .zero)
  let leaveDisplay2 = UILabel(frame: .zero)
  let leaveDisplay3 = UILabel(frame: .zero)
  let leaveDisplay4 = UILabel(frame: .zero)
  let leaveDisplay5 = UILabel(frame: .zero)
  let leaveDisplay6 = UILabel(frame: .zero)

  func bind(_ dto:EmployeeAllLeaveDto) {

    var displayUnit = ""
    switch dto.leaveUnit {
    case "0": displayUnit = "day(s)"
    case "1": displayUnit = "hour(s)"
    case "2": displayUnit = "minute"
    default:
      displayUnit = "_waiting.. new server_"
      return
    }

    leaveDisplay1.text = dto.leaveYear                                    //year
    leaveDisplay2.text = dto.duration                                     //period
    leaveDisplay2.adjustsFontSizeToFitWidth = true
    leaveDisplay3.text = String(dto.accrued.clean) + " " + displayUnit          //accrued
    leaveDisplay4.text = String(dto.used.clean) + " " + displayUnit             //Used
    leaveDisplay5.text = String(dto.signing.clean) + " " + displayUnit          //waiting for approval
    leaveDisplay6.text = String(dto.available.clean) + " " + displayUnit        //Available
    contentView.backgroundColor = Themes.starluxLight

    setUILayout() //mark: without left label will visible
  }

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override init(style:UITableViewCellStyle, reuseIdentifier:String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    selectionStyle = .none
    setUILayout()
  }

  private func setUILayout() {
    let leaveTitle1 = UILabel(frame: .zero)
    let leaveTitle2 = UILabel(frame: .zero)
    let leaveTitle3 = UILabel(frame: .zero)
    let leaveTitle4 = UILabel(frame: .zero)
    let leaveTitle5 = UILabel(frame: .zero)
    let leaveTitle6 = UILabel(frame: .zero)

    leaveTitle1.text = I18N.key("year")
    leaveTitle1.textColor = Themes.starluxGold
    leaveTitle2.text = I18N.key("period")
    leaveTitle2.textColor = Themes.starluxGold
    leaveTitle3.text = I18N.key("accrued")
    leaveTitle3.textColor = Themes.starluxGold
    leaveTitle4.text = I18N.key("used")
    leaveTitle4.textColor = Themes.starluxGold
    leaveTitle5.text = I18N.key("pending_approval")
    leaveTitle5.textColor = Themes.starluxGold
    leaveTitle6.text = I18N.key("available")
    leaveTitle6.textColor = Themes.starluxGold

    contentView.addSubview(leaveTitle1)
    contentView.addSubview(leaveTitle2)
    contentView.addSubview(leaveTitle3)
    contentView.addSubview(leaveTitle4)
    contentView.addSubview(leaveTitle5)
    contentView.addSubview(leaveTitle6)

    contentView.addSubview(leaveDisplay1)
    contentView.addSubview(leaveDisplay2)
    contentView.addSubview(leaveDisplay3)
    contentView.addSubview(leaveDisplay4)
    contentView.addSubview(leaveDisplay5)
    contentView.addSubview(leaveDisplay6)


    leaveTitle1.snp.makeConstraints { make in
      make.top.equalTo(30)
      make.leading.equalTo(40)
      //make.height.equalTo(10)
    }
    leaveTitle2.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle1.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveTitle3.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle2.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveTitle4.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle3.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveTitle5.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle4.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveTitle6.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle5.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }

    leaveDisplay1.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle1)
      make.trailing.equalTo(-40)
    }

    leaveDisplay2.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay1.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
      if Dimension.isPhoneSE {
        make.leading.equalTo(leaveTitle2.snp.trailing).offset(10)
      }
    }
    leaveDisplay3.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay2.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    leaveDisplay4.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay3.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    leaveDisplay5.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay4.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    leaveDisplay6.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay5.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    contentView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
      make.bottom.equalTo(leaveDisplay6.snp.bottom).offset(30)
    }
  }

  func appendLine() { // MARK: for Physiology Leave, maybe will remove
    let line = UIView()
    line.backgroundColor = .lightGray
    contentView.addSubview(line)

    line.snp.makeConstraints { maker in
      maker.leading.equalTo(20)
      maker.trailing.equalTo(-20)
      maker.height.equalTo(1)
      maker.bottom.equalTo(contentView).offset(20)
    }
  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
