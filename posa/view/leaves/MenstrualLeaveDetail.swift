//
// Created by Jaxun on 2018/7/19.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import PKHUD

class MenstrualLeaveDetail: UIViewController, UITableViewDelegate, UITableViewDataSource {

  fileprivate let pref = Pref()
  fileprivate let leaveStatisticsService:LeaveStatisticsService = Injector.inject()
  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
  fileprivate var accountId = String()
  public var choseYear = String()

  fileprivate let tableView = UITableView(frame: .zero, style: .plain)
  fileprivate var results:[MenstrualLeaveDto] = []


  init(choseYear:String) {
    super.init(nibName: nil, bundle: nil)

    self.choseYear = choseYear
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

  }

  override func viewDidLoad() {
    view.backgroundColor = .white

    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
    setNaviLogo()
    setTableViewUI()
    fetchApi()
  }

  private func fetchApi() {
    if let aId = pref.aliasAccount {
      accountId = aId
    }

    HUD.flash(.progress)
    leaveStatisticsService.fetchMenstrualLeave(accountId: accountId, year: choseYear)
        .subscribe(onNext: { [weak self] result -> Void in
          guard let strongSelf = self else {
            return
          }
          let recordsDto = result as! BaseArrayDto

          if let employeeList = recordsDto.data {
            employeeList.enumerated().forEach { _, dto in
              let rawDto = dto as! [String: Any?]

              let menstrualLeaveDto = MenstrualLeaveDto.fromDictionary(rawDto)
              //print("------\(menstrualLeaveDto.date)")
              strongSelf.results.append(menstrualLeaveDto)
              strongSelf.tableView.reloadData()
              HUD.flash(.progress)
            }
          }
        }).addDisposableTo(self.disposeBag)
  }

  private func setTableViewUI() {

    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    tableView.backgroundColor = Themes.starluxBg
    tableView.allowsSelection = false
    tableView.register(MenstrualLeaveBodyCell.self,
        forCellReuseIdentifier: NSStringFromClass(MenstrualLeaveBodyCell.self)) //leaveStatisticsBodyCell

    view.addSubview(tableView)

    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }
    tableView.snp.makeConstraints { maker in
      maker.edges.equalTo(view)
    }
  }

  private func setNaviLogo() {
    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
  }

  func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
    return 12 // regular, year of month
  }

  func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(MenstrualLeaveBodyCell.self),
        for: indexPath) as! MenstrualLeaveBodyCell
    cell.backgroundColor = Themes.starluxBg
    if results.count > 0 {
      cell.bind(results[indexPath.row])
    }
    cell.appendLine()
    return cell
  }
}