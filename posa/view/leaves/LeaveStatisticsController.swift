import UIKit
import SnapKit
import Alertift
import PKHUD


class LeaveStatisticsController: UIViewController {

    fileprivate let pref = Pref()
    fileprivate let leaveStatisticsService: LeaveStatisticsService = Injector.inject()
    fileprivate var sex = String()
    fileprivate var hideCompensatory = true
    fileprivate var choseYearForMenstrualLeave = String()
    fileprivate var choseYearString = String()
    private let yearTitle = UIButton(type: .system)

    fileprivate var leaveAllResult: [EmployeeAllLeaveDto] = []
    //fileprivate var compensatoryAllResult: [CompensatoryLeaveDto] = []

    fileprivate let expandableView = CollapsibleTableSectionViewController()

    fileprivate let menstrualLeaveView = MenstrualLeaveView(frame: .zero)
    fileprivate let compensatoryLeaveView = CompensatoryLeaveView(frame: .zero)

    fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    required override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_back")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // check F / M
        sex = pref.aliasSex ?? "M"
        
        setNaviLogo()

        view.backgroundColor = Themes.starluxGold

        expandableView.delegate = self

        yearTitle.addTarget(self, action: #selector(clickChoseYear), for: .touchUpInside)

        let currentYear = Utils.getYear(Date())
        yearTitle.setTitle("\(currentYear) \(I18N.key("attendance_record"))" + " ", for: .normal)
        yearTitle.setTitleColor(.white, for: .normal)
        yearTitle.titleLabel?.font = Themes.font20

        let arrowIcon = UIImageView(image: UIImage(named: "right_arrow"))
        arrowIcon.contentMode = .scaleAspectFit

        menstrualLeaveView.handleController = navigationController!
        compensatoryLeaveView.handleController = navigationController!

        view.addSubview(yearTitle)
        view.addSubview(arrowIcon)
        view.addSubview(menstrualLeaveView)
        view.addSubview(compensatoryLeaveView)
        view.addSubview(expandableView.view)

        fetchApi(choseYear: "\(currentYear)")
        fetchCompensatoryApi(choseYear: "\(currentYear)")

        logoImg.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
        }

        yearTitle.snp.makeConstraints { maker in
            maker.top.equalTo(15)
            //maker.centerX.equalToSuperview()
            maker.leading.equalTo(25)
            maker.size.equalTo(CGSize(width: 230, height: 30))
        }

        arrowIcon.snp.makeConstraints { maker in
            maker.size.equalTo(CGSize(width: 24, height: 24))
            maker.leading.equalTo(yearTitle.snp.trailing)
            maker.centerY.equalTo(yearTitle)
        }

        menstrualLeaveView.snp.makeConstraints { maker in
            maker.top.equalTo(yearTitle.snp.bottom).offset(15)
            maker.leading.equalTo(0)
            maker.size.equalTo(CGSize(width: Screens.width, height: 50))
            if sex == "M" {
                menstrualLeaveView.alpha = 0
                maker.size.equalTo(CGSize(width: Screens.width, height: 1))
            }
        }
        compensatoryLeaveView.snp.makeConstraints { maker in
            maker.top.equalTo(menstrualLeaveView.snp.bottom)//.offset(15)
            maker.leading.equalTo(0)
            maker.size.equalTo(CGSize(width: Screens.width, height: 50))
            if hideCompensatory {
                compensatoryLeaveView.alpha = 0
                maker.size.equalTo(CGSize(width: Screens.width, height: 1))
            } else {
                compensatoryLeaveView.alpha = 1
            }
        }

        expandableView.view.snp.makeConstraints { maker in
            maker.top.equalTo(compensatoryLeaveView.snp.bottom)
            maker.leading.trailing.bottom.equalToSuperview()
        }

    }

    private func setNaviLogo() {
        let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
        logoImg.contentMode = choseMode
        navigationItem.titleView = logoImg
    }

    @objc func clickChoseYear() {
        let currentYear: String = Utils.getYear(Date())
//    currentYear = "2050" //for test multi years
        var displayYears: [String] = [currentYear]
        var standardYear = Int(currentYear)
        if let onBoard = pref.aliasOnBoardDay {
            choseYearString = String(onBoard.suffix(4)) // ex: "day_onboard": "Apr 02 2018"
        }
        let onBoardYear = Int(choseYearString)

        while standardYear! > onBoardYear! {
            print(standardYear!)
            standardYear = standardYear! - 1
            displayYears.append("\(standardYear!)")
        }

        Alertift.actionSheet(message: nil)
                .actions(displayYears) //["2018","2017"...]
                .action(.cancel("cancel"))
                .finally { [weak self] action, index in
                    guard let `self` = self, action.style != .cancel
                            else { return }
                    self.fetchApi(choseYear: displayYears[index])
                    self.fetchCompensatoryApi(choseYear: displayYears[index])
                    self.menstrualLeaveView.choseYear = displayYears[index]
                    self.yearTitle.setTitle("\(displayYears[index]) \(I18N.key("attendance_record"))" + " ", for: .normal)
                }
                .show()
    }
}

extension LeaveStatisticsController: CollapsibleTableSectionDelegate {
    func numberOfSections(_ tableView: UITableView) -> Int {
        return leaveAllResult.count
    }

    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LeaveStatisticsBodyCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(
                LeaveStatisticsBodyCell.self)) as? LeaveStatisticsBodyCell ??
                LeaveStatisticsBodyCell(style: .default, reuseIdentifier: NSStringFromClass(LeaveStatisticsBodyCell.self))

        if leaveAllResult.count > 0 && indexPath.section < leaveAllResult.count {
            let leave = leaveAllResult[indexPath.section] as EmployeeAllLeaveDto
            cell.bind(leave)
            cell.backgroundColor = Themes.starluxLight
        }

        cell.selectionStyle = .none
        return cell
    }

    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return leaveAllResult[section].leaveNameEn
    }

    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func collapsibleTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return true
    }

}

private extension LeaveStatisticsController{
     func remakeCompensatoryLeaveView(){
        compensatoryLeaveView.snp.remakeConstraints { maker in
            maker.top.equalTo(self.menstrualLeaveView.snp.bottom)
            maker.leading.equalTo(0)
            maker.size.equalTo(CGSize(width: Screens.width, height: 50))
            if hideCompensatory {
                compensatoryLeaveView.alpha = 0
                maker.size.equalTo(CGSize(width: Screens.width, height: 1))
            } else {
                compensatoryLeaveView.alpha = 1
            }
        }
    }
}

private extension LeaveStatisticsController{
    
    func fetchCompensatoryApi(choseYear: String) {
        
        guard let accountId = pref.aliasAccount else {
            return
        }
        
        leaveStatisticsService.fetchCompensatoryLeave(accountId: accountId, year: choseYear)
            .subscribe(onNext: { [weak self] result -> Void in
                guard let `self` = self, let recordsDto = result as? BaseArrayDto, let compensatoryList = recordsDto.data else {
                    return
                }
                self.hideCompensatory = compensatoryList.isEmpty
                
                let compensatoryAllResult = compensatoryList.map { dto -> CompensatoryLeaveDto in
                    let rawDto = dto as! [String: Any?]
                    return CompensatoryLeaveDto.fromDictionary(rawDto)
                }
                
                self.compensatoryLeaveView.bind(dtos:compensatoryAllResult)
                self.remakeCompensatoryLeaveView()
                
            }).disposed(by: super.disposeBag)
    }
    
    func fetchApi(choseYear: String) {
        
        leaveAllResult.removeAll()
        
        guard let accountId = pref.aliasAccount else {
            return
        }
        
        HUD.show(.progress)
        
        leaveStatisticsService.fetchAllLeave(accountId: accountId, year: choseYear)
            .subscribe(onNext: { [weak self] result -> Void in
                
                guard let `self` = self,
                    let recordsDto = result as? BaseArrayDto,
                    let employeeList = recordsDto.data else {
                        HUD.hide()
                        return
                }
                
                self.leaveAllResult += employeeList.map { dto in
                    let rawDto = dto as! [String: Any?]
                    return EmployeeAllLeaveDto.fromDictionary(rawDto)
                }
                
                self.expandableView._tableView.reloadData()
                HUD.hide()
                
            }).disposed(by: super.disposeBag)
    }
}
