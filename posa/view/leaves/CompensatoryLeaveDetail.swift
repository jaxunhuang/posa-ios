//
// Created by Jaxun on 2018/7/19.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import PKHUD

class CompensatoryLeaveDetail: UIViewController, UITableViewDelegate, UITableViewDataSource {

  fileprivate let pref = Pref()
  fileprivate let leaveStatisticsService:LeaveStatisticsService = Injector.inject()
  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
  fileprivate var accountId = String()
  public var choseYear = String()

  fileprivate let tableView = UITableView(frame: .zero, style: .plain)
  fileprivate var results:[CompensatoryLeaveDto] = []


  init(choseYear:String, results:[CompensatoryLeaveDto]) {
    super.init(nibName: nil, bundle: nil)

    self.choseYear = choseYear
    self.results = results
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

  }

  override func viewDidLoad() {
    view.backgroundColor = .white

    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
    setNaviLogo()
    setTableViewUI()
  }

  private func setTableViewUI() {

    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    tableView.backgroundColor = Themes.starluxBg
    tableView.allowsSelection = false
    tableView.register(CompensatoryLeaveBodyCell.self,
        forCellReuseIdentifier: NSStringFromClass(CompensatoryLeaveBodyCell.self))

    view.addSubview(tableView)

    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }
    tableView.snp.makeConstraints { maker in
      maker.edges.equalTo(view)
    }
  }

  private func setNaviLogo() {
    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
  }

  func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
    return results.count // regular, year of month
  }

  func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(CompensatoryLeaveBodyCell.self),
        for: indexPath) as! CompensatoryLeaveBodyCell
    cell.backgroundColor = Themes.starluxBg
    if results.count > 0 {
      cell.bindCompensatory(results[indexPath.row])
    }
    cell.appendLine()
    return cell
  }
}