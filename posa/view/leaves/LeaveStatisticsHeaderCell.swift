//
// Created by Jaxun on 2018/7/3.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

final class LeaveStatisticsHeaderCell: UITableViewCell {

  private let headerSectionLabel = UILabel(frame: .zero)

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  func bind(_ dto:String) {
    headerSectionLabel.text = dto
  }

  override init(style:UITableViewCellStyle, reuseIdentifier:String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    headerSectionLabel.textColor = Themes.starluxBlack
    headerSectionLabel.font = Themes.font20Bold

    let separator = UIView()
    separator.backgroundColor = .lightGray

    contentView.addSubview(headerSectionLabel)
    contentView.addSubview(separator)

    headerSectionLabel.snp.makeConstraints { make in
      make.top.equalTo(10)
      make.leading.equalTo(30)
    }
    separator.snp.makeConstraints { make in
      make.leading.equalTo(20)
      make.trailing.equalTo(-20)
      make.height.equalTo(1)
      make.top.equalTo(headerSectionLabel.snp.bottom).offset(15)
    }
    contentView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
      make.height.equalTo(50)
    }
  }
  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
