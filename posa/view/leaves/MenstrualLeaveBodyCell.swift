//
// Created by Jaxun on 2018/7/3.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

final class MenstrualLeaveBodyCell: UITableViewCell {

  let leaveDisplay2 = UILabel(frame: .zero)
  let leaveDisplay3 = UILabel(frame: .zero)
  let leaveDisplay4 = UILabel(frame: .zero)
  let leaveDisplay5 = UILabel(frame: .zero)
  let leaveDisplay6 = UILabel(frame: .zero)

  func bind(_ dto:MenstrualLeaveDto) {
   leaveDisplay2.text = String(dto.leavePeriod)                          //period
   leaveDisplay3.text = String(dto.accrued.clean) + " " + "hours"        //accrued
   leaveDisplay4.text = String(dto.used.clean) + " " + "hours"           //Used Hours
   leaveDisplay5.text = String(dto.signing.clean) + " " + "hours"        //waiting for approval
   leaveDisplay6.text = String(dto.available.clean) + " " + "hours"      //signingHours

    contentView.backgroundColor = Themes.starluxLight

    setUILayout() //mark: without left label will visible
  }

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override init(style:UITableViewCellStyle, reuseIdentifier:String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    selectionStyle = .none
    setUILayout()
  }

  private func setUILayout() {
    let leaveTitle1 = UILabel(frame: .zero)
    let leaveTitle2 = UILabel(frame: .zero)
    let leaveTitle3 = UILabel(frame: .zero)
    let leaveTitle4 = UILabel(frame: .zero)
    let leaveTitle5 = UILabel(frame: .zero)

    leaveTitle1.text = I18N.key("period")
    leaveTitle1.textColor = Themes.starluxGold
    leaveTitle2.text = I18N.key("accrued")
    leaveTitle2.textColor = Themes.starluxGold
    leaveTitle3.text = I18N.key("used")
    leaveTitle3.textColor = Themes.starluxGold
    leaveTitle4.text = I18N.key("pending_approval")
    leaveTitle4.textColor = Themes.starluxGold
    leaveTitle5.text = I18N.key("available")
    leaveTitle5.textColor = Themes.starluxGold

    contentView.addSubview(leaveTitle1)
    contentView.addSubview(leaveTitle2)
    contentView.addSubview(leaveTitle3)
    contentView.addSubview(leaveTitle4)
    contentView.addSubview(leaveTitle5)

    contentView.addSubview(leaveDisplay2)
    contentView.addSubview(leaveDisplay3)
    contentView.addSubview(leaveDisplay4)
    contentView.addSubview(leaveDisplay5)
    contentView.addSubview(leaveDisplay6)


    leaveTitle1.snp.makeConstraints { make in
      make.top.equalTo(20)
      make.leading.equalTo(40)
      //make.height.equalTo(10)
    }
    leaveTitle2.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle1.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveTitle3.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle2.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveTitle4.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle3.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveTitle5.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle4.snp.bottom).offset(20)
      make.leading.equalTo(40)
    }
    leaveDisplay2.snp.makeConstraints { make in
      //make.top.equalTo(priceLabel1.snp.bottom).offset(20)
      make.top.equalTo(20)
      make.trailing.equalTo(-40)
    }
    leaveDisplay3.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay2.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    leaveDisplay4.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay3.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    leaveDisplay5.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay4.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    leaveDisplay6.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay5.snp.bottom).offset(20)
      make.trailing.equalTo(-40)
    }
    contentView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
      make.bottom.equalTo(leaveTitle5.snp.bottom).offset(20)
    }
  }

  func appendLine() { // MARK: for Physiology Leave, maybe will remove
    let line = UIView()
    line.backgroundColor = .lightGray
    contentView.addSubview(line)

    line.snp.makeConstraints { maker in
      maker.leading.equalTo(20)
      maker.trailing.equalTo(-20)
      maker.height.equalTo(1)
      maker.bottom.equalTo(contentView)
    }
  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
