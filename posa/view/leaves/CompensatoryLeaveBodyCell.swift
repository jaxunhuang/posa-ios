//
// Created by Jaxun on 2018/7/3.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

final class CompensatoryLeaveBodyCell: UITableViewCell {

  let leaveDisplay2 = UILabel(frame: .zero)
  let leaveDisplay3 = UILabel(frame: .zero)
  let leaveDisplay4 = UILabel(frame: .zero)
  let leaveDisplay5 = UILabel(frame: .zero)
  let leaveDisplay6 = UILabel(frame: .zero)

  func bindCompensatory(_ dto: CompensatoryLeaveDto) {
    leaveDisplay2.text = dto.leaveYear                                //date
    leaveDisplay3.text = dto.duration                                 //period
    leaveDisplay4.text = dto.accrued.clean + " " + "minute(s)"        //accrued
    leaveDisplay5.text = dto.used.clean    + " " + "minute(s)"        //Used Hours
    leaveDisplay6.text = dto.available.clean + " " + "minute(s)"      //signingHours

    contentView.backgroundColor = Themes.starluxLight

    setUILayout() //mark: without left label will visible
  }

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override init(style:UITableViewCellStyle, reuseIdentifier:String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    selectionStyle = .none
    setUILayout()
  }

  private func setUILayout() {
    let leaveTitle1 = UILabel(frame: .zero)
    let leaveTitle2 = UILabel(frame: .zero)
    let leaveTitle3 = UILabel(frame: .zero)
    let leaveTitle4 = UILabel(frame: .zero)
    let leaveTitle5 = UILabel(frame: .zero)

    leaveTitle1.text = I18N.key("date")
    leaveTitle1.textColor = Themes.starluxGold
    leaveTitle2.text = I18N.key("period")
    leaveTitle2.textColor = Themes.starluxGold
    leaveTitle3.text = I18N.key("accrued")
    leaveTitle3.textColor = Themes.starluxGold
    leaveTitle4.text = I18N.key("used")
    leaveTitle4.textColor = Themes.starluxGold
    leaveTitle5.text = I18N.key("available")
    leaveTitle5.textColor = Themes.starluxGold

    contentView.addSubview(leaveTitle1)
    contentView.addSubview(leaveTitle2)
    contentView.addSubview(leaveTitle3)
    contentView.addSubview(leaveTitle4)
    contentView.addSubview(leaveTitle5)

    contentView.addSubview(leaveDisplay2)
    leaveDisplay2.minimumScaleFactor = 0.5
    leaveDisplay2.adjustsFontSizeToFitWidth = true
    contentView.addSubview(leaveDisplay3)
    leaveDisplay3.minimumScaleFactor = 0.5
    leaveDisplay3.adjustsFontSizeToFitWidth = true
    contentView.addSubview(leaveDisplay4)
    leaveDisplay4.minimumScaleFactor = 0.5
    leaveDisplay4.adjustsFontSizeToFitWidth = true
    contentView.addSubview(leaveDisplay5)
    leaveDisplay5.minimumScaleFactor = 0.5
    leaveDisplay5.adjustsFontSizeToFitWidth = true
    contentView.addSubview(leaveDisplay6)
    leaveDisplay6.minimumScaleFactor = 0.5
    leaveDisplay6.adjustsFontSizeToFitWidth = true

    leaveTitle1.snp.makeConstraints { make in
      make.top.equalTo(20)
      make.leading.equalTo(32)
      //make.height.equalTo(10)
    }
    leaveTitle2.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle1.snp.bottom).offset(20)
      make.leading.equalTo(32)
    }
    leaveTitle3.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle2.snp.bottom).offset(20)
      make.leading.equalTo(32)
    }
    leaveTitle4.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle3.snp.bottom).offset(20)
      make.leading.equalTo(32)
    }
    leaveTitle5.snp.makeConstraints { make in
      make.top.equalTo(leaveTitle4.snp.bottom).offset(20)
      make.leading.equalTo(32)
    }
    leaveDisplay2.snp.makeConstraints { make in
      //make.top.equalTo(priceLabel1.snp.bottom).offset(20)
      make.top.equalTo(20)
      make.leading.greaterThanOrEqualTo(leaveTitle1.snp.trailing)
      make.trailing.equalTo(-32)
    }
    leaveDisplay3.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay2.snp.bottom).offset(20)
      make.leading.greaterThanOrEqualTo(leaveTitle2.snp.trailing).offset(16)
      make.trailing.equalTo(-32)
    }
    leaveDisplay4.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay3.snp.bottom).offset(20)
      make.leading.greaterThanOrEqualTo(leaveTitle3.snp.trailing)
      make.trailing.equalTo(-32)
    }
    leaveDisplay5.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay4.snp.bottom).offset(20)
      make.leading.greaterThanOrEqualTo(leaveTitle4.snp.trailing)
      make.trailing.equalTo(-32)
    }
    leaveDisplay6.snp.makeConstraints { make in
      make.top.equalTo(leaveDisplay5.snp.bottom).offset(20)
      make.leading.greaterThanOrEqualTo(leaveTitle5.snp.trailing)
      make.trailing.equalTo(-32)
    }
    contentView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
      make.bottom.equalTo(leaveTitle5.snp.bottom).offset(20)
    }
  }

  func appendLine() { // MARK: for Physiology Leave, maybe will remove
    let line = UIView()
    line.backgroundColor = .lightGray
    contentView.addSubview(line)

    line.snp.makeConstraints { maker in
      maker.leading.equalTo(20)
      maker.trailing.equalTo(-20)
      maker.height.equalTo(1)
      maker.bottom.equalTo(contentView)
    }
  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
