//
// Created by Jaxun on 2018/7/19.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class CompensatoryLeaveView: UIView {

  var handleController:UINavigationController = UINavigationController()
  var choseYear:String = "2018"
  var result:[CompensatoryLeaveDto] = []

  override init(frame:CGRect) {
    super.init(frame: frame)

    backgroundColor = .white
    let tap = UITapGestureRecognizer(target: self, action: #selector(tapToPhys))
    addGestureRecognizer(tap)

    let name = UILabel(frame: CGRect.zero)
    name.text = I18N.key("compensatory_leave")
    name.textColor = Themes.starluxBlack
    name.font = Themes.font20Bold

    let line = UIView(frame: CGRect.zero)
    line.backgroundColor = .gray

    let arrowIcon = UIImageView(image: UIImage(named:"right_arrow_black"))
    arrowIcon.contentMode = .scaleAspectFit

    addSubview(name)
    addSubview(arrowIcon)
    addSubview(line)

    choseYear = Utils.getYear(Date())

    name.snp.makeConstraints { maker in
      maker.centerY.equalTo(self)
      maker.leading.equalTo(line).offset(10)
    }
    arrowIcon.snp.makeConstraints { maker in
      maker.centerY.equalTo(name)
      maker.trailing.equalTo(line).offset(-5)
      maker.size.equalTo(CGSize(width: 12, height: 12))
    }
    line.snp.makeConstraints { maker in
      maker.bottom.equalTo(0)
      maker.leading.equalTo(20)
      maker.trailing.equalTo(-15)
      maker.height.equalTo(1)
    }
  }

  func bind(dtos: [CompensatoryLeaveDto]) {
      result = dtos
  }

    @objc func tapToPhys() {
    //print("chose year >>. \(choseYear)")
    let controller = CompensatoryLeaveDetail(choseYear: choseYear, results: result)
    handleController.pushViewController(controller, animated: true)
  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
