//
// Created by Jaxun on 2018/7/11.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import TTGSnackbar

class FormApplyEntry: UIViewController, UIWebViewDelegate {

  fileprivate let logoBtn = UIButton(type: .custom)
  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
  fileprivate let webView = UIWebView(frame: .zero)
  fileprivate var loadTag = 0
  fileprivate var closeTag = 0

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

    setNaviLogo()
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  private func setNaviLogo() {

    let choseMode:UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode

    logoBtn.setImage(UIImage(named: "title_logo"), for: .normal)
    logoBtn.contentMode = choseMode
    logoBtn.addTarget(self, action: #selector(closePage), for: .touchUpInside)
    self.navigationItem.titleView = logoBtn
  }

  // MARK: - UIViewController
  override func viewDidLoad() {
    super.viewDidLoad()

    //todo:
    let checkAction2 = FormHelper.bpmPath("CheckAction2")
    print("checkAction2.urlEncoded() = \(checkAction2)")
    let urlRequest = URLRequest(url: URL(string: checkAction2)!)
    webView.loadRequest(urlRequest)
    webView.delegate = self

    view.addSubview(webView)
    initLayout()
  }

  func webViewDidFinishLoad(_ webView:UIWebView) {
    let returnUrl = webView.request?.url

    if loadTag == 0 {
      reloadWeb()
      loadTag = 1
      return
    }

    // mark: cuz chose form will load "ApplicationAllList" twice and the last url with "#"
    if (returnUrl?.absoluteString.contains("#"))! {
      webView.goBack()
    }

    // mark: cuz not find "webView.cleanHistory" try to track request's path equal to the first load url path to close it.
    if returnUrl?.lastPathComponent == "FM7_BPMPlus_CheckAction2.aspx" {
      self.navigationController?.popViewController(animated: true)
    }

    // mark: when finished "apply form" will show "sign form" seem strange, so show success toast then back to first page.
    if returnUrl?.lastPathComponent == "FM7_BPMPlus_ApproveAllList.aspx" {
      let showSuccess = TTGSnackbar(message: I18N.key("apply success"), duration: .short)
      showSuccess.icon = UIImage(named: "demo_snack_logo")
      showSuccess.backgroundColor = Themes.starluxGold
      showSuccess.animationType = .slideFromTopBackToTop
      showSuccess.show()
      closeTag = 1

      reloadWeb()
    }
  }

  func reloadWeb() {
    let applicationAllList = FormHelper.bpmPath("ApplicantAllList")
    print("applicationAllList.urlEncoded() = \(applicationAllList)")
    let urlRequest = URLRequest(url: URL(string: applicationAllList)!)
    webView.loadRequest(urlRequest)
  }

  private func initLayout() {
    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }
    logoBtn.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: 155, height: 30)) //manual set
    }
    webView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }

  override func viewWillAppear(_ animated:Bool) {

    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_back"),
        style: .plain,
        target: self,
        action: #selector(clickBack))

    let tap = UITapGestureRecognizer(target: self, action: #selector(closePage))
    navigationItem.titleView?.addGestureRecognizer(tap)
  }

    @objc func clickBack() {
    if webView.canGoBack && closeTag == 0 {
      webView.goBack()
    } else {
      self.navigationController?.popViewController(animated: true)
    }
  }

    @objc func closePage() {
    self.navigationController?.popViewController(animated: true)
  }

  override func viewWillDisappear(_ animated:Bool) {
    super.viewWillDisappear(animated)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateBadgeCount"), object: nil)
  }

}
