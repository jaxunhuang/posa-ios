//
// Created by Jaxun on 2018/7/11.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class FormTrackEntry: UIViewController, UIWebViewDelegate {

  fileprivate let logoBtn = UIButton(type: .custom)
  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
  fileprivate let webView = UIWebView(frame: .zero)
  fileprivate var loadTag = 0

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

    setNaviLogo()
  }

    required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  private func setNaviLogo() {
    let choseMode:UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode

    logoBtn.setImage(UIImage(named: "title_logo"), for: .normal)
    logoBtn.contentMode = choseMode
    logoBtn.addTarget(self, action: #selector(closePage), for: .touchUpInside)
    self.navigationItem.titleView = logoBtn
  }

  // MARK: - UIViewController
  override func viewDidLoad() {
    super.viewDidLoad()

    let checkAction2 =  FormHelper.bpmPath("CheckAction2")
    let urlRequest = URLRequest(url: URL(string: checkAction2)!)
    webView.loadRequest(urlRequest)
    webView.delegate = self
    view.addSubview(webView)
    initLayout()
  }

  private func initLayout() {
    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }
    webView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    logoBtn.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: 155, height: 30)) //manual set
    }
  }

  func webViewDidFinishLoad(_ webView:UIWebView) {
    let returnUrl = webView.request?.url

    print("returnUrl = \(returnUrl)")

    if loadTag == 0 {
      reloadWeb()
      loadTag = 1
      return
    }

    // mark: cuz chose form will load "ApplicationAllList" twice and the last url with "#"
    if (returnUrl?.absoluteString.contains("#"))! {
      webView.goBack()
    }

    // mark: cuz not find "webView.cleanHistory" try to track request's path equal to the first load url path to close it.
    if returnUrl?.lastPathComponent == "FM7_BPMPlus_CheckAction2.aspx" {
      self.navigationController?.popViewController(animated: true)
    }
  }

  func reloadWeb() {
    let trackingAllList = FormHelper.bpmPath("ProcessingAllList")
    let urlRequest = URLRequest(url: URL(string: trackingAllList)!)
    webView.loadRequest(urlRequest)
    print("reload")
  }

  override func viewWillAppear(_ animated:Bool) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_back"),
        style: .plain,
        target: self,
        action: #selector(clickBack))
  }

    @objc func clickBack() {
    if webView.canGoBack {
      webView.goBack()
    } else {
      self.navigationController?.popViewController(animated: true)
    }
  }

    @objc func closePage() {
    self.navigationController?.popViewController(animated: true)
  }

  override func viewWillDisappear(_ animated:Bool) {
    super.viewWillDisappear(animated)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateBadgeCount"), object: nil)
  }
}
