//
// Created by Jaxun on 2018/7/11.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ApplyForgotToPunchEntry: UIViewController {

  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))
  fileprivate let webView = UIWebView(frame: .zero)

  fileprivate let bpmId = Pref().base64AccountId
  fileprivate let bpmTel = Pref().base64BPMTel
  fileprivate let bpmCode = Pref().uniqueBPMCode
  fileprivate let bpmCode2 = Pref().uniqueBPMCode2

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

    setNaviLogo()
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  private func setNaviLogo() {
    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
  }

  // MARK: - UIViewController
  override func viewDidLoad() {
    super.viewDidLoad()

    //todo: 
    let urlString = "\(BuildConfig.bpmEndpoint)FM7_BPMPlus_TrackingAllList.aspx?" +
        "AccountID=\(bpmId!)&" +
        "IMEI=\(bpmCode!)&" +
        "CellNo=\(bpmTel!)&" +
        "DeviceToken=\(bpmCode2!)&" +
        "Act=0"
    let urlRequest = URLRequest(url: URL(string: urlString)!)
    webView.loadRequest(urlRequest)

    view.addSubview(webView)
    initLayout()
  }

  private func initLayout() {
    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }
    webView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
}
