//
// Created by liq on 2016/12/30.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import UIKit

class UILabelWithInset: UILabel {

  var topInset:CGFloat = 0.0
  var bottomInset:CGFloat = 0.0
  var leftInset:CGFloat = 0.0
  var rightInset:CGFloat = 0.0

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  init(_ frame:CGRect, _ inset:CGFloat) {
    super.init(frame:frame)
    topInset = inset
    bottomInset = inset
    leftInset = inset
    rightInset = inset
  }

  init(_ frame:CGRect,
      top topInset:CGFloat,
      left leftInset:CGFloat,
      bottom bottomInset:CGFloat,
      right rightInset:CGFloat) {
    super.init(frame:frame)
    self.topInset = topInset
    self.bottomInset = bottomInset
    self.leftInset = leftInset
    self.rightInset = rightInset
  }

  override func drawText(in rect:CGRect) {
    let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
    super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
  }

  override var intrinsicContentSize:CGSize {
    var intrinsicSuperViewContentSize = super.intrinsicContentSize
    intrinsicSuperViewContentSize.height += topInset + bottomInset
    intrinsicSuperViewContentSize.width += leftInset + rightInset
    return intrinsicSuperViewContentSize
  }


}