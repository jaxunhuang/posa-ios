//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire

typealias ApiErrorHandler = (ApiError) -> Void

class BaseService {

  fileprivate let pref:Pref

  static var apiErrorHandler:ApiErrorHandler = {
    print($0)
  }

  init(_ pref:Pref) {
    self.pref = pref
  }
}

enum ApiError: Swift.Error, Equatable {
  case resourceNotFoundError
  case authenticationError
  case serviceError
  case internalServerError
  case networkError
  case httpStatusError
  case programBugError(AFError)

  static func convertError(_ error:Swift.Error) -> ApiError {
    guard let afError = (error as? AFError) else {
      return serviceError//networkError todo: seem 104 server connect error will return from here.
    }
    guard let responseCode = afError.responseCode else {
      return programBugError(afError)
    }
    if (responseCode == 400) {
      return serviceError
    } else if (responseCode == 401 || responseCode == 403) {
      return authenticationError
    } else if (responseCode == 404) {
      return resourceNotFoundError
    } else if (responseCode >= 500 && responseCode < 600) {
      return internalServerError
    } else {
      return httpStatusError
    }
  }

  static func ==(lhs:ApiError, rhs:ApiError) -> Bool {
    switch (lhs, rhs) {
    case (.programBugError, .programBugError):
      return true
    case (.serviceError, .serviceError):
      return true
    case (.resourceNotFoundError, .resourceNotFoundError):
      return true
    case (.authenticationError, .authenticationError):
      return true
    case (.internalServerError, .internalServerError):
      return true
    case (.networkError, .networkError):
      return true
    case (.httpStatusError, .httpStatusError):
      return true
    default:
      return false
    }
  }
}

// MARK: mapper

extension BaseService {

  func dtoArrayMapper<T: JsonDto>(_ transform:@escaping ([String: Any?]) -> T) -> (Any) -> Array<T> {
    return { (jsonArray) -> Array<T> in
      return (jsonArray as! Array).map {
        transform($0)
      }
    }
  }

  func dtoDataArrayMapper<T: JsonDto>(_ transform:@escaping ([String: Any?]) -> T) -> (Any) -> Array<T> {
    return { (json) -> Array<T> in
      let rawData = json as! BaseArrayDto
      rawData.data // -> dictionary
      return (rawData.data as! Array).map {
        transform($0)
      }
    }
  }

  func dtoAbnormalArrayMapper<T: JsonDto>(_ transform:@escaping ([String: Any?]) -> T) -> (Any) -> Array<T> {
    return { (json) -> Array<T> in
      let rawData = json as! MonthlyAbnormalDto
      rawData.abnormalStatusList

      return (rawData.abnormalStatusList as! Array).map {
        transform($0)
      }
    }
  }

  func pagerMapper<T: JsonDto>(_ name:String,
      _ transform:@escaping ([String: Any?]) -> T) -> (Any) -> Array<T> {

    let pageDtoMapper = dtoMapper(PagerDto.fromDictionary)
    let embeddedArrayMapper = dtoArrayMapper(transform)

    return { (pagerJson) -> Array<T> in
      let pagerDto = pageDtoMapper(pagerJson)
      let jsonArray = pagerDto.resolveEmbeddedArray(name)
      return embeddedArrayMapper(jsonArray)
    }
  }

  func dtoMapper<T: JsonDto>(_ transform:@escaping ([String: Any?]) -> T) -> (Any) -> T {
    return { (json) -> T in
      return transform(json as! [String: Any?])
    }
  }

  func dtoDataMapper<T: JsonDto>(_ transform:@escaping ([String: Any?]) -> T) -> (Any) -> T {
    return { (json) -> T in
      let rawData = json as! BaseDto
      if rawData.code != 1 {
        return transform(["msg": rawData.msg])
      } else {
        return transform(rawData.data as! [String: Any?])
      }
    }
  }

  func dtoDataIfErrorMapper<T: JsonDto>(_ transform:@escaping ([String: Any?]?) -> T) -> (Any) -> T {
    return { (json) -> T in
      let rawData = json as! BaseDto
      return transform(rawData.data as [String: Any?]?)
    }
  }

  func voidMapper() -> (Any) -> Void {
    return { (any) -> Void in
      return ()
    }
  }

  ///
  /// handle all network error and api error and consume to empty result (onNext: won't triggered)
  ///
  /// note that status 404 will fallback to default if provided
  ///
  fileprivate func consumeIfApiError<T>(_ resourceNotFoundDefault:Any?) -> (Swift.Error) -> Observable<T> {
    return { (error:Swift.Error) -> Observable<T> in
      let apiError = ApiError.convertError(error)
      switch (apiError) {
      case .resourceNotFoundError:
        if let defaultValue = resourceNotFoundDefault {
          return Observable.just(defaultValue as! T)
        } else {
          //log.error("api error - \(error)")
          BaseService.apiErrorHandler(apiError)
          return Observable.empty()
        }
      default:
        //log.error("api error - \(error)")

        BaseService.apiErrorHandler(apiError)
        return Observable.empty()
      }
    }
  }

  //todo:
  fileprivate func catchCodeError<T>(_ result:Any?) -> (Swift.Error) -> Observable<T> {
    return { (error:Swift.Error) -> Observable<T> in

      let rawData = result as! BaseDto
      if (rawData.data?.isEmpty)! {
        return Observable.just(rawData.msg as! T)
      } else {
        return Observable.empty()
      }
    }
  }

}

// MARK: json request

extension BaseService {

  private static var legacyEndpoint:String {
    return BuildConfig.ottEndpoint;
  }

  //todo:
  private static var endpoint:String {
    return BuildConfig.endpoint;
  }

  private func defaultHeaders() -> [String: String] {
    var headers = ["Content-Type": BuildConfig.ottBrand]
//    if let ottToken = pref.ottToken {
//      headers["Content-Type"] = ottToken
//    }
    return headers;
  }

  private static func cleanParams(_ params:[String: Any?]?) -> [String: Any]? {
    guard let params = params else {
      return nil
    }
    var cleanParams:[String: Any] = [:]
    for (key, value) in params {
      if (value != nil) {
        cleanParams[key] = value!;
      }
    }
    return cleanParams
  }

  private func ignoreHttpsCerVerify() {
    // this work for ignore SSL cer.
    let manager = SessionManager.default
    manager.delegate.sessionDidReceiveChallenge = {
      session, challenge in
      return (URLSession.AuthChallengeDisposition.useCredential,
          URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
  }

  private func legacyJsonOperation(method:HTTPMethod,
      encoding:ParameterEncoding,
      path:String,
      parameters:[String: Any?]?,
      resourceNotFoundDefault:Any?) -> Observable<Any> {
    return json(method,
        "\(BaseService.legacyEndpoint)\(path)",
        parameters: BaseService.cleanParams(parameters),
        encoding: encoding,
        headers: defaultHeaders())
        .observeOn(MainScheduler.instance)
        .catchError(consumeIfApiError(resourceNotFoundDefault))
  }

  private func jsonOperation(method:HTTPMethod,
      encoding:ParameterEncoding,
      path:String,
      parameters:[String: Any?]?,
      resourceNotFoundDefault:Any?) -> Observable<Any> {
    ignoreHttpsCerVerify()
    return json(method,
        "\(BaseService.endpoint)\(path)",
        parameters: BaseService.cleanParams(parameters),
        encoding: encoding,
        headers: defaultHeaders())
        .do(onNext: { sth in
          print("RxAlamofireResult --- Start")
          print(sth)
          print("RxAlamofireResult --- End")
        })
        .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
        .observeOn(MainScheduler.instance)
        .catchError(consumeIfApiError(resourceNotFoundDefault))
  }

  func fakeJsonOperationWithFullPath(method:HTTPMethod,
      encoding:ParameterEncoding,
      fullPath:String,
      parameters:[String: Any?]?,
      resourceNotFoundDefault:Any?) -> Observable<Any> {

    return json(method,
        "\(fullPath)",
        parameters: BaseService.cleanParams(parameters),
        encoding: encoding,
        headers: defaultHeaders())
        .observeOn(MainScheduler.instance)
        .catchError(consumeIfApiError(resourceNotFoundDefault))
  }

  private func requestOperation(method:HTTPMethod,
      encoding:ParameterEncoding,
      path:String,
      parameters:[String: Any?]?,
      ignoreResourceNotFoundDefault:Bool) -> Observable<Void> {
    return request(method,
        "\(BaseService.legacyEndpoint)\(path)",
        parameters: BaseService.cleanParams(parameters),
        encoding: encoding,
        headers: defaultHeaders())
        .map { (dataRequest) in
          return ()
        }
        .observeOn(MainScheduler.instance)
        .catchError(consumeIfApiError(ignoreResourceNotFoundDefault ? () : nil))
  }

  func jsonGet(_ path:String, params:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .get,
        encoding: URLEncoding.default,
        path: path,
        parameters: params,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  func localJsonGet(_ path:String,
      params:[String: Any?]? = nil,
      resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return legacyJsonOperation(method: .get,
        encoding: URLEncoding.default,
        path: path,
        parameters: params,
        resourceNotFoundDefault: resourceNotFoundDefault)
    //.map(dtoMapper(BaseDto.fromDictionary)) // todo:
  }

  func baseDtoJsonGet(_ path:String,
      params:[String: Any?]? = nil,
      resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .get,
        encoding: URLEncoding.default,
        path: path,
        parameters: params,
        resourceNotFoundDefault: resourceNotFoundDefault)
        .map(dtoMapper(BaseDto.fromDictionary))
  }

  func arrJsonGet(_ path:String, params:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .get,
        encoding: URLEncoding.default,
        path: path,
        parameters: params,
        resourceNotFoundDefault: resourceNotFoundDefault)
        .map(dtoMapper(BaseArrayDto.fromDictionary))
  }

  func fakeFullPathJsonGet(_ path:String,
      params:[String: Any?]? = nil,
      resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return fakeJsonOperationWithFullPath(method: .get,
        encoding: URLEncoding.default,
        fullPath: path,
        parameters: params,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  ///
  /// post and handle response as json (if response is empty content, use post() instead)
  ///
  func localJsonPost(_ path:String,
      body:[String: Any?]? = nil,
      resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return legacyJsonOperation(method: .post,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  func legacyJsonPost(_ path:String,
      body:[String: Any?]? = nil,
      resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .post,
        encoding: URLEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  // todo: tmp for punchtime
  func tmpJsonPost(_ path:String, body:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .post,
        encoding: URLEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
        .map(dtoMapper(BaseDto.fromDictionary))
  }

  func jsonPost(_ path:String, body:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .post,
  encoding: URLEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
        .map(dtoMapper(BaseDto.fromDictionary))
  }

  func post(_ path:String, body:[String: Any?]? = nil, ignoreResourceNotFound:Bool) -> Observable<Void> {
    return requestOperation(method: .post,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        ignoreResourceNotFoundDefault: ignoreResourceNotFound)
  }

  ///
  /// delete and handle response as json (if response is empty content, use delete() instead)
  ///
  func jsonDelete(_ path:String, body:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return legacyJsonOperation(method: .delete,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  func delete(_ path:String, body:[String: Any?]? = nil, ignoreResourceNotFound:Bool) -> Observable<Void> {
    return requestOperation(method: .delete,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        ignoreResourceNotFoundDefault: ignoreResourceNotFound)
  }

  ///
  /// put and handle response as json (if response is empty content, use put() instead)
  ///
  func jsonPut(_ path:String, body:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return legacyJsonOperation(method: .put,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  func put(_ path:String, body:[String: Any?]? = nil, ignoreResourceNotFound:Bool) -> Observable<Void> {
    return requestOperation(method: .put,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        ignoreResourceNotFoundDefault: ignoreResourceNotFound)
  }
}
