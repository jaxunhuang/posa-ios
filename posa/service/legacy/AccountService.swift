//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class AccountService: BaseService {

  func getMyself() -> Observable<AccountDto> {
    return jsonGet("me")
        .map(dtoMapper(AccountDto.fromDictionary))
  }

  func getTokenById(fbId:String, email:String, username:String, imagePath:String?) -> Observable<String> {
    let body = [
        "fbId": fbId, "email": email, "username": username, "imagePath": imagePath]
    return localJsonPost("tokens", body: body)
        .map(dtoMapper(AccessTokenDto.fromDictionary))
        .map {
          $0.accessToken
        }
  }
  func isAddFavorite(_ videoId:Int) -> Observable<Bool> {
    return jsonGet("me/favorites/\(videoId)")
        .map {
          $0 as! Bool
        }
  }

  func addFavorite(_ videoId:Int) -> Observable<Void> {
    return post("me/favorites/\(videoId)", ignoreResourceNotFound: true)
  }

  func removeFavorite(_ videoId:Int) -> Observable<Void> {
    return delete("me/favorites/\(videoId)", ignoreResourceNotFound: true)
  }

  func getMyFavorites(pageNumber:Int, size:Int) -> Observable<Array<VideoFavoriteDto>> {
    return jsonGet("me/favorites", params: ["index": pageNumber, "size": size], resourceNotFoundDefault: [:])
        .map(pagerMapper("programs", VideoFavoriteDto.fromDictionary))
  }

  func subscribe(_ programId:Int) -> Observable<Void> {
    return post("me/subscriptions/\(programId)", ignoreResourceNotFound: true)
  }

  func unsubscribe(_ programId:Int) -> Observable<Void> {
    return delete("me/subscriptions/\(programId)", ignoreResourceNotFound: true)
  }

  func getSubscriptions(pageNumber:Int, size:Int) -> Observable<Array<ProgramSubscriptionDto>> {
    return jsonGet("me/subscriptions", params: ["index": pageNumber, "size": size], resourceNotFoundDefault: [:])
        .map(pagerMapper("programs", ProgramSubscriptionDto.fromDictionary))
  }

  func isSubscribe(_ programId:Int) -> Observable<Bool> {
    return jsonGet("me/subscriptions/\(programId)")
        .map {
          $0 as! Bool
        }
  }
}
