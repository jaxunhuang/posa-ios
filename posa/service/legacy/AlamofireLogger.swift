//
// Created by jaxun on 1/4/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import Alamofire

///
/// copy from
///    https://github.com/konkab/AlamofireNetworkActivityLogger/blob/master/Source/NetworkActivityLogger.swift
///

enum AlamofireLoggerLogLevel: String {
  case none, basic, headers, body
}

class AlamofireLogger {

  public static let shared = AlamofireLogger()

  private var startDates:[URLSessionTask: Date]

  private var level:AlamofireLoggerLogLevel

  init() {
    startDates = [:]
    level = .basic
  }

  public func startLogging(level:AlamofireLoggerLogLevel = .basic) {
    stopLogging()
    self.level = level
    if self.level == .none {
      return
    }

    let notificationCenter = NotificationCenter.default

    notificationCenter.addObserver(
        self,
        selector: #selector(AlamofireLogger.networkRequestDidStart(notification:)),
        name: Notification.Name.Task.DidResume,
        object: nil
    )

    notificationCenter.addObserver(
        self,
        selector: #selector(AlamofireLogger.networkRequestDidComplete(notification:)),
        name: Notification.Name.Task.DidComplete,
        object: nil
    )
  }

  public func stopLogging() {
    if self.level == .none {
      return
    }
    NotificationCenter.default.removeObserver(self)
  }

  deinit {
    stopLogging()
  }

  @objc private func networkRequestDidStart(notification:Notification) {
    guard let userInfo = notification.userInfo,
          let task = userInfo[Notification.Key.Task] as? URLSessionTask,
          let request = task.originalRequest,
          let httpMethod = request.httpMethod,
          let requestURL = request.url
        else {
      return
    }

    if level == AlamofireLoggerLogLevel.none {
      return
    }

    startDates[task] = Date()

    // level == basic
    var msg = "\(httpMethod) '\(requestURL.absoluteString)' "

    if level == .headers || level == .body {
      msg += "{"
      if let httpHeadersFields = request.allHTTPHeaderFields {
        for (key, value) in httpHeadersFields {
          msg += "\(key): \(value) "
        }
      }
      msg += "}"
    }

    if level == .body {
      if let httpBody = request.httpBody,
         let httpBodyString = String(data: httpBody, encoding: .utf8) {
        msg += "\n\(httpBodyString)"
      }
    }

    log.debug(file: "", line: 0, function: "", msg)
  }

  @objc private func networkRequestDidComplete(notification:Notification) {
    guard let sessionDelegate = notification.object as? SessionDelegate,
          let userInfo = notification.userInfo,
          let task = userInfo[Notification.Key.Task] as? URLSessionTask,
          let request = task.originalRequest,
          let httpMethod = request.httpMethod,
          let requestURL = request.url
        else {
      return
    }

    if level == AlamofireLoggerLogLevel.none {
      return
    }

    var elapsedTime:TimeInterval = 0.0

    if let startDate = startDates[task] {
      elapsedTime = Date().timeIntervalSince(startDate)
      startDates[task] = nil
    }

    if let error = task.error {
      let time = String(format: "%.04f", elapsedTime)
      let msg = "[Error] \(httpMethod) '\(requestURL.absoluteString)' [\(time) s]:\(error)"
      log.error(file: "",
          line: 0,
          function: "",
          msg)
    } else {
      guard let response = task.response as? HTTPURLResponse else {
        return
      }

      let time = String(format: "%.04f", elapsedTime)
      // level == basic
      var msg = "\(String(response.statusCode)) '\(requestURL.absoluteString)' [\(time) s] "

      if level == .headers || level == .body {
        msg += "{"
        for (key, value) in response.allHeaderFields {
          msg += "\(key): \(value) "
        }
        msg += "}"
      }

      guard let data = sessionDelegate[task]?.delegate.data else {
        log.debug(file: "", line: 0, function: "", msg)
        return
      }

      if level == .body {
        do {
          let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
          let prettyData = try JSONSerialization.data(withJSONObject: jsonObject)

          if let prettyString = String(data: prettyData, encoding: .utf8) {
            msg += "\n\(prettyString)"
          }
        } catch {
          if let nsString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
            msg += "\n\(nsString as String)"
          }
        }
      }
      log.debug(file: "", line: 0, function: "", msg)
    }
  }
}
