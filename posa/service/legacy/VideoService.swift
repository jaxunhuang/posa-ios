//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire

class VideoService: BaseService {

  func getVideos(pageNumber:Int, size:Int) -> Observable<Array<VideoDto>> {
    let params = ["index": pageNumber, "size": size]
    return jsonGet("videos", params: params, resourceNotFoundDefault: [:])
        .map(pagerMapper("videos", VideoDto.fromDictionary))
  }

  func getVideo(_ videoId:Int) -> Observable<VideoDto> {
    return jsonGet("videos/\(videoId)")
        .map(dtoMapper(VideoDto.fromDictionary))
  }

  func getVideoFavoriteCount(_ videoId:Int) -> Observable<Int> {
    return jsonGet("videos/\(videoId)/favorites/count")
        .map {
          $0 as! Int
        }
  }

  func getVideoTypes(_ videoId:Int) -> Observable<Array<VideoTypeDto>> {
    return jsonGet("videotypes", params: ["videoId": videoId])
        .map(dtoArrayMapper(VideoTypeDto.fromDictionary))
  }

  func getVideoTypes() -> Observable<Array<VideoTypeProgramDto>> {
    return jsonGet("videotypes")
        .map(dtoArrayMapper(VideoTypeProgramDto.fromDictionary))
  }
}
