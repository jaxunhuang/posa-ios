//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

struct PunchTimeDefine {
    static let firstTime = I18N.key("in")
    static let lastTime = I18N.key("out")
    static let noData = "--:--:--"
    static let punchTimeOn = I18N.key("clock_in")
    static let punchTimeOff = I18N.key("clock_out")
    static let formApply = I18N.key("application")
    static let formSign = I18N.key("approval")
    static let formTrack = I18N.key("tracing")

    static let yearDayFormat = "MMM  dd  yyyy"
    static let yearNtimeFormat = "yyyy/MM/dd HH:mm:ss"
    static let timeFormat = "HH : mm : ss"
    static let weekDayFormat = "EEE"
    static let todayFormat = "yyyy/MM/dd"
}

class PunchTimeService: BaseService {

    func sendPushToken(token: String) {
        let account = Pref().aliasAccount

        let url = URL(string: "\(BuildConfig.endpoint)setTokensFromMobile")
        let params = [
            "ep_no": account!,
            "token": token,
        ]

        Alamofire.request(url!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { [weak self] response in
                if let status = response.response?.statusCode {
                    // used this response is not json.

                    /*switch (status) {
                    case 200:
                      print("send success!")
                    default:
                      print("error with response status: \(status)")
                    }*/
                }
            }
    }

    func getBPMInfo() -> Observable<Any> {
        let account = Pref().aliasAccount
        let tel = Pref().aliasTel
        let uniqueCode1 = Pref().aliasUniqueId
        //let uniqueCode2 = Pref().fcmPushToken
        let uniqueCode2 = String(uniqueCode1!.reversed())

        print("uniqueCode1 = \(uniqueCode1)")
        print("uniqueCode2 = \(uniqueCode2)")

        let params = [
            "account": account!,
            "mobile": tel!,
            "code": uniqueCode1,
            "token": uniqueCode2
        ]
        return jsonGet("getBPMInfo", params: params)
    }

    func getFormBadgeCounts() -> Observable<Any>/*BaseDto*/ {
        let account = Pref().aliasAccount
        let params = [
//      "ep_no": "administrator"
            "ep_no": account!,
        ]
        return baseDtoJsonGet("getFormBadgeCount", params: params)
    }

    func fetchWifiList() -> Observable<BssidListDto> {
        return baseDtoJsonGet("wifi")
            .map(dtoDataMapper(BssidListDto.fromDictionary))
    }

    func fetchTodayRecords(accountId: String, today: String) -> Observable<RecordsDto> {
        let params = [
            "ep_no": accountId, "date": today]
        return baseDtoJsonGet("getCardDataByDay", params: params)
            .map(dtoDataMapper(RecordsDto.fromDictionary))
    }

    /**
      reset punch time device id api:
      http://10.20.11.71:3000/api/updateDeviceID
      params: ep_no = "account id", device_id =""
     */
    func punchTimeWork(accountId: String, punchType: Int) -> Observable<PunchTimeDto> {
        //todo: vender uuid params // for simulator :5FE4F167-B635-4287-B043-CF32A2A8F776

        var deivceId = "5FE4F167-B635-4287-B043-CF32A2A8F776"
#if targetEnvironment(simulator) // is Simulator
#else
        deivceId = Pref().aliasUniqueId!
#endif
        let body = ["ep_no": accountId, "status": punchType, "device_id": deivceId] as [String: Any]
        return jsonPost("clock", body: body)
            .map(dtoDataMapper(PunchTimeDto.fromDictionary))
    }

    func fakeLocalJson() -> Observable<BaseTimeDto> {
        return fakeFullPathJsonGet("http://localhost:3000/posts")
            .map(dtoMapper(BaseTimeDto.fromDictionary))
    }

}
