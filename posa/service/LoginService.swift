//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

class LoginService: BaseService {

  fileprivate let pref = Pref()

  func fetchAlias(account:String, password:String) -> Observable<BaseDto?> {
    let body = [
      "account": account, "password": password]
    return jsonPost("login", body: body)
        .map{ result in result as? BaseDto }
        .do(onNext: { [weak self] optaionalDto in
            self?.accountSetting(dto: optaionalDto, email: account)
         })
  }

  fileprivate func accountSetting(dto: BaseDto?, email: String) {
    guard let dtoData = dto?.data else { return }
    let accountFullDto = AccountFullDto.fromDictionary(dtoData) // todo: fix init() method not fromDictionay
    pref.aliasAccount = accountFullDto.empNo
    pref.aliasNum = accountFullDto.empNo
    pref.aliasName = accountFullDto.name
    pref.aliasJobTitle = accountFullDto.jobEnTitle
    pref.aliasDepartment = accountFullDto.departmentName
    pref.aliasTel = accountFullDto.tel
    pref.aliasSex = accountFullDto.sex
    pref.aliasSiteFloor = accountFullDto.floorSite
    pref.aliasPhoto = accountFullDto.aliasIconUrl
    pref.aliasOnBoardDay = accountFullDto.onBoardDay
    pref.aliasNameEn = accountFullDto.eName
    pref.aliasDepartmentEn = accountFullDto.departmentEName
    if pref.aliasUniqueId?.isEmpty == true {
      pref.aliasUniqueId = UIDevice.current.identifierForVendor?.uuidString // also save to keychain
    }
    pref.aliasEmail = email // for account alias icon
  }
}
