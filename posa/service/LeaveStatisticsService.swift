//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift


class LeaveStatisticsService: BaseService {

  func fetchAllLeave(accountId:String,
      year:String) -> Observable<Any> {
    let params = [
      "ep_no": accountId, "year": year
    ]
    return arrJsonGet("getEmployeeAllLeaveWithoutMenstrual", params: params)
  }

  func fetchCompensatoryLeave(accountId:String,
                     year:String) -> Observable<Any> {
    let params = [
      "ep_no": accountId, "year": year
//      "ep_no": "1700258", "year": "2017" // sample
    ]
    return arrJsonGet("getCompensatoryLeave", params: params)
  }

  func fakeLocalJson() -> Observable<Array<BaseAbnormalDto>> {
    return fakeFullPathJsonGet("http://localhost:3000/posts")
        .map(dtoMapper(BaseArrayDto.fromDictionary))
        .map(dtoDataArrayMapper(BaseAbnormalDto.fromDictionary))
  }

  func fetchMenstrualLeave(accountId:String,
      year:String) -> Observable<Any> {
    let params = [
      "ep_no": accountId, "year": year
//      "ep_no": "1801104", "year": year
    ]
    return arrJsonGet("getMenstrualLeave", params: params)
  }
}
