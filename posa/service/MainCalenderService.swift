//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift


class MainCalenderService: BaseService {

  func fetchMonthlyPunchTimeList(accountId:String,
      year:String,
      month:String) -> Observable<Array<MonthlyPunchTimeDto>> {
    let params = [
      "ep_no": accountId, "year": year, "month": month
    ]
    return arrJsonGet("getCardDataByMonth", params: params)
        .map(dtoDataArrayMapper(MonthlyPunchTimeDto.fromDictionary))
  }

  func fetchMonthlyAbnormalList(accountId:String, year:String, month:String) -> Observable<Array<BaseAbnormalDto>> {
    let params = [
      "ep_no": accountId, "year": year, "month": month
    ]
    return arrJsonGet("getCardDataMatchByMonth", params: params)
        .map(dtoDataArrayMapper(BaseAbnormalDto.fromDictionary))
  }

  func fetchMonthlyMealOrderList(accountId:String, year:String, month:String) -> Observable<Array<BaseMealOrderDto>> {
    let params = [
      "ep_no": accountId, "year": year, "month": month
    ]
    return arrJsonGet("getMealOrderByMonth", params: params)
        .map(dtoDataArrayMapper(BaseMealOrderDto.fromDictionary))
  }

}
