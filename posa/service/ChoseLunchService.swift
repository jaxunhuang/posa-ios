//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift


class ChoseLunchService: BaseService {

  func orderLunchMeal(accountId:String,
      cname:String,
      ename:String,
      selection:String,
      depName:String
  ) -> Observable<Any> {
    let params = [
      "ep_no": accountId,
      "cname": cname,
      "ename": ename,
      "selection": selection,
      "dep_name": depName
    ]
    return legacyJsonPost("MealOrder", body: params)
    //return jsonPost("MealOrder", body: params)

    // api return format!!!!
  }

  func fakeLocalJson() -> Observable<Array<BaseAbnormalDto>> {
    return fakeFullPathJsonGet("http://localhost:3000/posts")
        .map(dtoMapper(BaseArrayDto.fromDictionary))
        .map(dtoDataArrayMapper(BaseAbnormalDto.fromDictionary))
  }

}
