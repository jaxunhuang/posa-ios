//
//  AppDelegate.swift
//  posa
//
//  Created by jaxun on 12/27/16.
//  Copyright © 2016 STARLUX. All rights reserved.
//

import UIKit
import SwiftTheme
import RxSwift
import Foundation
import Fabric
import Crashlytics
import Firebase

import UserNotifications
import Firebase
import FirebaseInstanceID

import Alertift

//import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

  var window:UIWindow?

  let gcmMessageIDKey = "gcm.message_id"

  let disposeBag = DisposeBag()

  func application(_ application:UIApplication,
      didFinishLaunchingWithOptions launchOptions:[UIApplicationLaunchOptionsKey: Any]?) -> Bool {


    if #available(iOS 10.0, *) {
      // For iOS 10 display notification (sent via APNS)
      UNUserNotificationCenter.current().delegate = self

      let authOptions:UNAuthorizationOptions = [.alert, .badge, .sound]
      UNUserNotificationCenter.current().requestAuthorization(
          options: authOptions,
          completionHandler: { _, _ in })
    } else {
      let settings:UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
      application.registerUserNotificationSettings(settings)
    }

    application.registerForRemoteNotifications()

    // Add observer for InstanceID token refresh callback.
    NotificationCenter.default.addObserver(self,
        selector: #selector(self.tokenRefreshNotification),
        name: Notification.Name.MessagingRegistrationTokenRefreshed,
        object: nil)

    FirebaseApp.configure()
    Messaging.messaging().delegate = self

//    InstanceID.instanceID().instanceID { (result, error) in
//      if let error = error {
//        print("Error fetching remote instange ID: \(error)")
//      } else if let result = result {
//        print("Remote instance ID token: \(result.token)")
//      }
//    }

#if BUILD_RELEASE
    Fabric.with([Crashlytics.self])
#endif

    window = UIWindow(frame: UIScreen.main.bounds)
    if let window = window {

      //3D Touch
      let punchTimeIconOn = UIApplicationShortcutIcon(templateImageName:"shortcut_on")
      let punchTimeIconOff = UIApplicationShortcutIcon(templateImageName:"shortcut_off")
      let punchTimeItem = UIApplicationShortcutItem(type: "punchTimeOn",
          localizedTitle: I18N.key("clock_in"),
          localizedSubtitle: "",
          icon: punchTimeIconOn,
          userInfo: nil)

      let punchTimeOffItem = UIApplicationShortcutItem(type: "punchTimeOff",
          localizedTitle: I18N.key("clock_out"),
          localizedSubtitle: "",
          icon: punchTimeIconOff,
          userInfo: nil)

      UIApplication.shared.shortcutItems = [punchTimeOffItem, punchTimeItem]

      var isLogin = false
      if (Pref().aliasAccount != nil) {
        isLogin = true
      }
      let navigationController = UINavigationController(rootViewController: LoginViewController(isLogin: isLogin))
      navigationController.isNavigationBarHidden = true

      window.theme_backgroundColor = Themes.backgroundColor
      window.rootViewController = navigationController
      window.makeKeyAndVisible()
    }

    return true
  }

  func applicationWillResignActive(_ application:UIApplication) {
  }

  func applicationDidEnterBackground(_ application:UIApplication) {
  }

  func applicationWillEnterForeground(_ application:UIApplication) {
  }

  func applicationDidBecomeActive(_ application:UIApplication) {
  }

  func applicationWillTerminate(_ application:UIApplication) {

  }

  // 3D-Touch event
  func application(_ application:UIApplication,
      performActionFor shortcutItem:UIApplicationShortcutItem,
      completionHandler:@escaping (Bool) -> Void) {
    var punchType = 9 as Int

    window = UIWindow(frame: UIScreen.main.bounds)
    if let window = window {

      switch shortcutItem.type {
      case "punchTimeOn":
        punchType = 0
      case "punchTimeOff":
        punchType = 1
      default:
        return
      }

      // auth in check in

      //let rootViewController = PunchTimeEntry(shortCut: true, punchType: punchType)


      var isLogin = false
      if (Pref().aliasAccount != nil) {
        isLogin = true

//        let rootViewController = UINavigationController(rootViewController: LegacyLoginViewController(isLogin: isLogin,
//            type: punchType))
//        rootViewController.isNavigationBarHidden = true
//
//        window.theme_backgroundColor = Themes.backgroundColor
//        window.makeKeyAndVisible()
//        window.rootViewController = rootViewController
      } else {
        // nothing to do...
        Alertift.alert(message: "can't use short cut")
            .action(.destructive("OK"), handler: { (_, _, _) in
                exit(0)
            })
            .show()
      }

    }

  }


  // [START receive_message]
  func application(_ application:UIApplication, didReceiveRemoteNotification userInfo:[AnyHashable: Any]) {

    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // Print full message.
    print(userInfo)
  }

  func application(_ application:UIApplication, didReceiveRemoteNotification userInfo:[AnyHashable: Any],
      fetchCompletionHandler completionHandler:@escaping (UIBackgroundFetchResult) -> Void) {

    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // Print full message.
    print(userInfo)

    completionHandler(UIBackgroundFetchResult.newData)
  }

  // [END receive_message]
  // [START refresh_token]
    @objc func tokenRefreshNotification(_ notification:Notification) {
    if let refreshedToken = InstanceID.instanceID().token() {
      //todo: will send to hank api

      Pref().fcmPushToken = refreshedToken
      print("InstanceID token: \(refreshedToken)")
        // W8RXd2ETI1S58jnrYNarbS57oBb6aRm0jygUe25BwwPItJ4jQ-4S2GHIaDke56nsoWEgc02SoW4pzc7oYXq21EQP9yLIqXLstgJuNKynsbNJAlMrTLaQiM0CSJiK17ZsQsm0-uUBpjIdA //
    }

    // Connect to FCM since connection may have failed when attempted before having a token.
    connectToFcm()
  }

  // [END refresh_token]
  // [START connect_to_fcm]
  func connectToFcm() {
    // Won't connect since there is no token
    guard InstanceID.instanceID().token() != nil else {
      return
    }

    // Disconnect previous FCM connection if it exists.
    Messaging.messaging().disconnect()

    Messaging.messaging().connect { (error) in
      if error != nil {
        print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
      } else {
        print("Connected to FCM.")
      }
    }
  }

  // [END connect_to_fcm]
  func application(_ application:UIApplication, didFailToRegisterForRemoteNotificationsWithError error:Error) {
    print("Unable to register for remote notifications: \(error.localizedDescription)")
  }

  /* // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
   // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
   // the InstanceID token.
   func application(_ application:UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken:Data) {
     print("APNs token retrieved: \(deviceToken)")

     let deviceTokenString = deviceToken.reduce("") {
       return $0 + String(format: "%02x", $1)
     }
     print(deviceTokenString)


     // With swizzling disabled you must set the APNs token here.
     // FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
   }

     func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
         print("Firebase registration token: \(fcmToken)")

         // Note: This callback is fired at each app startup and whenever a new token is generated.
     }*/


  // [START connect_on_active]
//  func applicationDidBecomeActive(_ application: UIApplication) {
//    connectToFcm()
//  }
  // [END connect_on_active]
  // [START disconnect_from_fcm]
//  func applicationDidEnterBackground(_ application: UIApplication) {
//    Messaging.messaging().disconnect()
//    print("Disconnected from FCM.")
//  }
  // [END disconnect_from_fcm]
}


// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate {//} : UNUserNotificationCenterDelegate {

  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center:UNUserNotificationCenter,
      willPresent notification:UNNotification,
      withCompletionHandler completionHandler:@escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo
    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // Print full message.
    print(userInfo)

    // Change this to your preferred presentation option
    // 設置通知的選項
    completionHandler(UNNotificationPresentationOptions.alert)
  }

  func userNotificationCenter(_ center:UNUserNotificationCenter,
      didReceive response:UNNotificationResponse,
      withCompletionHandler completionHandler:@escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo
    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // Print full message.
    print(userInfo)

    completionHandler()
  }
}

// [END ios_10_message_handling]
// [START ios_10_data_message_handling]
extension AppDelegate: MessagingDelegate {
  func messaging(_ messaging:Messaging, didRefreshRegistrationToken fcmToken:String) {

  }

  // Receive data message on iOS 10 devices while app is in the foreground.
  func application(received remoteMessage:MessagingRemoteMessage) {
    print(remoteMessage.appData)
  }
}

// [END ios_10_data_message_handling]
