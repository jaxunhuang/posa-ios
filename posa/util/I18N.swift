//
// Created by Jaxun on 2018/7/19.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

struct I18N {
  static func key(_ localizedKey:String) -> String {
    let format = NSLocalizedString(localizedKey, comment: "")
    return String(format: format)
  }
}