//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

protocol JsonDto: AutoToString, Hashable {
  static func fromDictionary(_ raw:[String: Any?]) -> Self
}
