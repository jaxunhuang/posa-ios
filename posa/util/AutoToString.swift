//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

protocol AutoToString: CustomStringConvertible {
}

extension CustomStringConvertible where Self: AutoToString {
  var description:String {
    var desc = "\(type(of: self)) {"
    let selfMirror = Mirror(reflecting: self)
    desc += selfMirror.children.filter {
      $0.label != nil
    }.map {
      "\($0.label!):\($0.value)"
    }.joined(separator: ", ")
    return desc + "}"
  }
}
