//
//  CollapsibleTableViewHeader.swift
//  CollapsibleTableSectionViewController
//
//  Created by Yong Su on 7/20/17.
//  Copyright © 2017 jeantimex. All rights reserved.
//

import UIKit
import SnapKit

protocol CollapsibleTableViewHeaderDelegate: class {
  func toggleSection(_ section:Int)
}

open class CollapsibleTableViewHeader: UITableViewHeaderFooterView {

  weak var delegate:CollapsibleTableViewHeaderDelegate?
  var section:Int = 0

  let titleLabel = UILabel()
  let arrowIcon = UIImageView(image: UIImage(named: "right_arrow_black"))

  //let arrowLabel = UILabel()

  override public init(reuseIdentifier:String?) {
    super.init(reuseIdentifier: reuseIdentifier)

    // Content View
    contentView.backgroundColor = .white

    let marginGuide = contentView.layoutMarginsGuide

    // Arrow label
    contentView.addSubview(arrowIcon)
    arrowIcon.contentMode = .scaleAspectFit
    arrowIcon.translatesAutoresizingMaskIntoConstraints = false
    arrowIcon.widthAnchor.constraint(equalToConstant: 12).isActive = true
    arrowIcon.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
    arrowIcon.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
    arrowIcon.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true

    // Title label
    titleLabel.textColor = Themes.starluxBlack
    titleLabel.font = Themes.font20Bold
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(titleLabel)

    // origin Title layout setting
    /*titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
    titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
    titleLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
    titleLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true*/

    // Gray Line
    let separator = UIView()
    separator.backgroundColor = .lightGray
    contentView.addSubview(separator)

    titleLabel.snp.makeConstraints { maker in
      maker.top.equalTo(15)
      maker.leading.equalTo(30)
    }
    separator.snp.makeConstraints { make in
      make.leading.equalTo(20)
      make.trailing.equalTo(-15)
      make.height.equalTo(1)
      //make.top.equalTo(titleLabel.snp.bottom).offset(15)
      make.bottom.equalTo(contentView)
    }

    //
    // Call tapHeader when tapping on this header
    //
    addGestureRecognizer(UITapGestureRecognizer(target: self,
        action: #selector(CollapsibleTableViewHeader.tapHeader(_:))))
  }

  required public init?(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  //
  // Trigger toggle section when tapping on the header
  //
    @objc func tapHeader(_ gestureRecognizer:UITapGestureRecognizer) {
    guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
      return
    }

    _ = delegate?.toggleSection(cell.section)
  }

  func setCollapsed(_ collapsed:Bool) {
    //
    // Animate the arrow rotation (see Extensions.swf)
    //
    arrowIcon.rotate(collapsed ? 0.0 : .pi / 2)
  }

}

/*extension UIColor {
    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
}*/

extension UIView {

  func rotate(_ toValue:CGFloat, duration:CFTimeInterval = 0.2) {
    let animation = CABasicAnimation(keyPath: "transform.rotation")

    animation.toValue = toValue
    animation.duration = duration
    animation.isRemovedOnCompletion = false
    animation.fillMode = kCAFillModeForwards

    self.layer.add(animation, forKey: nil)
  }

}


