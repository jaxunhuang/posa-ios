//
// Created by JaxunC on 2018/7/12.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindMissBssid.h"

@implementation FindMissBssid


// MARK: iOS some time will lose one bssid code. so scan than fix if result missing. https://bbs.csdn.net/topics/390966176
+ (NSString *) standardFormatMAC:(NSString *)MAC {
  NSArray * subStr = [MAC componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":-"]];
  NSMutableArray * subStr_M = [[NSMutableArray alloc] initWithCapacity:0];
  for (NSString * str in subStr) {
    if (1 == str.length) {
      NSString * tmpStr = [NSString stringWithFormat:@"0%@", str];
      [subStr_M addObject:tmpStr];
    } else {
      [subStr_M addObject:str];
    }
  }

  NSString * formateMAC = [subStr_M componentsJoinedByString:@":"];

  return [formateMAC uppercaseString];
}

@end